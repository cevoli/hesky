#include <iostream>
#include <ctime>

#include "errorCodes.h"
#include "readInputXML.h"
#include "hesky.h"
#include "outputManager.h"

int main(int argc, char** argv) {
  
  try {
    if (argc != 2) 
      throw NO_XML_FILE;
    
    time_t beginRun, 
      endRun;
    
    time (&beginRun);
    
    hesky::Input * in = new hesky::Input();
    in->loadFile( argv[1] );
    in->printInputParameters();
    
    hesky::HESKY * he = new hesky::HESKY(in);
    
    he->Run();
  
    time (&endRun);
    
    std::cout<<"Computation of maps ended in "<<endRun-beginRun<<" s."<<std::endl;
    
    hesky::OutputManager * om = new hesky::OutputManager(he);
    
    if (in->doOutputFITS) 
      om->saveMapsOnFITS();
    if (in->doOutputEmissivity) 
      om->saveEmissivityOnFITS();
    if (in->doOutputASCII) 
      om->saveMapsOnASCII();
    
    if (om) delete om;
    if (in) delete in;
    if (he) delete he;
  }
  
  catch (int e) {
    std::cerr<<"Usage: ./hesky <settings xml file>"<<std::endl;
  }
  
  return (0);
}

