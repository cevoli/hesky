#include "galaxy.h"

/*
void Galaxy::Print(fitsfile* output_ptr, double numin, double numax, double nufactor, int nnu) {
    
  int status = 0;
  const int naxis = 1;
  long output_size_axes[naxis] = {1};
  int nelements = output_size_axes[0];
  int fpixel = 1;
  int bitpix = FLOAT_IMG;
  float* result = new float[nelements]();  
  
  if (fits_create_img(output_ptr, bitpix, naxis, output_size_axes, &status)) fits_report_error(stderr, status); 
  if (!(inp->iHealpix)) {
    double grid_lmin = l_grid.front();
    double grid_lmax = l_grid.back();
    double grid_bmin = b_grid.front();
    double grid_bmax = b_grid.back();

    char str[] = "GRID";
  
    if (fits_write_key(output_ptr, TDOUBLE, "lmin",    &grid_lmin, NULL, &status)) fits_report_error(stderr, status); 
    if (fits_write_key(output_ptr, TDOUBLE, "lmax",    &grid_lmax, NULL, &status)) fits_report_error(stderr, status); 
    if (fits_write_key(output_ptr, TDOUBLE, "bmin",    &grid_bmin, NULL, &status)) fits_report_error(stderr, status); 
    if (fits_write_key(output_ptr, TDOUBLE, "bmax",    &grid_bmax, NULL, &status)) fits_report_error(stderr, status); 
    if (fits_write_key(output_ptr, TDOUBLE, "dl",      &Dl,        NULL, &status)) fits_report_error(stderr, status); 
    if (fits_write_key(output_ptr, TDOUBLE, "db",      &Db,        NULL, &status)) fits_report_error(stderr, status); 
    if (fits_write_key(output_ptr, TINT,    "nl",      &nl,        NULL, &status)) fits_report_error(stderr, status); 
    if (fits_write_key(output_ptr, TINT,    "nb",      &nb,        NULL, &status)) fits_report_error(stderr, status); 
    if (fits_write_key(output_ptr, TSTRING, "PIXTYPE", str, "GRID", &status)) fits_report_error(stderr, status);
  }
  else {
    if (fits_write_date(output_ptr, &status)) fits_report_error(stderr, status);
    char order[9];                 // HEALPix ordering 

    char str[] = "HEALPIX";
    if (fits_write_key(output_ptr, TSTRING, "PIXTYPE", str, "HEALPIX Pixelisation", &status)) fits_report_error(stderr, status);
    
    strcpy(order, "RING    ");
    if (fits_write_key(output_ptr, TSTRING, "ORDERING", order, "Pixel ordering scheme, either RING or NESTED", &status)) fits_report_error(stderr, status);
    
    if (fits_write_key(output_ptr, TLONG, "NSIDE", &nside, "Resolution parameter for HEALPIX", &status)) fits_report_error(stderr, status);
    if (fits_write_key(output_ptr, TDOUBLE, "RES", &res, "Resolution of map", &status)) fits_report_error(stderr, status);
    
    if (fits_write_key(output_ptr, TDOUBLE, "numin",    &numin, NULL, &status)) fits_report_error(stderr, status); 
    if (fits_write_key(output_ptr, TDOUBLE, "numax",    &numax, NULL, &status)) fits_report_error(stderr, status); 
    if (fits_write_key(output_ptr, TDOUBLE, "nufactor", &nufactor,   NULL, &status)) fits_report_error(stderr, status); 
    if (fits_write_key(output_ptr, TINT,    "nnu",      &nnu,        NULL, &status)) fits_report_error(stderr, status); 
  }
  
  if (fits_write_img(output_ptr, TFLOAT, fpixel, nelements, result, &status))    fits_report_error(stderr, status);
  
  delete [] result;
  
  return ;
}

double Galaxy::GetSpiralExponent() {
  
 return inp->SpiralExponent; 
}
*/
