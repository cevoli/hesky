#include "utils.h"

double KamaeYields::GetSigma(double Eg, double Ekinbullet, int par) {

	PARAMSET parameters;
	double sigma;

	if (Ekinbullet < 520000.1 && Eg < 520000) {
		switch (par) {
		case ID_GAMMA:
			gamma_param(Ekinbullet, &parameters);
			break;
		case ID_NUE:
			nue_param(Ekinbullet, &parameters);
			break;
		case ID_NUMU:
			numu_param(Ekinbullet, &parameters);
			break;
		case ID_ANTINUE:
			antinue_param(Ekinbullet, &parameters);
			break;
		case ID_ANTINUMU:
			antinumu_param(Ekinbullet, &parameters);
			break;
		default:
			break;
		}
		sigma = 1.e-3*sigma_incl_tot(par,Eg, Ekinbullet, &parameters)/Eg;
	}

	else {

		double L = std::log((Ekinbullet+0.938)/1000.0);

		double sigmatot = (34.3 + 1.88*L + 0.25*pow(L,2.0))*1e-3;

		double x = Eg/(Ekinbullet+0.938);
		double Be = 1.0/(69.5+2.65*L+0.3*pow(L,2.0));
		double betae = 1./pow(0.201+0.062*L+0.00042*pow(L,2.0), 1.0/4.0);
		double ke = (0.279+0.141*L+0.0172*pow(L,2.0))/(0.3+pow(2.0+L,2.0));
		double Fnumu2 = Be*pow(1.0+ke*pow(log(x),2.0), 3.0)/(x*(1.0+0.3/pow(x,betae)))*pow(-log(x),5.0);
		double Fnumu1 = 0.0;
		double y = x/0.427;
		if (y<1.0) {
			double Bprime = 1.75+0.204*L+0.010*pow(L,2.0);
			double betaprime = 1.0/(1.67+0.111*L+0.0038*pow(L,2.0));
			double kprime = 1.07-0.086*L+0.002*pow(L,2.0);
			Fnumu1 = Bprime*log(y)/y*pow((1.0-pow(y,betaprime))/(1.0+kprime*pow(y,betaprime)*(1.0-pow(y,betaprime))),4.0)*(1.0/log(y) - (4.0*betaprime*pow(y,betaprime))/(1.0-pow(y,betaprime))-(4.0*kprime*betaprime*pow(y,betaprime)*(1.0-2.0*pow(y,betaprime)))/(1.0+kprime*pow(y,betaprime)*(1.0-pow(y,betaprime))));
		}

		double fraction = 1.0/(Ekinbullet+0.938)*(Fnumu1+2.0*Fnumu2)/2.0;
		sigma = sigmatot*fraction;

	}
	return (sigma);
}
