#include "myCfitsio.h"

namespace myCfitsio {
  
  void fits_create_file(fitsfile **output, const std::string& filename ){
    int status = 0;
    if ( fits_create_file(output,filename.c_str(),&status) ) fits_report_error(stderr, status);
    return;
  };
  
  void fits_close_file(fitsfile * output){
    int status = 0;
    if ( fits_close_file(output, &status) ) fits_report_error(stderr, status);
    return;
  };
  
  void fits_create_tbl(fitsfile * output,
		       const long& nrows,
		       const int& nnu,
		       const std::string& units){ 
    int status = 0;
    int tfields = nnu;
    
    char extname[] = "BINTABLE"; /* extension name */
    char** ttype = NULL;
    char** tform = NULL;
    char** tunit = NULL;
    char dummy[32];
    
    ttype = new char*[nnu];
    tform = new char*[nnu];
    tunit = new char*[nnu];
    for (int i = 0; i < nnu; i++) {
      ttype[i] = new char[32];
      tform[i] = new char[32];
      tunit[i] = new char[32];
    }
    
    for (int i = 0; i < nnu; i++) {
      sprintf(dummy,"%d",i+1);
      strcpy(ttype[i],dummy);
      strcpy(tform[i], "1E");
      strcpy(tunit[i],units.c_str());//"erg/cm^2/s/sr/Hz");
    }
    
    if ( fits_create_tbl(output,BINARY_TBL,nrows,tfields,ttype,tform,tunit,extname,&status) ) 
      fits_report_error(stderr, status);
    
    for (int i = 0; i < nnu; i++) {
      delete [] ttype[i];
      delete [] tform[i];
      delete [] tunit[i];
    }
    delete [] ttype;
    delete [] tform;
    delete [] tunit;
    ttype = NULL;
    tform = NULL;
    tunit = NULL;

    return;
  }
  void fits_write_col(fitsfile *output,
		      const int& iFreq,
		      const long& nrows,
		      float * signal){
    int status = 0;

    long firstrow  = 1;  /* first row in table to write   */
    long firstelem = 1;  /* first element in row  (ignored in ASCII tables)  */
    
    if ( fits_write_col(output,TFLOAT,iFreq+1,firstrow,firstelem,nrows,signal,&status) ) 
      fits_report_error(stderr, status);
    
    return;
  }
  
  void fits_create_img(fitsfile *output,
		       const int& naxis,
		       const int& bitpix,
		       long * size_axis){
    int status = 0;
    
    if ( fits_create_img(output, bitpix, naxis, size_axis, &status) ) 
      fits_report_error(stderr, status); 
    
    return;
  }
  
  void fits_write_img(fitsfile *output,
		      const int& nelements,
		      float * result){
    int status = 0;
    int fpixel = 1;

    if ( fits_write_img(output,TFLOAT,fpixel,nelements,result,&status) )    
      fits_report_error(stderr, status);
    
    return;
  }

  void fits_write_date(fitsfile *output){
    int status = 0;

    if ( fits_write_date(output,&status) ) 
      fits_report_error(stderr, status);
    
    return;
  }
  
  void fits_write_key(fitsfile *output,
		      const std::string& name,
		      const std::string& type){
    int status = 0;
    
    char *name1;//[] = "PROCESS";//name.c_str();
    char *type1;//[] = "temp";//type.c_srt();
    
    name1 = new char[name.size()+1];
    type1 = new char[type.size()+1];

    memcpy(name1,name.c_str(),name.size()+1);
    memcpy(type1,type.c_str(),type.size()+1);
    
    if ( fits_write_key(output,TSTRING,name1,type1,NULL,&status) ) 
      fits_report_error(stderr, status);

    delete [] name1;
    delete [] type1;
    
    return;
  }

  void fits_write_key(fitsfile *output,
		      const std::string& name,
		      const std::string& type,
		      const std::string& comment){
    int status = 0;
    
    char *name1;
    char *type1;
    char *comment1;
    
    name1 = new char[name.size()+1];
    type1 = new char[type.size()+1];
    comment1 = new char[comment.size()+1];
    
    memcpy(name1,name.c_str(),name.size()+1);
    memcpy(type1,type.c_str(),type.size()+1);
    memcpy(comment1,comment.c_str(),comment.size()+1);
    
    if ( fits_write_key(output,TSTRING,name1,type1,comment1,&status) ) 
      fits_report_error(stderr, status);
    
    delete [] name1;
    delete [] type1;
    delete [] comment1;
    
    return;
  }
  
  void fits_write_key(fitsfile *output,
		      const std::string& name,
		      int& value){
    int status = 0;
    
    char *name1;
    name1 = new char[name.size()+1];
    memcpy(name1,name.c_str(),name.size()+1);
    
    if ( fits_write_key(output,TINT,name1,&value,NULL,&status) ) 
      fits_report_error(stderr, status);
    
    delete [] name1;
    
    return;
  }
  
  void fits_write_key(fitsfile *output,
		      const std::string& name,
		      long& value,
		      const std::string& comment){
    int status = 0;
    
    char *name1; 
    char *comment1;
    
    name1 = new char[name.size()+1];  
    comment1 = new char[comment.size()+1];

    memcpy(name1,name.c_str(),name.size()+1);
    memcpy(comment1,comment.c_str(),comment.size()+1);

    if ( fits_write_key(output,TLONG,name1,&value,comment1,&status) ) 
      fits_report_error(stderr, status);
    
    delete [] name1;
    delete [] comment1;

    return;
  }   
  
  void fits_write_key(fitsfile *output,
		      const std::string& name,
		      double& value){
    int status = 0;
    
    char *name1;
    name1 = new char[name.size()+1];
    memcpy(name1,name.c_str(),name.size()+1);
    
    if ( fits_write_key(output,TDOUBLE,name1,&value,NULL,&status) ) 
      fits_report_error(stderr, status);
    
    delete [] name1;
    
    return;
  }
   
  void fits_write_key(fitsfile *output,
		      const std::string& name,
		      double& value,
		      const std::string& comment){
    int status = 0;
    
    char *name1; 
    char *comment1;
    
    name1 = new char[name.size()+1];  
    comment1 = new char[comment.size()+1];

    memcpy(name1,name.c_str(),name.size()+1);
    memcpy(comment1,comment.c_str(),comment.size()+1);

    if ( fits_write_key(output,TDOUBLE,name1,&value,comment1,&status) ) 
      fits_report_error(stderr, status);
    
    delete [] name1;
    delete [] comment1;

    return;
  }   
   
} // namespace
