#include "outputManager.h"
#include "myCfitsio.h"

namespace hesky {
  
  void OutputManager::createSynchroOnFITSfirstHeader() {
  
    double minFreqInMhz = minFrequency/1e6;
    double maxFreqInMhz = maxFrequency/1e6;
    
    const int naxis = 1;
    long size_axis[naxis] = {1};
    
    float * result = new float[1]();  
    
    myCfitsio::fits_create_img(output_synchrotron_maps,naxis,FLOAT_IMG,size_axis);
    
    myCfitsio::fits_write_date(output_synchrotron_maps);
    
    myCfitsio::fits_write_key(output_synchrotron_maps,"PIXTYPE","HEALPIX","HEALPIX Pixelisation");
    myCfitsio::fits_write_key(output_synchrotron_maps,"ORDERING","RING    ","Pixel ordering scheme, either RING or NESTED");
    myCfitsio::fits_write_key(output_synchrotron_maps,"NSIDE",nside,"Resolution parameter for HEALPIX");
    myCfitsio::fits_write_key(output_synchrotron_maps,"RES",angres,"Resolution of map");
    myCfitsio::fits_write_key(output_synchrotron_maps,"MINFREQ",minFreqInMhz,"[Mhz]");
    myCfitsio::fits_write_key(output_synchrotron_maps,"MAXFREQ",maxFreqInMhz,"[Mhz]");
    myCfitsio::fits_write_key(output_synchrotron_maps,"RATFREQ",ratioFrequency,"Ratio between freq bins");
    myCfitsio::fits_write_key(output_synchrotron_maps,"NFREQ",nFrequency);
    
    myCfitsio::fits_write_img(output_synchrotron_maps,1,result);
    
    delete [] result;
    
    return;
  }
  
  void OutputManager::saveSynchroMapsOnFITSwithHealpix() {
    
    createSynchroOnFITSfirstHeader();

    if ( in->outputFITStype == "Binary" )
      saveSynchroMapsOnFITSwithHealpixBinaryMode();
    else if ( in->outputFITStype == "Image" )
      saveSynchroMapsOnFITSwithHealpixImageMode();
    
    return;
  }
  
  void OutputManager::saveSynchroMapsOnFITSwithHealpixImageMode() {
    
    const int naxis = 2;
    const long nelements = npix*nFrequency;
    long size_axis[naxis] = {npix,nFrequency};
    long index = 0;
    
    float * signal = new float[nelements]();
    
    myCfitsio::fits_create_img(output_synchrotron_maps,naxis,FLOAT_IMG,size_axis);
    myCfitsio::fits_write_key(output_synchrotron_maps,"PROCESS","SynchParallel");
    myCfitsio::fits_write_key(output_synchrotron_maps,"UNITS","erg/cm^2/s/sr/Hz");
    
    index = 0;
    for ( int iFreq=0;iFreq<nFrequency;iFreq++ )
      for ( unsigned long i=0;i<npix;i++ ){ 
	signal[index] = float(he->getSynchrotronMaps()->getParallelToHealpix(i,iFreq));
	index++;
      }
    myCfitsio::fits_write_img(output_synchrotron_maps,nelements,signal);
    
    myCfitsio::fits_create_img(output_synchrotron_maps,naxis,FLOAT_IMG,size_axis);
    myCfitsio::fits_write_key(output_synchrotron_maps,"PROCESS","SynchPerpendicular");
    myCfitsio::fits_write_key(output_synchrotron_maps,"UNITS","erg/cm^2/s/sr/Hz");

    index = 0;
    for ( int iFreq=0;iFreq<nFrequency;iFreq++ )
      for ( unsigned long i=0;i<npix;i++ ){ 
	signal[index] = float(he->getSynchrotronMaps()->getPerpendicularToHealpix(i,iFreq));
	index++;
      }
    myCfitsio::fits_write_img(output_synchrotron_maps,nelements,signal);
    
    myCfitsio::fits_create_img(output_synchrotron_maps,naxis,FLOAT_IMG,size_axis);
    myCfitsio::fits_write_key(output_synchrotron_maps,"PROCESS","SynchRandom");
    myCfitsio::fits_write_key(output_synchrotron_maps,"UNITS","erg/cm^2/s/sr/Hz");
    
    index = 0;
    for ( int iFreq=0;iFreq<nFrequency;iFreq++ )
      for ( unsigned long i=0;i<npix;i++ ){ 
	signal[index] = float(he->getSynchrotronMaps()->getRandomToHealpix(i,iFreq));
	index++;
      }
    myCfitsio::fits_write_img(output_synchrotron_maps,nelements,signal);
    
    return;
  }
  
  void OutputManager::saveSynchroMapsOnFITSwithHealpixBinaryMode() {
    
    const long nrows = npix;
    float * signal = new float[npix]();
    
    myCfitsio::fits_create_tbl(output_synchrotron_maps,nrows,nFrequency,"erg/cm^2/s/sr/Hz");
    myCfitsio::fits_write_key(output_synchrotron_maps,"PROCESS","SynchParallel");
    
    for ( int iFreq=0;iFreq<nFrequency;iFreq++ ){
      for ( unsigned long i=0;i<npix;i++ ) 
	signal[i] = float(he->getSynchrotronMaps()->getParallelToHealpix(i,iFreq));
      
      myCfitsio::fits_write_col(output_synchrotron_maps,iFreq,nrows,signal);
    } 
    
    myCfitsio::fits_create_tbl(output_synchrotron_maps,nrows,nFrequency,"erg/cm^2/s/sr/Hz");
    myCfitsio::fits_write_key(output_synchrotron_maps,"PROCESS","SynchPerpendicular");
    
    for ( int iFreq=0;iFreq<nFrequency;iFreq++ ){
      for ( unsigned long i=0;i<npix;i++ ) 
	signal[i] = float(he->getSynchrotronMaps()->getPerpendicularToHealpix(i,iFreq));
      
      myCfitsio::fits_write_col(output_synchrotron_maps,iFreq,nrows,signal);
    }
        
    myCfitsio::fits_create_tbl(output_synchrotron_maps,nrows,nFrequency,"erg/cm^2/s/sr/Hz");
    myCfitsio::fits_write_key(output_synchrotron_maps,"PROCESS","SynchRandom");

    for ( int iFreq=0;iFreq<nFrequency;iFreq++ ){
      for ( unsigned long i=0;i<npix;i++ ) 
	signal[i] = float(he->getSynchrotronMaps()->getRandomToHealpix(i,iFreq));

      myCfitsio::fits_write_col(output_synchrotron_maps,iFreq,nrows,signal);
    }
    
    delete [] signal;
    return ;
  }
  
  void OutputManager::saveSynchroMapsOnFITSwithGrid(){
    
    //if (!(inp->iHealpix)) {
    //double grid_lmin = l_grid.front();
    //double grid_lmax = l_grid.back();
    //double grid_bmin = b_grid.front();
    //double grid_bmax = b_grid.back();

    //char str[] = "GRID";
    
    //if (fits_write_key(output_ptr, TDOUBLE, "lmin",    &grid_lmin, NULL, &status)) fits_report_error(stderr, status); 
    //if (fits_write_key(output_ptr, TDOUBLE, "lmax",    &grid_lmax, NULL, &status)) fits_report_error(stderr, status); 
    //if (fits_write_key(output_ptr, TDOUBLE, "bmin",    &grid_bmin, NULL, &status)) fits_report_error(stderr, status); 
    //if (fits_write_key(output_ptr, TDOUBLE, "bmax",    &grid_bmax, NULL, &status)) fits_report_error(stderr, status); 
    //if (fits_write_key(output_ptr, TDOUBLE, "dl",      &Dl,        NULL, &status)) fits_report_error(stderr, status); 
    //if (fits_write_key(output_ptr, TDOUBLE, "db",      &Db,        NULL, &status)) fits_report_error(stderr, status); 
    //if (fits_write_key(output_ptr, TINT,    "nl",      &nl,        NULL, &status)) fits_report_error(stderr, status); 
    //if (fits_write_key(output_ptr, TINT,    "nb",      &nb,        NULL, &status)) fits_report_error(stderr, status); 
    //if (fits_write_key(output_ptr, TSTRING, "PIXTYPE", str, "GRID", &status)) fits_report_error(stderr, status);
    //}
 
    //   if (!(inp->iHealpix)) {
        
//     int status = 0;
//     const int naxis = 3;
//     long output_size_axes[naxis] = {nnu,nl,nb};
//     int nelements = output_size_axes[0]*output_size_axes[1]*output_size_axes[2];
//     int fpixel = 1;
//     int bitpix = FLOAT_IMG;
//     float* result = new float[nelements]();
        
//     if (fits_create_img(output_ptr, bitpix, naxis, output_size_axes, &status)) fits_report_error(stderr, status); 
        
//     /* Write one component */  
//     char type1[] = "Synch_par";
//     if (fits_write_key(output_ptr, TSTRING, "PROCESS", &type1, NULL, &status)) fits_report_error(stderr, status);
        
//     int ind = 0;
        
//     for (int j = 0; j < nb; ++j) {
//       for (int i = 0; i < nl; ++i) {
// 	for (int iEgamma = 0; iEgamma < nnu; iEgamma++) {
// 	  result[ind] = float(flux_par[index_flux(i,j,iEgamma)]/(4.0*M_PI));
// 	  ind++;
// 	}
//       }
//     }
//     if (fits_write_img(output_ptr, TFLOAT, fpixel, nelements, result, &status)) fits_report_error(stderr, status);
//     /* End write one component*/
        
//     /* Write one component */  
//     char type2[] = "Synch_perp";
//     if (fits_write_key(output_ptr, TSTRING, "PROCESS", &type2, NULL, &status)) fits_report_error(stderr, status);
        
//     ind = 0;
        
//     for (int j = 0; j < nb; ++j) {
//       for (int i = 0; i < nl; ++i) {
// 	for (int iEgamma = 0; iEgamma < nnu; iEgamma++) {
// 	  result[ind] = float(flux_perp[index_flux(i,j,iEgamma)]/(4.0*M_PI));
// 	  ind++;
// 	}
//       }
//     }
//     if (fits_write_img(output_ptr, TFLOAT, fpixel, nelements, result, &status)) fits_report_error(stderr, status);
//     /* End write one component*/
        
//     /* Write one component */  
//     char type3[] = "Synch_rand";
//     if (fits_write_key(output_ptr, TSTRING, "PROCESS", &type3, NULL, &status)) fits_report_error(stderr, status);
        
//     ind = 0;
        
//     for (int j = 0; j < nb; ++j) {
//       for (int i = 0; i < nl; ++i) {
// 	for (int iEgamma = 0; iEgamma < nnu; iEgamma++) {
// 	  result[ind] = float(flux_rand[index_flux(i,j,iEgamma)]/(4.0*M_PI));
// 	  ind++;
// 	}
//       }
//     }
//     if (fits_write_img(output_ptr, TFLOAT, fpixel, nelements, result, &status)) fits_report_error(stderr, status);
//     /* End write one component*/
        
//     delete [] result;
//     result = NULL;
//   } // if (!iHealpix)
//   else {
    
    return;
  };

} // namespace
