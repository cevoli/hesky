#include "outputManager.h"

namespace hesky {
  
  void OutputManager::saveMapsOnASCII(){
    
    std::cout<<"Printing gamma-ray maps on ASCII file..."<<std::endl;

    std::ofstream myfile (outputGammaFilenameASCII.c_str());
    if (myfile.is_open())
      {
	myfile << "This is a line.\n";
	myfile << "This is another line.\n";
	myfile.close();
      }
    else std::cout << "Unable to open file" << std::endl;

    return;
  }
  
} // namespace hesky
  
