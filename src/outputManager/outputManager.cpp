#include "outputManager.h"

namespace hesky {

  OutputManager::OutputManager(HESKY * he_) {

    he = he_;
    in = he_->getInput();
    
    nl = he_->getGalaxy()->GetNl();
    nb = he_->getGalaxy()->GetNb();
    npix = he_->getGalaxy()->GetNpix();
    nrows = he_->getGalaxy()->GetNpix();
    nside = he_->getGalaxy()->GetNside();
    deltal = he_->getGalaxy()->GetDeltal();
    deltab = he_->getGalaxy()->GetDeltab();
    angres = he_->getGalaxy()->GetAngularResolution();
    bGrid = he_->getGalaxy()->GetB();
    lGrid = he_->getGalaxy()->GetL();
    rGrid = he_->getGalaxy()->GetR();
    zGrid = he_->getGalaxy()->GetZ();
    
    if ( in->doComputeGammaMaps ){
      nE = he_->getGammaMaps()->GetNE();
      nr = he_->getGammaMaps()->GetNr();
      nz = he_->getGammaMaps()->GetNz();
      nComp = he_->getGammaMaps()->GetNMapComponents();
      Emin = he_->getGammaMaps()->GetEmin();
      Emax = he_->getGammaMaps()->GetEmax();
      Efactor = he_->getGammaMaps()->GetEratio();
      makeGammaFilenames();
    }
    if ( in->doComputeSynchrotronMaps ){
      nFrequency = he_->getSynchrotronMaps()->getNFrequency();
      minFrequency = he_->getSynchrotronMaps()->getMinFrequency();
      maxFrequency = he_->getSynchrotronMaps()->getMaxFrequency();
      ratioFrequency = he_->getSynchrotronMaps()->getRatioFrequency();
      makeSynchrotronFilenames();
    }
  }    
  
  OutputManager::~OutputManager()
  {
    bGrid.clear();
    lGrid.clear();
    rGrid.clear();
    zGrid.clear();
  }
  
  std::string OutputManager::getProcessToString(const int& nComp, const int& iComp)
  {
    std::string type;
    
    if (nComp == 1) 
      type = "pi0";
    
    else if (nComp == 2) {
      if (iComp == 0) 
	type = "brems";
      else if (iComp == 1)
	type = "IC";
    }
    
    else if (nComp == 3) {
      if (iComp == 0) 
	type = "pi0";
      else if (iComp == 1) 
	type = "brems";
      else if (iComp == 2) 
	type = "IC";
    }
    
    return type;
  }  

  void OutputManager::makeGammaFilenames() {
    outputGammaFilenameASCII = initFilename();
    outputGammaFilenameASCII += ".txt";
    
    outputGammaFilenameFITS = initFilename();
    outputGammaFilenameFITS += ".fits.gz";
    
    outputGammaFilenameEmissivityFITS = initFilename();
    outputGammaFilenameEmissivityFITS += "_emissivity"; 
    outputGammaFilenameEmissivityFITS += ".fits.gz";
    
    std::cout<<outputGammaFilenameASCII<<std::endl;
    std::cout<<outputGammaFilenameFITS<<std::endl;
    std::cout<<outputGammaFilenameEmissivityFITS<<std::endl;
    return;
  }
  
  void OutputManager::makeSynchrotronFilenames() {
    outputSynchroFilenameASCII = initFilename();
    outputSynchroFilenameASCII += "_synchrotron.txt";
    
    outputSynchroFilenameFITS = initFilename();
    outputSynchroFilenameFITS += "_synchrotron.fits.gz";
    
    std::cout<<outputSynchroFilenameASCII<<std::endl;
    std::cout<<outputSynchroFilenameFITS<<std::endl;
    return;
  }
  
}  // namespace 
