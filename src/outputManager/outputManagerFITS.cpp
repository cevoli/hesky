#include "outputManager.h"
#include "myCfitsio.h"

namespace hesky {

  void OutputManager::saveMapsOnFITS()
  {
    if ( in->doComputeGammaMaps ){
      std::cout<<"Printing gamma-ray maps on FITS file..."<<std::endl;
      
      int status = 0;
      
      if ( fits_create_file(&output_gamma_maps, outputGammaFilenameFITS.c_str(), &status) ) 
	fits_report_error(stderr, status);
      
      if (in->doUseHealpix)
	saveMapsOnFITSwithHealpix();
      else
	saveMapsOnFITSwithGrid();

      if ( fits_close_file(output_gamma_maps, &status) )
	fits_report_error(stderr, status);
      
      std::cout<<"...done!"<<std::endl;
    }
    
    if ( in->doComputeSynchrotronMaps ){
      std::cout<<"Printing synchrotron maps on FITS file..."<<std::endl;

      myCfitsio::fits_create_file(&output_synchrotron_maps,outputSynchroFilenameFITS);
      
      if (in->doUseHealpix)
	saveSynchroMapsOnFITSwithHealpix();
      else
	saveSynchroMapsOnFITSwithGrid();
      
      myCfitsio::fits_close_file(output_synchrotron_maps);

      std::cout<<"...done!"<<std::endl;
    }
    
    return ;
  }
  
  void OutputManager::saveEmissivityOnFITS()
  {
    std::cout<<"Printing emission maps ... "<<std::endl;
    
    if ( in->doComputeGammaMaps ){
      myCfitsio::fits_create_file(&output_emissivity,outputGammaFilenameEmissivityFITS);
      
      const int naxis = 3;    
      const long nelements = nE*nr*nz;
      long size_axis[naxis] = {nE,nr,nz};
      long index = 0;

      float * result = new float[nelements]();

      myCfitsio::fits_create_img(output_emissivity,naxis,FLOAT_IMG,size_axis);
      {
	myCfitsio::fits_write_key(output_emissivity,"rmin",rGrid.front());
	myCfitsio::fits_write_key(output_emissivity,"rmax",rGrid.back());
	myCfitsio::fits_write_key(output_emissivity,"zmin",zGrid.front());
	myCfitsio::fits_write_key(output_emissivity,"zmax",zGrid.back());
	myCfitsio::fits_write_key(output_emissivity,"Emin",Emin);
	myCfitsio::fits_write_key(output_emissivity,"Emax",Emax);
	myCfitsio::fits_write_key(output_emissivity,"nr",nr);
	myCfitsio::fits_write_key(output_emissivity,"nz",nz);
	myCfitsio::fits_write_key(output_emissivity,"nE",nE);
	myCfitsio::fits_write_key(output_emissivity,"Efactor",Efactor);
      }
      myCfitsio::fits_write_img(output_emissivity,nelements,result);

      myCfitsio::fits_create_img(output_emissivity,naxis,FLOAT_IMG,size_axis);
      {
	myCfitsio::fits_write_key(output_emissivity,"PIXTYPE","GRID");
	myCfitsio::fits_write_key(output_emissivity,"BUNITS","ph/sr-s-MeV");

	index = 0;
	for ( int j=0;j<nz;++j ){
	  for ( int i=0;i<nr;++i ){      
	    for ( int iEgamma=0;iEgamma<nE;iEgamma++ ){
	      result[index] = float(he->getGammaMaps()->getEmissivityToOutput(i,j,iEgamma));
	      index++;
	    }
	  }
	}
      }
      myCfitsio::fits_write_img(output_emissivity,nelements,result);
      
      myCfitsio::fits_close_file(output_emissivity);
    }
    std::cout<<"... done!"<<std::endl;
    return;
  }

  void OutputManager::saveMapsOnFITSwithGrid(){
    
    double grid_lmin = lGrid.front();
    double grid_lmax = lGrid.back();
    double grid_bmin = bGrid.front();
    double grid_bmax = bGrid.back();   
    
    int status = 0;
    const int naxis = 3;
    long outputSizeAxes[naxis] = {nE,nl,nb};
    int nelements = outputSizeAxes[0]*outputSizeAxes[1]*outputSizeAxes[2];
    int fpixel = 1;
    int bitpix = FLOAT_IMG;
      
    if ( fits_create_img(output_gamma_maps, bitpix, naxis, outputSizeAxes, &status) ) 
      fits_report_error(stderr, status); 
    
    float * result = new float[nelements]();
    
    char str[] = "GRID";
    
    if (fits_write_key(output_gamma_maps, TDOUBLE, "lmin", &grid_lmin, NULL, &status)) 
      fits_report_error(stderr, status); 
    if (fits_write_key(output_gamma_maps, TDOUBLE, "lmax", &grid_lmax, NULL, &status)) 
      fits_report_error(stderr, status); 
    if (fits_write_key(output_gamma_maps, TDOUBLE, "bmin", &grid_bmin, NULL, &status)) 
      fits_report_error(stderr, status); 
    if (fits_write_key(output_gamma_maps, TDOUBLE, "bmax", &grid_bmax, NULL, &status)) 
      fits_report_error(stderr, status); 
    if (fits_write_key(output_gamma_maps, TDOUBLE, "Emin", &Emin, NULL, &status)) 
      fits_report_error(stderr, status); 
    if (fits_write_key(output_gamma_maps, TDOUBLE, "Emax", &Emax, NULL, &status)) 
      fits_report_error(stderr, status); 
    if (fits_write_key(output_gamma_maps, TDOUBLE, "dl", &deltal, NULL, &status)) 
      fits_report_error(stderr, status); 
    if (fits_write_key(output_gamma_maps, TDOUBLE, "db", &deltab, NULL, &status)) 
      fits_report_error(stderr, status); 
    if (fits_write_key(output_gamma_maps, TINT, "nl", &nl, NULL, &status)) 
      fits_report_error(stderr, status); 
    if (fits_write_key(output_gamma_maps, TINT, "nb", &nb, NULL, &status)) 
      fits_report_error(stderr, status); 
    if (fits_write_key(output_gamma_maps, TINT, "nE", &nE, NULL, &status)) 
      fits_report_error(stderr, status); 
    if (fits_write_key(output_gamma_maps, TDOUBLE, "Efactor", &Efactor, NULL, &status)) 
      fits_report_error(stderr, status); 
    if (fits_write_img(output_gamma_maps, TFLOAT, fpixel, nelements, result, &status))    
      fits_report_error(stderr, status);
    if (fits_write_key(output_gamma_maps, TSTRING, "PIXTYPE", str, "GRID", &status)) 
      fits_report_error(stderr, status);
    if (fits_write_key(output_gamma_maps, TSTRING, "BUNIT", str, "ph/cm^2-sr-s-MeV", &status)) 
      fits_report_error(stderr, status);
    
    for ( int iComp=0;iComp<nComp;iComp++ ){
      
      if ( fits_create_img(output_gamma_maps, bitpix, naxis, outputSizeAxes, &status) ) 
	fits_report_error(stderr, status); 
      
      std::string type = getProcessToString(nComp,iComp);
      
      if ( fits_write_key(output_gamma_maps, TSTRING, "PROCESS", &type, NULL, &status) ) 
	fits_report_error(stderr, status);
      
      int index = 0;
      
      for ( int j=0;j<nb;++j ){
	for ( int i=0;i<nl;++i ){
	  for ( int iEgamma=0;iEgamma<nE;iEgamma++ ){
	    //result[index] = float(dscm*flux[index_flux(iComp,i,j,iEgamma)]*pow(E_grid[iEgamma],2)); 
	    result[index] = 1;//fload(losStepInCm*flux[index_flux(iComp,i,j,iEgamma)]*pow(E_grid[iEgamma],2)); 
	    index++;
	  }
	}
      }
      if ( fits_write_img(output_gamma_maps, TFLOAT, fpixel, nelements, result, &status) ) 
	fits_report_error(stderr, status);
    }
    
    if (in->doComputeAnisotropicIC) {
      
      if ( fits_create_img(output_gamma_maps, bitpix, naxis, outputSizeAxes, &status) ) 
	fits_report_error(stderr, status); 
      char type[] = "IC_ANISO";
      if ( fits_write_key(output_gamma_maps, TSTRING, "PROCESS", &type, NULL, &status) ) 
	fits_report_error(stderr, status); 
      
      int index = 0;
      
      for ( int j=0;j<nb;++j ){
	for ( int i=0;i<nl;++i ){
	  for ( int iEgamma=0;iEgamma<nE;iEgamma++ ){
	    result[index] = 1.0;//float((aniso[index_pgr_aniso_iso(i,j,iEgamma)]+dscm*iso[index_pgr_aniso_iso(i,j,iEgamma)])*pow(E_grid[iEgamma],2)); TBD!!!!
	    index++;
	  }
	}
      }
      if ( fits_write_img(output_gamma_maps, TFLOAT, fpixel, nelements, result, &status) ) 
	fits_report_error(stderr, status);
    }
    
    delete [] result;
    result = NULL;
    
    return;
  }
    
  void OutputManager::saveMapsOnFITSwithHealpix(){

    int status, 
      bitpix = 8,
      tfields = nE;
    
    long firstrow, 
      firstelem, 
      naxes[] = {0,0};
    
    char order[9];                 /* HEALPix ordering */
    char extname[] = "BINTABLE";   /* extension name */
    char** ttype = NULL;
    char** tform = NULL;
    char** tunit = NULL;
    char dummy[32];
    
    ttype = new char*[nE];
    tform = new char*[nE];
    tunit = new char*[nE];
    
    for (int i = 0; i < nE; i++) {
      ttype[i] = new char[32];
      tform[i] = new char[32];
      tunit[i] = new char[32];
    }
      
    for (int i = 0; i < nE; i++) {
      sprintf(dummy,"%d",i+1);
	strcpy(ttype[i],dummy);
	strcpy(tform[i],"1E");
	strcpy(tunit[i],"Gev/cm^2/s/sr");
      }
    
    float * signal = new float[npix]();
    
    status = 0;
    
    if ( fits_create_img(output_gamma_maps, bitpix, 0, naxes, &status) ) 
      fits_report_error(stderr, status);
    
    if ( fits_write_date(output_gamma_maps, &status) ) 
      fits_report_error(stderr, status);
    
    char str[] = "HEALPIX";
    
    if ( fits_write_key(output_gamma_maps, TSTRING, "PIXTYPE", str, "HEALPIX Pixelisation", &status) ) 
      fits_report_error(stderr, status);
    
    strcpy(order, "RING    ");
    
    if ( fits_write_key(output_gamma_maps, TSTRING, "ORDERING", order, "Pixel ordering scheme, either RING or NESTED", &status) ) 
      fits_report_error(stderr, status);
    if ( fits_write_key(output_gamma_maps, TLONG, "NSIDE", &nside, "Resolution parameter for HEALPIX", &status) ) 
      fits_report_error(stderr, status);
    if ( fits_write_key(output_gamma_maps, TDOUBLE, "ANGULAR_RES", &angres, "Resolution of map", &status) ) 
      fits_report_error(stderr, status);
    if ( fits_write_key(output_gamma_maps, TDOUBLE, "Emin", &Emin, NULL, &status) ) 
      fits_report_error(stderr, status); 
    if ( fits_write_key(output_gamma_maps, TDOUBLE, "Emax", &Emax, NULL, &status) ) 
      fits_report_error(stderr, status); 
    if ( fits_write_key(output_gamma_maps, TDOUBLE, "Efactor", &Efactor, NULL, &status) ) 
      fits_report_error(stderr, status); 
    if ( fits_write_key(output_gamma_maps, TINT, "nE", &nE, NULL, &status) ) 
      fits_report_error(stderr, status); 
    
    for ( int iComp=0;iComp<nComp;iComp++ ){
      
      /* append a new empty binary table onto the FITS file */
      if (fits_create_tbl(output_gamma_maps, BINARY_TBL, nrows, tfields, ttype, tform, tunit, extname, &status)) 
	fits_report_error(stderr, status);
      
      std::string processStr = getProcessToString(nComp,iComp);
      char processChar[10];
      strcpy(processChar,processStr.c_str());
      
      if ( fits_write_key(output_gamma_maps, TSTRING, "PROCESS", &processChar, NULL, &status) ) 
	fits_report_error(stderr, status);
      
      for ( int iEgamma=0;iEgamma<nE;iEgamma++ ){
	firstrow  = 1;  /* first row in table to write */
	firstelem = 1;  /* first element in row  (ignored in ASCII tables) */
	
	for ( unsigned long i=0;i<npix;i++ )
	  signal[i] = float(losStepInCm*he->getGammaMaps()->getE2FluxToHealpix(iComp,i,iEgamma)); 
	
	if ( fits_write_col(output_gamma_maps, TFLOAT, iEgamma+1, firstrow, firstelem, nrows, signal, &status) ) 
	  fits_report_error(stderr, status);
	
      } // iEgamma
    } // iComp
    
    if ( in->doComputeAnisotropicIC ){
      
      /* append a new empty binary table onto the FITS file */
      if ( fits_create_tbl(output_gamma_maps, BINARY_TBL, nrows, tfields, ttype, tform, tunit, extname, &status) ) 
	fits_report_error(stderr, status);
      
      char type[] = "IC_ANISO";
      
      if ( fits_write_key(output_gamma_maps, TSTRING, "PROCESS", &type, NULL, &status) ) 
	fits_report_error(stderr, status); 
      
      for ( int iEgamma=0;iEgamma<nE;iEgamma++ ){
	firstrow  = 1;  /* first row in table to write */
	firstelem = 1;  /* first element in row  (ignored in ASCII tables) */
	
	//for ( unsigned long i=0;i<npix;i++ ) 
	//signal[i] = float((aniso_healpix[index_healpix(i,iEgamma)]
	//+dscm*iso_healpix[index_healpix(i,iEgamma)])*pow(E_grid[iEgamma],2));
	
	if ( fits_write_col(output_gamma_maps, TFLOAT, iEgamma+1, firstrow, firstelem, nrows, signal, &status) ) 
	  fits_report_error(stderr, status);
	
      } // iEgamma
    }
    
    delete [] signal;
    for (int i = 0; i < nE; i++) {
      delete [] ttype[i];
      delete [] tform[i];
      delete [] tunit[i];
    }
    delete [] ttype;
    delete [] tform;
    delete [] tunit;
    ttype = NULL;
    tform = NULL;
    tunit = NULL;
    signal = NULL;
    
    return;
  }

} // namespace
