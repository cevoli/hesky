#include "utils.h"

extern "C" double pp_meson_(double*,double*,int*,int*,int*);
extern "C" double bremss_spec_(double*,double*,int*,int*,double*);
extern "C" void aic_(int*,int*,int*,double*,double*,double*,double*,double*,double*,double*,double*,double*,double*);

namespace Utils {

  // From Healpix.
  void pix2ang_ring( long nside, long ipix, double *theta, double *phi) {
    /*
      c=======================================================================
      c     gives theta and phi corresponding to pixel ipix (RING) 
      c     for a parameter nside
      c=======================================================================
    */
    
    int nl2, nl4, npix, ncap, iring, iphi, ip, ipix1;
    double  fact1, fact2, fodd, hip, fihip;
    double PI=M_PI;

    //      PARAMETER (pi     = 3.1415926535897932384626434d0)
    //      parameter (ns_max = 8192) ! 2^13 : largest nside available
    
    int ns_max=8192;
    
    if( nside<1 || nside>ns_max ) {
      fprintf(stderr, "%s (%d): nside out of range: %ld\n", __FILE__, __LINE__, nside);
      exit(0);
    }
    npix = 12*nside*nside;      // ! total number of points
    if( ipix<0 || ipix>npix-1 ) {
      fprintf(stderr, "%s (%d): ipix out of range: %ld\n", __FILE__, __LINE__, ipix);
      exit(0);
    }
    
    ipix1 = ipix + 1; // in {1, npix}
    nl2 = 2*nside;
    nl4 = 4*nside;
    ncap = 2*nside*(nside-1);// ! points in each polar cap, =0 for nside =1
    fact1 = 1.5*nside;
    fact2 = 3.0*nside*nside;
    
    if( ipix1 <= ncap ) {  //! North Polar cap -------------
        
      hip   = ipix1/2.;
      fihip = floor(hip);
      iring = (int)floor( sqrt( hip - sqrt(fihip) ) ) + 1;// ! counted from North pole
      iphi  = ipix1 - 2*iring*(iring - 1);
        
      *theta = acos( 1. - iring*iring / fact2 );
      *phi   = (1.*iphi - 0.5) * PI/(2.*iring);
    }
    else if( ipix1 <= nl2*(5*nside+1) ) {//then ! Equatorial region ------
        
      ip    = ipix1 - ncap - 1;
      iring = (int)floor( ip / nl4 ) + nside;// ! counted from North pole
      iphi  = (int)fmod(double(ip),double(nl4)) + 1;
        
      fodd  = 0.5 * (1 + fmod((double)(iring+nside),2));//  ! 1 if iring+nside is odd, 1/2 otherwise
      *theta = acos( (nl2 - iring) / fact1 );
      *phi   = (1.*iphi - fodd) * PI /(2.*nside);
    }
    else {//! South Polar cap -----------------------------------
        
      ip    = npix - ipix1 + 1;
      hip   = ip/2.;
      /* bug corrige floor instead of 1.* */
      fihip = floor(hip);
      iring = (int)floor( sqrt( hip - sqrt(fihip) ) ) + 1;//     ! counted from South pole
      iphi  = (int)(4.*iring + 1 - (ip - 2.*iring*(iring-1)));
        
      *theta = acos( -1. + iring*iring / fact2 );
      *phi   = (1.*iphi - 0.5) * PI/(2.*iring);
    }
  }

  void InitElEm(std::vector<double>& brems, 
		std::vector<double>& IC, 
		int nE, 
		int nEpr, 
		std::vector<double>& Egamma, 
		std::vector<double>& Epr, 
		double Ekf, 
		std::vector<double>& nu ) {
    
    const double factor = 1.e-21*log(Ekf);
    const double factorIC = (eV_to_erg /h_planck) *log(nu[1]/nu[0]) *log(Ekf);
    std::vector<double> Etarget(nu);
    for(unsigned int inu=0; inu < nu.size(); inu++) Etarget[inu] *= (1e-9 * h_planck * erg_to_eV);  // target photon energy in eV
    const int nnu = nu.size();

    for(int iEgamma=0; iEgamma < nE; iEgamma++) {
      double cs_HII, cs_HI, cs_He;
      int IZ1, Ne1;
      for(int ip = 0; ip < nEpr-1; ip++) {  // energy conservation check
	if(!(Epr[ip+1] + Mele <= 1.02*Mele || Epr[ip+1]-Egamma[iEgamma] <= 0.)) {
                
	  IZ1=1; Ne1=0; //              ionized hydrogen H II
	  cs_HII=bremss_spec_cc(1.e3*Egamma[iEgamma],1.e3*(Epr[ip]+Mele),IZ1,Ne1)*factor*Epr[ip];
                
	  IZ1=1; Ne1=1; //              1-electron atom  H I
	  cs_HI =bremss_spec_cc(1.e3*Egamma[iEgamma],1.e3*(Epr[ip]+Mele),IZ1,Ne1)*factor*Epr[ip];
                
	  IZ1=2; Ne1=2; //              2-electron atom  He
	  cs_He =bremss_spec_cc(1.e3*Egamma[iEgamma],1.e3*(Epr[ip]+Mele),IZ1,Ne1)*factor*Epr[ip];
                
	  int indbrems = (ip*nE+iEgamma)*2;
                
	  brems[indbrems] = cs_HI + cs_He*He_abundance; // not ionized
	  brems[indbrems+1] = cs_HII; // ionized 
	}
            
	// IC starts
	int indIC = (ip*nE+iEgamma)*nnu;
            
	for(int inu = 0; inu < nnu; inu++) {
	  //	double Etarget = nu[inu]*1e-9*h_planck * erg_to_eV;  // target photon energy in GeV
	  double Et = Etarget[inu];
                
	  if(Egamma[iEgamma] > 4.*Et *pow(1.0+Epr[ip]/Mele,2)
	     /(1.+4.*Et/Mele*(1.0+Epr[ip]/Mele))) continue;
	  IC[indIC+inu] = ICspectrum(1.0+Epr[ip]/Mele, Et/Mele, Egamma[iEgamma]/Mele)*factorIC*Epr[ip];
	}
      }
    }
    
    
    return ;
  }

  double ICspectrum(double gam, double E1, double E4) {
    double result = 0.0;
    
    if(E4/gam > 4.0*E1*gam/(1.0+4.0*E1*gam)) return 0.0;
    double q = E4/(4.0*E1*gam*gam)*(1.0+4.0*E1*gam);
    if(4.0*E1*gam < 1.e10) q = E4/(4.0*E1*gam*(gam-E4));
    result = 2.0*q*log(q)+(1.0+2.0*q)*(1.0-q) + (1.0-q)/2.0*pow(4.0*E1*gam*q,2)/(1.0+4.0*E1*gam*q);
    result *= M_PI*Rele*Rele/Mele*2.0/E1/gam/gam;//  ! eq.(12)
    
    return result;
  }

  void InitPionEm(std::vector<double>& pionsp, 
		  int nE, 
		  int nEpr, 
		  std::vector<double>& Egamma, 
		  std::vector<double>& Epr, 
		  double Ekf, 
		  int NA1, 
		  hesky::Input * inp ){
    
    const double Pth0=.780; //GeV/amu,Pth0-threshold momentum
    const double factor = log(Ekf)*1.e-24;
    // cparamlib is not thread safe!
    for(int iEgamma = 0; iEgamma < nE; iEgamma++) {
      double cs_HI,cs_He;
      int NA2, key1 = 0;
      double Eg = Egamma[iEgamma];
      //cout << Eg << endl;
      for(int ip = 0; ip < nEpr-1; ip++) {  

	double Ekinbullet = Epr[ip];
	double Ekinbullet1 = Epr[ip+1];
	double pbullet = NA1*sqrt(Ekinbullet*Ekinbullet + 2.0*mp*Ekinbullet);
	double pbullet1 = NA1*sqrt(Ekinbullet1*Ekinbullet1 + 2.0*mp*Ekinbullet1);
            
	if(pbullet1/NA1 <= Pth0) continue; // energy conservation check
	/*
	  if (gy == Kamae && NA1 == 1) {
	  KamaeYields::gamma_param_nd(Ekinbullet, a);
	  KamaeYields::gamma_param_diff(Ekinbullet, b);
	  KamaeYields::gamma_param_delta(Ekinbullet, c);
	  KamaeYields::gamma_param_res(Ekinbullet, d);
	  }
	*/
	NA2=1; // target HI          
	
	cs_HI = 0.0;
	cs_HI = (NA1 == 1 && inp->gammaYieldModel == KamaeGammaYield) ? 
	  KamaeYields::GetSigma(Eg, Ekinbullet, ID_GAMMA) : pp_meson_cc( Eg, pbullet, NA1, NA2, key1 );
            
	//if (NA1==1 && Eg > 15.0 && Eg < 16.0) 
	//cout << Ekinbullet << " " << KamaeYields::GetSigma(Eg, Ekinbullet, ID_GAMMA) << " ";
	//cout << pp_meson_cc( Eg, pbullet, NA1, NA2, key1 ) << endl;            
	
	NA2=4; // target He
	pionsp[ip*nE+iEgamma] = (cs_HI + He_abundance*pp_meson_cc( Eg, pbullet, NA1, NA2, key1 ))*Ekinbullet*factor;
            
      }
    }
    
    return ;
  }

  void InitPionEm(std::vector<double>& pionsp, 
		  std::vector<double>& pionspbar, 
		  int nE, 
		  int nEpr, 
		  std::vector<double>& Egamma, 
		  std::vector<double>& Epr, 
		  double Ekf, 
		  int NA1, 
		  hesky::Input * inp ){
    
    const double Pth0=.780; //GeV/amu,Pth0-threshold momentum
    const double factor = log(Ekf)*1.e-24;

    // cparamlib is not thread safe!
    for(int iEgamma = 0; iEgamma < nE; iEgamma++) {
      double cs_HI;
      double Eg = Egamma[iEgamma];
      // cout << Eg << endl;
      for(int ip = 0; ip < nEpr-1; ip++) {
            
	double Ekinbullet = Epr[ip];
	double Ekinbullet1 = Epr[ip+1];
	double pbullet = NA1*sqrt(Ekinbullet*Ekinbullet + 2.0*mp*Ekinbullet);
	double pbullet1 = NA1*sqrt(Ekinbullet1*Ekinbullet1 + 2.0*mp*Ekinbullet1);
            
	if(pbullet1/NA1 <= Pth0) continue; // energy conservation check
	if (Eg > Ekinbullet+0.938) continue;
            
	cs_HI = KamaeYields::GetSigma(Eg, Ekinbullet, ID_NUE) + KamaeYields::GetSigma(Eg, Ekinbullet, ID_NUMU);
	pionsp[ip*nE+iEgamma] = pow(NA1,2.0/3.0)*(1.0+He_abundance*pow(4.0,2.0/3.0))*cs_HI*Ekinbullet*factor;
            
	cs_HI = KamaeYields::GetSigma(Eg, Ekinbullet, ID_ANTINUE) + KamaeYields::GetSigma(Eg, Ekinbullet, ID_ANTINUMU);
	pionspbar[ip*nE+iEgamma] = pow(NA1,2.0/3.0)*(1.0+He_abundance*pow(4.0,2.0/3.0))*cs_HI*Ekinbullet*factor;
            
      }
    }
    
    return ;
  }

  int findParticleIndex(hesky::ParticleName particleName, std::vector<hesky::Particle*>& particleDict) {
      for (int i = 0; i < particleDict.size(); i++) {
          if (particleName == particleDict[i]->WhoAmI()) {
            return i;
          }
      }
      
      return -1;
  }

  double pp_meson_cc(double Esec, double Pp1, int NA1, int NA2, int key1)
  { 
    return ( pp_meson_(&Esec,&Pp1,&NA1,&NA2,&key1) );
  }

  double bremss_spec_cc(double Egam, double E0, int IZ1, int Ne1)
  { 
    double dSdK;
    bremss_spec_(&Egam, &E0, &IZ1, &Ne1, &dSdK);
    return dSdK;
  }

  double aic_cc(int key,int kbg,double E0,double En,double gamma,double RG,double rho,double xi,double z,double RS,double DENS, int icomp)
  {
    double SPEC;
    double zz = z;
    if(zz==0.) zz = 1.e-4;
    //  cout<<"aic_cc"<<endl;
    aic_(&key,&kbg,&icomp,&E0,&En,&gamma,&RG,&rho,&xi,&zz,&RS,&SPEC,&DENS);
    return (M_PI*Rele*Rele/Mele *SPEC);
  }

} // namespace
