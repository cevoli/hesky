#include "magneticField.h"

namespace hesky {

  MagneticField::MagneticField(const int& N, 
			       Galaxy * gal_ ){ 
    dim = N;
    gal = gal_;
    xGrid = std::vector<double>(N,0.0);
    yGrid = std::vector<double>(N,0.0);
    zGrid = std::vector<double>(N,0.0);
    
    std::vector<double> rGalaxy = gal->GetR();
    std::vector<double> zGalaxy = gal->GetZ();
    
    for  ( int i=0;i<N;i++ ){
      xGrid[i] = double(i)/double(N)*(2.0*rGalaxy.back()) - rGalaxy.back();
      yGrid[i] = double(i)/double(N)*(2.0*rGalaxy.back()) - rGalaxy.back();
      zGrid[i] = double(i)/double(N)*(zGalaxy.back()-zGalaxy.front()) + zGalaxy.front();
    }
    
    for ( int i=0;i<3;i++ ){
      BRegular[i] = std::vector<double>(N*N*N,0.0);
    }
    BRandom = std::vector<double>(N*N*N,0.0);
  }
  
  double MagneticField::getBperp(const double& x,
				 const double& z, 
				 const double& y ){
    
    int i = int(floor((x-xGrid[0])/(xGrid[1]-xGrid[0])));
    int j = int(floor((y-yGrid[0])/(yGrid[1]-yGrid[0])));
    int k = int(floor((z-zGrid[0])/(zGrid[1]-zGrid[0])));
    
    if (i > xGrid.size()-2) i = xGrid.size()-2;
    if (j > yGrid.size()-2) j = yGrid.size()-2;
    if (k > zGrid.size()-2) k = zGrid.size()-2;
    
    const double t = (x-xGrid[i])/(xGrid[i+1]-xGrid[i]);
    const double u = (y-yGrid[j])/(yGrid[j+1]-yGrid[j]);
    const double v = (z-zGrid[k])/(zGrid[k+1]-zGrid[k]);
    
    double BInterpolated[3];
    
    int basic = globalindex(i,j,k);
    
    for ( int index=0;index<3;index++ ){
      
      const double BFieldLow = BRegular[index][basic]*(1.0-t)*(1.0-u) 
	+ BRegular[index][basic+dim*dim]*(t)*(1.0-u) 
	+ BRegular[index][basic+dim]*(1.0-t)*(u) 
	+ BRegular[index][basic+dim*(dim+1)]*(t)*(u);
      
      const double BFieldHigh = BRegular[index][basic+1]*(1.0-t)*(1.0-u) 
	+ BRegular[index][basic+dim*dim+1]*(t)*(1.0-u) 
	+ BRegular[index][basic+dim+1]*(1.0-t)*(u) 
	+ BRegular[index][basic+dim*(dim+1)+1]*(t)*(u);
      
      BInterpolated[index] = BFieldLow*(1.0-v) + BFieldHigh*v;
    }

    const double Bpar = ((x-xsun)*BInterpolated[0]+(y-ysun)*BInterpolated[1]+(z-zsun)*BInterpolated[2])
      /sqrt(pow(x-xsun,2)+pow(y-ysun,2)+pow(z-zsun,2));

    const double Bperp2 = BInterpolated[0]*BInterpolated[0]
      +BInterpolated[1]*BInterpolated[1]
      +BInterpolated[2]*BInterpolated[2]
      -Bpar*Bpar;
    
    if ( Bperp2 > 0 ) 
      return std::sqrt(Bperp2);
    
    return 0;
  }
  
  double MagneticField::getBtotal(const double& x,
				  const double& z,
				  const double& y ){
    
    int i = int(floor((x-xGrid[0])/(xGrid[1]-xGrid[0])));
    int j = int(floor((y-yGrid[0])/(yGrid[1]-yGrid[0])));
    int k = int(floor((z-zGrid[0])/(zGrid[1]-zGrid[0])));
    
    if (i > xGrid.size()-2) i = xGrid.size()-2;
    if (j > yGrid.size()-2) j = yGrid.size()-2;
    if (k > zGrid.size()-2) k = zGrid.size()-2;
    
    const double t = (x-xGrid[i])/(xGrid[i+1]-xGrid[i]);
    const double u = (y-yGrid[j])/(yGrid[j+1]-yGrid[j]);
    const double v = (z-zGrid[k])/(zGrid[k+1]-zGrid[k]);
    
    double BInterpolated[3];
    int basic = globalindex(i,j,k);
    
    for ( int index=0;index<3;index++ ){
      const double BFieldLow = BRegular[index][basic]*(1.0-t)*(1.0-u) 
	+ BRegular[index][basic+dim*dim]*(t)*(1.0-u) 
	+ BRegular[index][basic+dim]*(1.0-t)*(u) 
	+ BRegular[index][basic+dim*(dim+1)]*(t)*(u);
      
      const double BFieldHigh = BRegular[index][basic+1]*(1.0-t)*(1.0-u) 
	+ BRegular[index][basic+dim*dim+1]*(t)*(1.0-u) 
	+ BRegular[index][basic+dim+1]*(1.0-t)*(u) 
	+ BRegular[index][basic+dim*(dim+1)+1]*(t)*(u);
      
      BInterpolated[index] = BFieldLow*(1.0-v) + BFieldHigh*v;
    }
    
    return sqrt(BInterpolated[0]*BInterpolated[0] 
		+ BInterpolated[1]*BInterpolated[1] 
		+ BInterpolated[2]*BInterpolated[2] );
  }

  double MagneticField::getBrand(const double& x,
				 const double& z,
				 const double& y ){
    
    int i = int(floor((x-xGrid[0])/(xGrid[1]-xGrid[0])));
    int j = int(floor((y-yGrid[0])/(yGrid[1]-yGrid[0])));
    int k = int(floor((z-zGrid[0])/(zGrid[1]-zGrid[0])));

    if (i > xGrid.size()-2) i = xGrid.size()-2;
    if (j > yGrid.size()-2) j = yGrid.size()-2;
    if (k > zGrid.size()-2) k = zGrid.size()-2;
    
    const double t = (x-xGrid[i])/(xGrid[i+1]-xGrid[i]);
    const double u = (y-yGrid[j])/(yGrid[j+1]-yGrid[j]);
    const double v = (z-zGrid[k])/(zGrid[k+1]-zGrid[k]);

    int basic = globalindex(i,j,k);
    
    const double BFieldLow = BRandom[basic]*(1.0-t)*(1.0-u) 
      + BRandom[basic+dim*dim]*(t)*(1.0-u) 
      + BRandom[basic+dim]*(1.0-t)*(u) 
      + BRandom[basic+dim*(dim+1)]*(t)*(u);
    
    const double BFieldHigh = BRandom[basic+1]*(1.0-t)*(1.0-u) 
      + BRandom[basic+dim*dim+1]*(t)*(1.0-u) 
      + BRandom[basic+dim+1]*(1.0-t)*(u) 
      + BRandom[basic+dim*(dim+1)+1]*(t)*(u); 

    return BFieldLow*(1.0-v)+BFieldHigh*v;
  }
  
  UniformField::UniformField(const int& N, 
			     const double& BValue,
			     Galaxy* gal ) : MagneticField(N,gal) {
    
    for ( int i=0;i<N;i++ ){
      for ( int j=0;j<N;j++ ){
	for ( int k=0;k<N;k++ ){
	  int basic = globalindex(i,j,k);
	  BRandom[basic] = BValue;
	}
      }
    }
  }
  
} // namespace
