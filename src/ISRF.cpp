#include "ISRF.h"

namespace hesky {

  ISRF::ISRF() {
    
    dimnu = 0;
    nr    = 0;
    nz    = 0;
    ncomp = 0;
    
    rmin  = 0;
    zmin  = 0;
    numin = 0;
    
    Dr    = 0;
    Dz    = 0;
    Dnu   = 0;
    logDnu = 0;
    
    return ;
  }

  ISRF::ISRF(const ISRF& rad) {
    
    dimnu = rad.dimnu ;
    nr    = rad.nr    ;
    nz    = rad.nz    ;
    ncomp = rad.ncomp ;
    
    rmin   = rad.rmin  ;
    zmin   = rad.zmin  ;
    numin  = rad.numin ;
    
    Dr  = rad.Dr  ;
    Dz  = rad.Dz  ;
    Dnu = rad.Dnu ;
    logDnu = rad.logDnu;
    
    nu.assign(rad.nu.begin(), rad.nu.end());
    
    r_array.assign(rad.r_array.begin(), rad.r_array.end());
    z_array.assign(rad.z_array.begin(), rad.z_array.end());
    
    field.assign(rad.field.begin(), rad.field.end());
    en_dens.assign(rad.en_dens.begin(), rad.en_dens.end());
    field_comp.assign(rad.field_comp.begin(), rad.field_comp.end());
    
    return ;
  }

  ISRF::ISRF(std::string filename, 
	     std::vector<double> r, 
	     std::vector<double> y, 
	     std::vector<double> z) {
    
    fitsfile * fptr;
    
    r_array = r;
    y_array = y;
    ny = y_array.size();
    
    z_array = z;
    rmin = r_array[0];
    ymin = (ny) ? y_array[0] : 0;
    zmin = z_array[0];
    Dr = r_array[1]-r_array[0];
    Dy = ny ? y_array[1]-y_array[0] : 0;
    Dz = z_array[1]-z_array[0];
    nr = r_array.size();
    nz = z_array.size();
    
    int status=0;
    
    if (fits_open_file(&fptr, filename.c_str(), READONLY, &status)) fits_report_error(stderr, status);
    char comment[100];
    int NAXIS, NAXIS1, NAXIS2, NAXIS3, NAXIS4;
    float CRVAL1,CRVAL2,CRVAL3,CDELT1,CDELT2,CDELT3;
    
    if( fits_read_key(fptr,TINT,"NAXIS" ,&NAXIS ,comment,&status) ) 
      std::cout<<"0read isrf status= "<<status<<std::endl;
    if( fits_read_key(fptr,TINT,"NAXIS1",&NAXIS1,comment,&status) ) 
      std::cout<<"1read isrf status= "<<status<<std::endl;
    if( fits_read_key(fptr,TINT,"NAXIS2",&NAXIS2,comment,&status) ) 
      std::cout<<"2read isrf status= "<<status<<std::endl;
    if( fits_read_key(fptr,TINT,"NAXIS3",&NAXIS3,comment,&status) ) 
      std::cout<<"3read isrf status= "<<status<<std::endl;
    if( fits_read_key(fptr,TINT,"NAXIS4",&NAXIS4,comment,&status) ) 
      std::cout<<"4read isrf status= "<<status<<std::endl;
    
    if( fits_read_key(fptr,TFLOAT,"CRVAL1",&CRVAL1,comment,&status) ) 
      std::cout<<"5read isrf status= "<<status<<std::endl;
    if( fits_read_key(fptr,TFLOAT,"CRVAL2",&CRVAL2,comment,&status) ) 
      std::cout<<"6read isrf status= "<<status<<std::endl;
    if( fits_read_key(fptr,TFLOAT,"CRVAL3",&CRVAL3,comment,&status) ) 
      std::cout<<"7read isrf status= "<<status<<std::endl;
    if( fits_read_key(fptr,TFLOAT,"CDELT1",&CDELT1,comment,&status) ) 
      std::cout<<"8read isrf status= "<<status<<std::endl;
    if( fits_read_key(fptr,TFLOAT,"CDELT2",&CDELT2,comment,&status) ) 
      std::cout<<"9read isrf status= "<<status<<std::endl;
    if( fits_read_key(fptr,TFLOAT,"CDELT3",&CDELT3,comment,&status) ) 
      std::cout<<"/read isrf status= "<<status<<std::endl;
    
    ncomp = NAXIS4;
    
    long nelements = NAXIS1*NAXIS2*NAXIS3*NAXIS4;
    long felement = 1;
    float *isrf_in = new float[nelements];
    float nulval = 0;
    int anynul;
    
    if( fits_read_img(fptr,TFLOAT,felement,nelements,&nulval,isrf_in,&anynul,&status) ) fits_report_error(stderr, status);
    
    dimnu = NAXIS3;
    Dnu = CDELT3;
    numin = CRVAL3;
    nu = std::vector<double>(dimnu,0.0);
    
    // microns -> cm; nu=c/lambda
    for ( int inu=0;inu<dimnu;inu++ ) 
      nu[dimnu-1-inu]=cLight/(pow(10.,1.*numin+double(inu)*Dnu)*1.0e-4);
    
    numin = nu[0];
    Dnu = nu[1]/nu[0];
    
    field = ny ? std::vector<double>(dimnu*nr*nz*ny, 0.0) : std::vector<double>(dimnu*nr*nz,0.0);
    en_dens = ny ? std::vector<double>(nr*nz*ncomp*ny, 0.0) : std::vector<double>(nr*nz*ncomp,0.0);
    field_comp = ny ? std::vector<double>(dimnu*nr*nz*ny*ncomp, 0.0) : std::vector<double>(dimnu*nr*nz*ncomp,0.0);
    
    for(int i = 0; i < ncomp; i++) {
      if (ny == 0) {
	for(int ir = 0; ir < nr; ir++) {
	  for(int iz = 0; iz < nz; iz++) {
	    int irr=(int)((r_array[ir] -CRVAL1) /CDELT1+0.5);
	    int izz=(int)((fabs(z_array[iz])-CRVAL2) /CDELT2+0.5);
	    if(irr>NAXIS1-2) irr=NAXIS1-2;
	    if(izz>NAXIS2-2) izz=NAXIS2-2;
	    float rr=CRVAL1+irr*CDELT1;
	    float zz=CRVAL2+izz*CDELT2;
                    
	    for(int inu = 0; inu < dimnu; inu++) {
	      float v1=isrf_in[isrf_index(irr  ,izz  ,inu,i,NAXIS1,NAXIS2,NAXIS3,NAXIS4)];
	      float v2=isrf_in[isrf_index(irr+1,izz  ,inu,i,NAXIS1,NAXIS2,NAXIS3,NAXIS4)];
	      float v3=isrf_in[isrf_index(irr  ,izz+1,inu,i,NAXIS1,NAXIS2,NAXIS3,NAXIS4)];
	      float v4=isrf_in[isrf_index(irr+1,izz+1,inu,i,NAXIS1,NAXIS2,NAXIS3,NAXIS4)];
	      float v5=v1+(v2-v1)*(r_array[ir]-rr)/CDELT1;
	      float v6=v3+(v4-v3)*(r_array[ir]-rr)/CDELT1;
	      float value=v5+(v6-v5)*(fabs(z_array[iz])-zz)/CDELT2;
	      if(value<0.0) value=0.0;
                        
	      field[index_field(dimnu-1-inu,ir,iz)] += value;
	      field_comp[index_fieldcomp(dimnu-1-inu,ir,iz,i)] = value;
	    }
	  }
	}
      }
      else { // 3D
	for(int ir = 0; ir < nr; ir++) {
	  for(int iy = 0; iy < ny; iy++) {
	    for(int iz = 0; iz < nz; iz++) {
	      double rr1 = sqrt(r_array[ir]*r_array[ir] + y_array[iy]*y_array[iy]);
                        
	      int irr=(int)((rr1 -CRVAL1) /CDELT1+0.5);
	      int izz=(int)((fabs(z_array[iz])-CRVAL2) /CDELT2+0.5);
	      if(irr>NAXIS1-2) irr=NAXIS1-2;
	      if(izz>NAXIS2-2) izz=NAXIS2-2;
	      float rr=CRVAL1+irr*CDELT1;
	      float zz=CRVAL2+izz*CDELT2;
                        
	      for(int inu = 0; inu < dimnu; inu++) {
		float v1=isrf_in[isrf_index(irr  ,izz  ,inu,i,NAXIS1,NAXIS2,NAXIS3,NAXIS4)];
		float v2=isrf_in[isrf_index(irr+1,izz  ,inu,i,NAXIS1,NAXIS2,NAXIS3,NAXIS4)];
		float v3=isrf_in[isrf_index(irr  ,izz+1,inu,i,NAXIS1,NAXIS2,NAXIS3,NAXIS4)];
		float v4=isrf_in[isrf_index(irr+1,izz+1,inu,i,NAXIS1,NAXIS2,NAXIS3,NAXIS4)];
		float v5=v1+(v2-v1)*(r_array[ir]-rr)/CDELT1;
		float v6=v3+(v4-v3)*(r_array[ir]-rr)/CDELT1;
		float value=v5+(v6-v5)*(fabs(z_array[iz])-zz)/CDELT2;
		if(value<0.0) value=0.0;
                            
		field[index_field(dimnu-1-inu,ir,iy, iz)] += value;
		field_comp[index_fieldcomp(dimnu-1-inu,ir,iy,iz,i)] = value;
	      }
	    }
	  }
	}
      }
    }
    
    if (ny == 0) {
      for (int i = 0; i < nr; i++) {
	for (int j = 0; j < nz; j++) {
	  for (int k = 0; k < ncomp; k++) {
	    for (int inu = 0; inu < dimnu; inu++) 
	      en_dens[index_endens(i,j,k)] += log(Dnu)*field_comp[index_fieldcomp(inu,i,j,k)];
	  }
	}
      }
    }
    else {
      for (int i = 0; i < nr; i++) {
	for (int iy = 0; iy < ny; iy++) {
	  for (int j = 0; j < nz; j++) {
	    for (int k = 0; k < ncomp; k++) {
	      for (int inu = 0; inu < dimnu; inu++) 
		en_dens[index_endens(i,iy,j,k)] += log(Dnu)*field_comp[index_fieldcomp(inu,i,iy,j,k)];
	    }
	  }
	}
      }
    }
    delete[] isrf_in;
    logDnu = log10(Dnu);
    
    return ;
  }

  void ISRF::Print() {
    std::ofstream outfile("ISRF.dat", std::ios::out);
    for ( int i=0;i<dimnu;i++ ) 
      outfile<<nu[i]<<"\t"<<this->GetDensity(8,21,i)<<std::endl;
    outfile.close();
    return ;
  }

  const double ISRF::GetDensity(double r, double z, int i) {
    double rr = fabs(r);
    int ir = int((rr-rmin)/Dr);
    int iz = int((z-zmin)/Dz);
    if (r_array[ir] > rr && ir > 0) ir--; 
    if (z_array[iz] > z && iz > 0) iz--; 
    
    double t = (rr-r_array[ir])/(r_array[ir+1]-r_array[ir]);
    double u = (z-z_array[iz])/(z_array[iz+1]-z_array[iz]);
    return (field[index_field(i,ir,iz)]*(1.-t)*(1.-u) 
	    + field[index_field(i,ir+1,iz)]*(t)*(1.-u) 
	    + field[index_field(i,ir+1,iz+1)]*(t)*(u) 
	    + field[index_field(i,ir,iz+1)]*(1.-t)*(u));
  }

  const double ISRF::GetDensity(int r, int z, int freq) {
    return field[index_field(freq,r,z)];
  }

  const double ISRF::GetDensity_over_Nu(int r, int z, int freq) {
    return field[index_field(freq,r,z)]/nu[freq];
  }

  const double ISRF::GetDensity(double r, double z, double freq) {
    
    int i = int(log10(freq/nu[0])/logDnu);
    if (nu[i] > freq && i > 0) i--; 
    
    double t = (freq-nu[i])/(nu[i+1]-nu[i]);
    return (this->GetDensity(r,z,i)*(1.-t) + this->GetDensity(r,z,i+1)*t);
  }

  const double ISRF::GetDensity_over_Nu(double r, double z, int i) {
    double rr = fabs(r);
    int ir = int((rr-rmin)/Dr);
    int iz = int((z-zmin)/Dz);
    if (r_array[ir] > rr && ir > 0) ir--; 
    if (z_array[iz] > z && iz > 0) iz--; 
    
    double t = (rr-r_array[ir])/(r_array[ir+1]-r_array[ir]);
    double u = (z-z_array[iz])/(z_array[iz+1]-z_array[iz]);
    return (field[index_field(i,ir,iz)]*(1.-t)*(1.-u) 
	    + field[index_field(i,ir+1,iz)]*(t)*(1.-u) 
	    + field[index_field(i,ir+1,iz+1)]*(t)*(u) 
	    + field[index_field(i,ir,iz+1)]*(1.-t)*(u))/nu[i];
  }

  const double ISRF::GetDensity_over_Nu(double r, double z, double freq) {
    
    int i = int(log10(freq/nu[0])/logDnu);
    if (nu[i] > freq && i > 0) i--; 
    
    double t = (freq-nu[i])/(nu[i+1]-nu[i]);
    return (this->GetDensity_over_Nu(r,z,i)*(1.-t) 
	    + this->GetDensity_over_Nu(r,z,i+1)*t);
  }
  
  const double ISRF::GetDensity(double r, double z, int i, int comp) {
    double rr = fabs(r);
    int ir = int((rr-rmin)/Dr);
    int iz = int((z-zmin)/Dz);
    if (r_array[ir] > rr && ir > 0) ir--; 
    if (z_array[iz] > z && iz > 0) iz--; 
    
    double t = (rr-r_array[ir])/(r_array[ir+1]-r_array[ir]);
    double u = (z-z_array[iz])/(z_array[iz+1]-z_array[iz]);
    return (field_comp[index_fieldcomp(i,ir,iz,comp)]*(1.-t)*(1.-u) 
	    + field_comp[index_fieldcomp(i,ir+1,iz,comp)]*(t)*(1.-u) 
	    + field_comp[index_fieldcomp(i,ir+1,iz+1,comp)]*(t)*(u) 
	    + field_comp[index_fieldcomp(i,ir,iz+1,comp)]*(1.-t)*(u));
  }

  const double ISRF::GetDensity(int r, int z, int freq, int comp) {
    return field_comp[index_fieldcomp(freq,r,z,comp)];
  }

  const double ISRF::GetDensity_over_Nu(int r, int z, int freq, int comp) {
    return field_comp[index_fieldcomp(freq,r,z,comp)]/nu[freq];
  }

  const double ISRF::GetDensity(double r, double z, double freq, int comp) {
    
    int i = int(log10(freq/nu[0])/logDnu);
    if (nu[i] > freq && i > 0) i--; 
    
    double t = (freq-nu[i])/(nu[i+1]-nu[i]);
    return (this->GetDensity(r,z,i,comp)*(1.-t) 
	    + this->GetDensity(r,z,i+1,comp)*t);
  }

  const double ISRF::GetDensity_over_Nu(double r, double z, int i, int comp) {
    double rr = fabs(r);
    int ir = int((rr-rmin)/Dr);
    int iz = int((z-zmin)/Dz);
    if (r_array[ir] > rr && ir > 0) ir--; 
    if (z_array[iz] > z && iz > 0) iz--; 
    
    double t = (rr-r_array[ir])/(r_array[ir+1]-r_array[ir]);
    double u = (z-z_array[iz])/(z_array[iz+1]-z_array[iz]);
    return (field_comp[index_fieldcomp(i,ir,iz,comp)]*(1.-t)*(1.-u) 
	    + field_comp[index_fieldcomp(i,ir+1,iz,comp)]*(t)*(1.-u) 
	    + field_comp[index_fieldcomp(i,ir+1,iz+1,comp)]*(t)*(u) 
	    + field_comp[index_fieldcomp(i,ir,iz+1,comp)]*(1.-t)*(u))/nu[i];
  }

  const double ISRF::GetDensity_over_Nu(double r, double z, double freq, int comp) {
    
    int i = int(log10(freq/nu[0])/logDnu);
    if (nu[i] > freq && i > 0) i--; 
    
    double t = (freq-nu[i])/(nu[i+1]-nu[i]);
    return (this->GetDensity_over_Nu(r,z,i,comp)*(1.-t) 
	    + this->GetDensity_over_Nu(r,z,i+1,comp)*t);
  }

  float ISRF::isrf_energy_density(float rr, float zz, int comp ){
    
    int iz=(int)((zz-zmin)/Dz + 0.5);
    
    if(iz<0 || iz > nz-1) 
      { 
	std::cerr<<"isrf_energy_density iz out of range"<<std::endl;
        exit(ISRF_ENERGY_OUT_OF_RANGE);
      }
    
    int ir=(int)(rr/Dr + 0.5);

    if(ir<0 || ir>nr-1)
      {
	std::cerr<<"isrf_energy_density rr ir = "<<rr<<"\t"<<ir<<" out of range!"<<std::endl; 
        exit(ISRF_ENERGY_OUT_OF_RANGE);
      }
    
    return en_dens[index_endens(ir,iz,comp)];
  }
  
} // namespace
