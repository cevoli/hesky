#include "hesky.h"

namespace hesky {

  ISRF * radiationField = NULL;
  
  extern "C" void isrf_energy_density_( float* r,float* z,int* icomp,float* energy_density ){
    *energy_density = radiationField->isrf_energy_density(*r,*z,*icomp);
    return;
  }  
  
  HESKY::HESKY(Input * in_) {
    
    in = in_;
    
    try {
      
      readingCR();
      
      computeGalaxyComponents();
      
      if (in->doComputeGammaMaps) {
	std::cout<<"Creating gamma map structure ..."<<std::endl;
	gammaMaps = new ComputeGammaMaps(galaxyGrid,H2,HI,HII,nMapComponents); 
	std::cout<<"... done!"<<std::endl;
      }
      
      if (in->doComputeSynchrotronMaps) {
	
	/*if (el->IsPresent()) elposvect.push_back(el);
	  if (pos->IsPresent()) elposvect.push_back(pos);
	  if (elposvect.empty()) {
	  cerr << "No synching particles. Returning" << endl;
	  exit(NO_SYNCH_PART);
	  }
	  
	  switch (in->BM) {
	  case Wmap:
	  magnfield = new WmapField(100, gal, 3.0e-6, -8.5, 25.0, 8.0);
	  break;
	  case Prouda:
	  magnfield = NULL;//new ProudaField(100, gal, 10., );
	  break;
	  case Tkachev:
	  magnfield = new TkachevField(100, gal, 1.4e-6, -8, 0.5, 1.5, 4.0);
	  break;
	  case Pshirkov:
	  magnfield = new PshirkovField(100, in->B0disk, 5., 1., 10., in->B0halo, 8., 1.3, in->B0turb, 8.5, zt, gal);
	  break;
	  case Farrar:
	  magnfield = new FarrarField(100, gal, in->betaFarrar);
	  break;
	  case Azimuthal:
	  magnfield = new AzimField(100, 2.e-6, 8, 2.0, zt, gal);
	  break;
	  case Spiral:
	  magnfield = new SpiralField(100, gal, 2.e-6, -12.0, 2.e-6, 10, 5, 1.0);
	  break;
	  case Uniform:
	  magnfield = new UniformField(100, gal, in->B0turb);
	  break;
	  default:
	  magnfield = NULL;
	  }   
	  
	  cout << "Creating synchrotron structure ..." << endl;
	  synchandle = new Synchrotron(in->Nufact, gal, in->MinFreq, in->MaxFreq);
	  cout << "... done!" << endl;
	  
	  string output_filename = "!output/";
	  output_filename += in->filename;
	  output_filename += "_";
	  output_filename += in->input_filename;
	  output_filename += "_synch";
	  if (!(in->iHealpix)) output_filename += "_grid";
	  else output_filename += "_healpix";
	  output_filename += ".fits.gz";
	  if (fits_create_file(&outfile_s, output_filename.c_str(), &status)) fits_report_error(stderr, status);
	*/
      }
    }
    
    catch (int e) {
      if (e == NO_RADIATING_PARTICLES)
	std::cerr<<"There are no radiating particles in this FITS file!"<<std::endl;
      if (e == NO_ROOT)
	std::cout<<"Warning! ROOT not implemented yet."<<std::endl;
      if (e == NO_GAS_TYPE)
	std::cerr<<"No Gas model!"<<std::endl;
      if (e == NO_GAS) 
	std::cout<<"Error in ringGasModel!"<<std::endl;
      exit(1);
    }
  }
  
  void HESKY::readingCR(){ 
    
    std::cout<<"Reading CR data ... "<<std::endl;
    
    nMapComponents = 0;    
    
    if (in->filetype == "Constant") {
      Particle * protons = new Particle(1,1);
      std::cout<<"... "<<"GCR proton densities are set constant!"<<std::endl;
      if (protons->checkIsPresent()) particleDict.push_back(protons);	
      nMapComponents++; // pi0 decay
      
      Particle * electrons = new Particle(0,-1);
      std::cout<<"... "<<"GCR electron densities are set constant!"<<std::endl;
      if (electrons->checkIsPresent()) particleDict.push_back(electrons);
      nMapComponents += 2; // bremsstrahlung and IC
    }
    
    else if (in->filetype == "FITS") {
      Particle * protons = new Particle( in->dragonRunFilenameFull, 1, 1 );
      std::cout<<"... "<<"GCR proton densities were successfully read!"<<std::endl;
      if (protons->checkIsPresent()) particleDict.push_back(protons);
      
      Particle * helium = new Particle( in->dragonRunFilenameFull, 4, 2 );
      std::cout<<"... "<<"GCR Helium densities were successfully read!"<<std::endl;
      if (helium->checkIsPresent()) particleDict.push_back(helium);
      
      Particle * electrons = new Particle( in->dragonRunFilenameFull, 0, -1 );
      std::cout<<"... "<<"GCR electron densities were successfully read1"<<std::endl; 
      if (electrons->checkIsPresent()) particleDict.push_back(electrons);
      
      Particle * positrons = new Particle( in->dragonRunFilenameFull, 0, 1 );
      std::cout<<"... "<<"GCR positron densities were successfully read!"<<std::endl;    
      if (positrons->checkIsPresent()) particleDict.push_back(positrons);   
      
      if (protons->checkIsPresent() || helium->checkIsPresent())
	nMapComponents++; // pi0 decay
      
      if (electrons->checkIsPresent() || positrons->checkIsPresent())
	nMapComponents += 2; // bremsstrahlung and IC
    }
    
    else if (in->filetype == "ROOT" )
      throw NO_ROOT;
    
    if (nMapComponents == 0)
      throw NO_RADIATING_PARTICLES;
    
    return;
  }
  
  void HESKY::computeGalaxyComponents(){
    
    std::cout<<"Initializing Galaxy ..."<<std::endl;
    
    std::cout<<"... creating galactic grid"<<std::endl;
    galaxyGrid = new Galaxy(in,particleDict[0]->GetR(),particleDict[0]->GetY(),particleDict[0]->GetZ());
    std::cout<<"... done!"<<std::endl;
    
    std::cout<<"... reading ISRF"<<std::endl;  
    radiationField = new ISRF(isrfGalpropFile,particleDict[0]->GetR(),particleDict[0]->GetY(),particleDict[0]->GetZ());
    std::cout<<"... done!"<<std::endl;
    
    if (in->doComputeGammaMaps) {
      std::cout<<"... calculating Gas Grid"<<std::endl;
      H2 = new COGasDistribution(galaxyGrid);
      HI = new HIGasDistribution(galaxyGrid);
      HII =  new HIIGasDistribution(galaxyGrid);
      std::cout<<"... done!"<<std::endl;
      /*
	add a flag in input: If SpiralArms selected, then call SpiralArmGas
	if (in->COtype == GalpropCOWithSpirals && in->HItype == GalpropHIWithSpirals)
	totalgas_spiral = new SpiralArmGas(481,481,81,gal);
      */
    }
    return;
  }
  
  void HESKY::Run() {
        
    if (in->doComputeGammaMaps) {
      
      gammaMaps->computeEmissionMap(particleDict,radiationField);

#ifdef _OPENMP

      std::cout<<"OPENMP-based parallelization"<<std::endl;
      
      unsigned long maxiter = getNpix(); 
      gammaMaps->ComputeMapRings(0,maxiter-1);
      
#endif
      
      /*
      if (in->doComputeAnisotropicIC && (iEl + 1 || iPos + 1)) {
	if (!(iPos+1)) ph->ComputeMap(part[iEl], NULL, rad);
	else if (!(iEl+1)) ph->ComputeMap(NULL, part[iPos], rad);
	else ph->ComputeMap(part[iEl], part[iPos], rad);
	}
	
	ph->PrintMap(outfile_g, pgr);
	ph->PrintEmissionMap(outfile_g_emiss);
      */
    }
    
    /*
    if (in->iSync) {
    	
    time_t tin;
    time_t tfin;
        
    time(&tin);
        
    #ifndef _OPENMP
    	
    //cout << "pthreads-based parallelization " << endl;
    	
    pthread_t ThreadVector[NUMTHREADS];
        
    //cout << "Created " << NUMTHREADS << " threads" << endl;
        
    for (unsigned int count =0; count <NUMTHREADS; count++) {
            
    unsigned long maxiter = synchandle->GetNpix(); 
            
    unsigned long start_pix = count*maxiter/NUMTHREADS;
    unsigned long stop_pix  = (count+1)*maxiter/NUMTHREADS;            
    //cout << "Thread " << count << " from pixel " << start_pix << " to " << stop_pix << endl;	
    parameter_list_type p1_synchro = {start_pix, stop_pix-1, this};
    pthread_create( &ThreadVector[count], NULL, &ComputeMapSynchroPartial, &p1_synchro);
            
    }
        
    for (unsigned int count =0; count <NUMTHREADS; count++) {
            
    pthread_join( ThreadVector[count], NULL);
    }
    	
    #endif	
        
    #ifdef _OPENMP
    	
    //cout << "OPENMP-based parallelization " << endl;
        
    unsigned long maxiter = synchandle->GetNpix(); 
    	
    synchandle->ComputeMap(elposvect, magnfield, 0, maxiter-1);
    	
    #endif
        
    time(&tfin);
        
    //cout << "Time needed to build Synchrotron map: " << tfin-tin << " s." << endl;	
        
    synchandle->PrintMap(outfile_s);
    }
    */
    return ;
  }

  void HESKY::setWorldNull(){
    
    /*
      magnfield = NULL;
      synchandle = NULL;*/
    gammaMaps = NULL;
    H2 = NULL;
    HI = NULL;
    HII = NULL;
    /*totalgas_spiral = NULL;
      outfile_g = NULL;
      outfile_n = NULL;
      outfile_s = NULL;
      carb = NULL;*/
    
    return;
  }
  
  HESKY::~HESKY() {
    
    //if (HI) delete HI;
    //if (H2) delete H2;
    //if (HII) delete HII;

    /*    
	  if (in->iGamma) {
	  if (fits_close_file(outfile_g, &status)) fits_report_error(stderr, status);
	  if (fits_close_file(outfile_g_emiss, &status)) fits_report_error(stderr, status); // close the FITS file 
	  }
	  if (in->iSync) {
	  if (fits_close_file(outfile_s, &status)) fits_report_error(stderr, status);
	  }

	  if (in->iNeutrino) {
	  if (fits_close_file(outfile_n, &status)) fits_report_error(stderr, status);
	  }
	  if (rad) delete rad;
    */
    
    //if (gammaMaps) delete gammaMaps;
    //if (HII) delete HII;
    //if (H2) delete H2;
    //if (HI) delete HI;
    //if (radiationField) delete radiationField;
    //if (galaxyGrid) delete galaxyGrid;
    //if (in) delete in;
    
    /*if (neu) delete neu;
	  if (magnfield) delete magnfield;
	  if (synchandle) delete synchandle;
	  if (pgr) delete pgr;
	  while (part.size()) {
	  if (part.back()) delete part.back();
	  part.pop_back();
	  }
	  elposvect.clear();
	  if (gal) delete gal;
    */
  }

} // namespace hesky

  /*#ifndef _OPENMP
      
      static void* ComputeMapRingsPartial(void* ptr) {
      parameter_list_type* parameter_list = static_cast<parameter_list_type*>(ptr);
      unsigned long start_pixel = parameter_list->low_pixel;
      unsigned long stop_pixel  = parameter_list->high_pixel;
      GAMMASKY* gammasky_instance = (parameter_list->gamma_object);
      //GAMMASKY* gammasky_instance = ptr_to_gammasky;
      (gammasky_instance->GetPhoton())->ComputeMapRings(gammasky_instance->GetCOGas(), gammasky_instance->GetHIGas(), gammasky_instance->GetHIIGas(), gammasky_instance->Get_PromptGammaRays(), start_pixel, stop_pixel);	
      }
      static void* ComputeMapSynchroPartial(void* ptr) {
      parameter_list_type* parameter_list = static_cast<parameter_list_type*>(ptr);
      unsigned long start_pixel = parameter_list->low_pixel;
      unsigned long stop_pixel  = parameter_list->high_pixel;
      GAMMASKY* gammasky_instance = (parameter_list->gamma_object);
      //GAMMASKY* gammasky_instance = ptr_to_gammasky;
      (gammasky_instance->GetSynchrotron())->ComputeMap(gammasky_instance->Get_particle_vector(), gammasky_instance->Get_magnetic_field(), start_pixel, stop_pixel);	
    }
    
    #endif*/
    
