#include "computeGammaMaps.h"

//#undef _OPENMP

#ifdef _OPENMP
#include <omp.h>
#endif

namespace hesky {
  
  ComputeGammaMaps::ComputeGammaMaps(Galaxy * gal_,
				     COGasDistribution * H2_,
				     HIGasDistribution * HI_,
				     HIIGasDistribution * HII_,
				     const int& nMapComponents_){     

    galaxyGrid = gal_;
    in = galaxyGrid->GetInput();
    
    HI = HI_;
    H2 = H2_;
    HII = HII_;

    maxEnergy = in->maxEnergy;
    minEnergy = in->minEnergy;
    ratioEnergy = in->ratioEnergy;
    
    nl = galaxyGrid->GetNl();
    nb = galaxyGrid->GetNb();
    nr = galaxyGrid->GetNr();
    ny = galaxyGrid->GetNy(); 
    nz = galaxyGrid->GetNz();
    nMapEnergy = int(std::log(maxEnergy/minEnergy)/std::log(ratioEnergy)+1.9);
    nMapComponents = nMapComponents_;
    nside = galaxyGrid->GetNside();
    npix = galaxyGrid->GetNpix(); 
    
    nrnznE = nr*nz*nMapEnergy;
    nrnynznE = nr*ny*nz*nMapEnergy;
    nlnbnE = nl*nb*nMapEnergy;
    npixnE = npix*nMapEnergy;

    rGrid = galaxyGrid->GetR(); yGrid = galaxyGrid->GetY(); zGrid = galaxyGrid->GetZ();
    bGrid = galaxyGrid->GetB(); lGrid = galaxyGrid->GetL();
    
    deltar = galaxyGrid->GetDeltar(); deltay = galaxyGrid->GetDeltay(); deltaz = galaxyGrid->GetDeltaz();
      
    zmin = zGrid.front(); zmax = zGrid.back();
    rmin = rGrid.front();
    rmax = (ny) ? sqrt(rGrid.back()*rGrid.back() + yGrid.back()*yGrid.back()) : rGrid.back();
    xmin = rGrid.front();
    ymin = (ny) ? yGrid.front() : 0;
    xmax = rGrid.back();
    ymax = (ny) ? yGrid.back() : 0;
   
    maxiter = in->doUseHealpix ? npix : nl*nb;
    
    if (in->doUseHealpix) {
      fluxToHealpix = std::vector<double>(nMapEnergy*npix*nMapComponents_,0.0);
      
      if (in->doComputeAnisotropicIC) {
	anisotropicIcToHealpix = std::vector<double>(nMapEnergy*npix,0.0);
	isotropicIcToHealpix = std::vector<double>(nMapEnergy*npix,0.0);
      }
    }
    else {
      fluxToGrid = std::vector<double>(nMapEnergy*nl*nb*nMapComponents_,0.0);
      
      if (in->doComputeAnisotropicIC) {
	anisotropicIcToGrid = std::vector<double>(nMapEnergy*nl*nb,0.0);
	isotropicIcToGrid = std::vector<double>(nMapEnergy*nl*nb,0.0);
      }
    }

    for ( int i=0;i<nMapEnergy;i++ )
      energyGrid.push_back(std::exp(std::log(minEnergy)+double(i)*std::log(ratioEnergy)));
    
    if (ny) {  
      emissionMaps = std::vector<double>(nMapEnergy*nr*ny*nz*4,0.0);
      emissionMapForHII = std::vector<double>(nMapEnergy*nr*ny*nz,0.0);
      if (in->doComputeAnisotropicIC) 
	emissionMapForIsotropic = std::vector<double>(nMapEnergy*nr*ny*nz,0.0);
    }
    else {     
      emissionMaps = std::vector<double>(nMapEnergy*nr*nz*4,0.0);
      emissionMapForHII = std::vector<double>(nMapEnergy*nr*nz,0.0);
      if (in->doComputeAnisotropicIC)
	emissionMapForIsotropic = std::vector<double>(nMapEnergy*nr*nz,0.0);
    }
  }

  void ComputeGammaMaps::computeEmissionMap(std::vector<hesky::Particle*> part, ISRF* rad) {
    
    std::cout<<"Computing emission maps ... "<<std::endl;
    
    int nnu = (rad) ? rad->GetDimnu() : 0;
    
    int nEpr = part[0]->GetNE();
    double Ekf = part[0]->GetEkfact();
    
    std::vector<double> pion_sp(nEpr*nMapEnergy,0.0);
    std::vector<double> pion_sp_He(nEpr*nMapEnergy,0.0);
    
    std::vector<double> lep_sp_brems(nEpr*nMapEnergy*2,0.0);
    std::vector<double> lep_sp_IC(nEpr*nMapEnergy*nnu,0.0);
    
    int iProton = Utils::findParticleIndex(hesky::PROTON, part);
    int iHelium = Utils::findParticleIndex(hesky::HELIUM, part);
    int iElectron = Utils::findParticleIndex(hesky::ELECTRON, part);
    int iPositron = Utils::findParticleIndex(hesky::POSITRON, part);
    
    //std::cout<<iProton<<"\t"<<iHelium<<"\t"<<iElectron<<"\t"<<iPositron<<"\t"<<nMapComponents<<std::endl;

    if (iProton+1) {
      Utils::InitPionEm(pion_sp,nMapEnergy,nEpr,energyGrid,part[iProton]->GetE(),Ekf,part[iProton]->GetAnumber(),in);
    }
    if (iHelium+1) {
      Utils::InitPionEm(pion_sp_He,nMapEnergy,nEpr,energyGrid,part[iHelium]->GetE(),Ekf,part[iHelium]->GetAnumber(),in);
    }
    
    std::cout<<"... pion production computed!"<<std::endl;
    
    if (iElectron+1 || iPositron+1) {
      Utils::InitElEm(lep_sp_brems,lep_sp_IC,nMapEnergy,nEpr,energyGrid,part[iElectron]->GetE(),Ekf,rad->GetNu());
    }
    
    std::cout << "... bremsstrahlung + IC production computed!"<<std::endl;
    
    const int nrnznE = nr*nz*nMapEnergy;
    const int nrnynznE = nr*ny*nz*nMapEnergy;
    
    int indiso,indem,indpsp,indbrems,indIC;
    indiso = indem = indpsp = indbrems = indIC = 0;
    
    //Start map computation!

#ifdef _OPENMP
#pragma omp parallel default(shared) private(indiso,indem,indpsp,indbrems,indIC) num_threads(NUMTHREADS)
#endif
    {
      std::vector<double> ISRFovernu(nnu);
      std::vector<double> ISRFovernu_iso(nnu);
      
#pragma omp for schedule(dynamic)    
      for (int i = 0; i < nr; i++) { 
	for (int iy = 0; iy < (ny && iy < ny) || (!ny && iy < 1); iy++) { 
	  for (int j = 0; j < nz; j++) {        
	    
	    if (iElectron+1 || iPositron+1) {
	      for (int inu = 0; inu < nnu; inu++) {
		ISRFovernu[inu] = rad->GetDensity_over_Nu(i,j,inu);
		if ((in->doComputeAnisotropicIC)) 
		  ISRFovernu_iso[inu] = rad->GetDensity_over_Nu(i,j,inu,2);
	      }
	    }
	    
	    for (int ip = 0; ip < nEpr; ip++) {
	      
	      double prot = 0.0;
	      double he = 0.0;
	      double lept = 0.0;
	      double pos = 0.0;
	      int indpart = (ny) ? part[0]->index(ip, i, iy, j) : part[0]->index(ip,i,j);
	      
	      if (iProton+1) prot = part[iProton]->CRdensity(indpart);
	      if (iHelium+1) he = part[iHelium]->CRdensity(indpart);
	      if (iElectron+1) lept = part[iElectron]->CRdensity(indpart);
	      if (iPositron+1) {
		pos = part[iPositron]->CRdensity(indpart);
		lept += pos;
	      }
	      
	      for ( int iEgamma=0;iEgamma<nMapEnergy;iEgamma++ ){
		
		indiso = index_emission_isoHII(i,j,iEgamma); 
		indem = index_emission(0, i, j, iEgamma);    
		indpsp = index_healpix(ip,iEgamma);          
		indbrems = index_brems(ip, iEgamma, 0);       
		indIC = (ip*nMapEnergy+iEgamma)*nnu;

		emissionMaps[indem] += pion_sp[indpsp]*prot;
		emissionMaps[indem] += pion_sp_He[indpsp]*he;
		
		emissionMapForHII[indiso] += pion_sp[indpsp]*prot;
		emissionMapForHII[indiso] += pion_sp_He[indpsp]*he;
		
		if (ny) {
		  emissionMaps[indem+nrnynznE] += lept*lep_sp_brems[indbrems+1];
		  emissionMaps[indem+2*nrnynznE] += lept*lep_sp_brems[indbrems];
                  
		  for ( int inu=0;inu<nnu;inu++ ){ 				
		    emissionMaps[indem+3*nrnynznE] += lep_sp_IC[indIC+inu]*lept*ISRFovernu[inu];
		    if (in->doComputeAnisotropicIC)
		      emissionMapForIsotropic[indiso] += lep_sp_IC[indIC+inu]*lept*ISRFovernu_iso[inu];
		  }
		} // if 
	
		else {
		  emissionMaps[indem+nrnznE] += lept*lep_sp_brems[indbrems+1];
		  emissionMaps[indem+2*nrnznE] += lept*lep_sp_brems[indbrems];
                  
		  for (int inu = 0; inu < nnu; inu++) {
		    emissionMaps[indem+3*nrnznE]  += lep_sp_IC[indIC+inu]*lept*ISRFovernu[inu];
		    if (in->doComputeAnisotropicIC)
		      emissionMapForIsotropic[indiso] += lep_sp_IC[indIC+inu]*lept*ISRFovernu_iso[inu];
		  }
		} // else
		
	      }
	    }
	  }
	}
      }
    }
    
    pion_sp.clear();
    pion_sp_He.clear();
    lep_sp_brems.clear();
    lep_sp_IC.clear();
    
    std::cout<<"... done!"<<std::endl;
    return;
  }
   
  void ComputeGammaMaps::computeMapRings(unsigned long Initial_pixel,
					 unsigned long Final_pixel ){
    
    const std::vector<double> H2Gas = H2->GetGas(), 
      HiGas = HI->GetGas(), 
      HiiGas = HII->GetGas();
    //    const std::vector<float> rb = H2->GetRbins();
    const int nring = H2->GetNrings();
    
    // START TO CALCULATE MAP
    
    std::cout<<"Computing gamma-ray map..."<<std::endl;
    
    if (in->doUseHealpix) 
      std::cout<<"... evaluating the map in Healpix format"<<std::endl;
    
    unsigned long counter = 0;
    
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic) num_threads(NUMTHREADS)
#endif
    for (counter = Initial_pixel; counter <= Final_pixel; counter++) {

      std::vector<double> nHIaverage(nring,small), 
	nH2average(nring,small);
      
      Emissivity * emissivity = new Emissivity(nring);
      
      Direction * direction = new Direction();
      
      initDirection(direction,counter);
      
      for ( int iEgamma=0;iEgamma<nMapEnergy;iEgamma++ ){
	
	Los * los = new Los();
	
	const int indFlux = index_flux(0,direction->i_long,direction->i_lat,iEgamma);
	//int ind_iso = index_pgr_aniso_iso(direction->i_long,direction->i_lat,iEgamma);
	const int indFluxHeal = index_fluxhealpix(0,counter,iEgamma);
	const int indHeal = index_healpix(counter,iEgamma);
	
	nHIaverage.assign(nring,small);
	nH2average.assign(nring,small);
	
	emissivity->Init();
	
	while ( ((!ny && los->r < rmax)/*||(ny && fabs(xs)<xmax && fabs(ys)< ymax)*/) && fabs(los->z)<zmax ){
	  
	  updateLos(los,direction,iEgamma);

	  if ( fabs(los->z) < maxzAboutGas )
	    computeEmissivity(los->dStepFactor,los,emissivity,nHIaverage,nH2average);
	    
	  if (in->doUseHealpix) {
	    
    	    if (in->doComputeAnisotropicIC) 
	      isotropicIcToHealpix[indHeal] += (ny) 
		? this->GetEmissionIso(los->ind_isoHII,los->t,los->u,los->lpart)*los->dStepFactor 
		: this->GetEmissionIso(los->ind_isoHII,los->t2d,los->u)*los->dStepFactor ;//*dscm;

	    switch (nMapComponents) {
	      
     	    case 3 :
     	      fluxToHealpix[indFluxHeal+2*npixnE] += (ny) 
		? this->GetEmission(los->ind+3*nrnynznE,los->t,los->u,los->lpart)*los->dStepFactor 
		: this->GetEmission(los->ind+3*nrnznE,los->t2d,los->u)*los->dStepFactor ;//*dscm;
     	      break;
	      
     	    case 2 :
     	      fluxToHealpix[indFluxHeal+npixnE] += (ny) 
		? this->GetEmission(los->ind+3*nrnynznE,los->t,los->u,los->lpart)*los->dStepFactor 
		: this->GetEmission(los->ind+3*nrnznE,los->t2d,los->u)*los->dStepFactor ;//*dscm;
     	      break;
	      
	    default :
     	      break ;
	    }
	    
	  }
	  
	  else {
	    
	    /*if (in->ComputeAnisotropicIC)
	      iso[ind_iso] +=  this->GetEmissionIso(ind_isoHII,t,u)*new_factor;// * dscm;*/

	    switch (nMapComponents) {
	      
	    case 3 :
	      fluxToGrid[indFlux+2*nlnbnE] += (ny) 
		? this->GetEmission(los->ind+3*nrnynznE,los->t,los->u,direction->l)*los->dStepFactor
		: this->GetEmission(los->ind+3*nrnznE,los->t2d,los->u)*los->dStepFactor ;//*dscm;
	      break;
	      
	    case 2 :
	      fluxToGrid[indFlux+nlnbnE] += (ny) 
		? this->GetEmission(los->ind+3*nrnynznE,los->t,los->u,direction->l)*los->dStepFactor
		: this->GetEmission(los->ind+3*nrnznE,los->t2d,los->u)*los->dStepFactor ;//*dscm;
	      break;
	      
	    default :
	      break;
	      
	    }
	  }
	  
    	} // while
	
	loopOverRings(iEgamma,counter,direction,emissivity,nH2average,nHIaverage);
	
      } // iEgamma
    } //   counter
    
    return ;
  }
  
  void ComputeGammaMaps::computeEmissivity(const double& new_factor,
					   Los * los_,
					   Emissivity * e_,
					   std::vector<double>& nHI,
					   std::vector<double>& nH2 ){
    
    const int i_ring = H2->WhichRing(los_->r);
    
    const double HII_now = HII->Get2DDensity(los_->r,los_->z),
      HI_now = HI->Get2DDensity(los_->r,los_->z), 
      H2_now = H2->Get2DDensity(los_->r,los_->z);
    
    nHI[i_ring] += HI_now*new_factor;
    nH2[i_ring] += H2_now*new_factor;
    
    // computes the emissivity averaged on the portion of line of sight corresponding to each ring
    
    if (nMapComponents != 2) {
      double pi0em = (ny) 
	? this->GetEmission(los_->ind,los_->t,los_->u,los_->lpart) 
	: this->GetEmission(los_->ind,los_->t2d,los_->u);
      
      e_->pi0fromH2[i_ring] += pi0em*H2_now*new_factor;
      e_->pi0fromHI[i_ring] += pi0em*HI_now*new_factor;
      e_->pi0fromHII[i_ring] += ((ny) 
				 ? this->GetEmissionHII(los_->ind_isoHII,los_->t,los_->u,los_->lpart) 
				 : this->GetEmissionHII(los_->ind_isoHII,los_->t2d,los_->u))*HII_now*new_factor;

    }
    
    if (nMapComponents != 1) {
      double bremsem = (ny) 
	? this->GetEmission(los_->ind+2*nrnynznE,los_->t,los_->u,los_->lpart) 
	: this->GetEmission(los_->ind+2*nrnznE,los_->t2d,los_->u);
      e_->bremssfromH2[i_ring] += bremsem*H2_now*new_factor;
      e_->bremssfromHI[i_ring] += bremsem*HI_now*new_factor;
      e_->bremssfromHII[i_ring] += ((ny) 
				    ? this->GetEmission(los_->ind+nrnynznE,los_->t,los_->u,los_->lpart) 
				    : this->GetEmission(los_->ind+nrnznE,los_->t2d,los_->u))*HII_now*new_factor;
    }
    
    return;
  }
  
  void ComputeGammaMaps::loopOverRings(const int& iEgamma_,
				       const unsigned long& counter_,
				       Direction * d_,
				       Emissivity * e_,
				       std::vector<double>& nH2average_,
				       std::vector<double>& nHIaverage_ ){
    
    double tgal = 0.0, ugal = 0.0;
    
    const int nring = H2->GetNrings();   
    const double ldeg1 = (d_->ldeg>=0) ? d_->ldeg : d_->ldeg + 360.0;
    const int ind_gal_start = H2->GetIndGal(ldeg1, d_->bdeg, 0, tgal, ugal);
    const int indFlux = index_flux(0,d_->i_long,d_->i_lat,iEgamma_);
    const int indFluxHeal = index_fluxhealpix(0,counter_,iEgamma_);
    
    const std::vector<float> rb = H2->GetRbins();
    
    for ( int i_ring=0;i_ring<nring;i_ring++ ) { // loop over rings 
      
      const int ind_gal_now = ind_gal_start + i_ring;
      
      //      const double weightFactorH2 = H2->GetDensity(ind_gal_now, tgal, ugal) 
      //* H2->X_CO((rb[i_ring] + rb[nring+i_ring])/2.) 
      //	* 2.e20 /(nH2average_[i_ring]*losStepInCm);
      
      double weightFactorH2 = ( in->coModel==galpropRingsForCO ) 
	? H2->GetDensity(ind_gal_now,tgal,ugal)/nH2average_[i_ring]
	: 1;
      
      weightFactorH2 *= H2->X_CO((rb[i_ring] + rb[nring+i_ring])/2.);
      weightFactorH2 *= 2.e20/losStepInCm;
      
      double weightFactorHI = ( in->hiModel==galpropRingsForHI )
	? HI->GetDensity(ind_gal_now,tgal,ugal)/nHIaverage_[i_ring]
	: 1;
      
      weightFactorHI *= 1.0e20/losStepInCm;
      
      if (in->doUseHealpix) {
	
	switch (nMapComponents) {
	  
	case 1 :
	  fluxToHealpix[indFluxHeal] += (weightFactorH2*e_->pi0fromH2[i_ring] 
					 + weightFactorHI*e_->pi0fromHI[i_ring] 
					 + e_->pi0fromHII[i_ring]);
	  break;
	  
	case 2 :
	  fluxToHealpix[indFluxHeal] += (weightFactorH2*e_->bremssfromH2[i_ring] 
					 + weightFactorHI*e_->bremssfromHI[i_ring] 
					 + e_->bremssfromHII[i_ring]);
	  break;
	
	case 3 :
	  fluxToHealpix[indFluxHeal] += (weightFactorH2*e_->pi0fromH2[i_ring] 
					 + weightFactorHI*e_->pi0fromHI[i_ring] 
					 + e_->pi0fromHII[i_ring]);

	  fluxToHealpix[indFluxHeal+npixnE] += (weightFactorH2*e_->bremssfromH2[i_ring] 
						+ weightFactorHI*e_->bremssfromHI[i_ring] 
						+ e_->bremssfromHII[i_ring]);
	  
	  break;
	  
	default :
	  break;
	}
      }
      
      else {

	switch (nMapComponents) {

	case 3 :
	  fluxToGrid[indFlux] += (weightFactorH2*e_->pi0fromH2[i_ring] 
				  + weightFactorHI*e_->pi0fromHI[i_ring] 
				  + e_->pi0fromHII[i_ring]);
	  fluxToGrid[indFlux+nlnbnE] += (weightFactorH2*e_->bremssfromH2[i_ring] 
					 + weightFactorHI*e_->bremssfromHI[i_ring] 
					 + e_->bremssfromHII[i_ring]);
	  break;
     	  
	case 1 :
	  fluxToGrid[indFlux] += (weightFactorH2*e_->pi0fromH2[i_ring] 
				  + weightFactorHI*e_->pi0fromHI[i_ring] 
				  + e_->pi0fromHII[i_ring]);
	  break;
     	  
	case 2 :
	  fluxToGrid[indFlux] += (weightFactorH2*e_->bremssfromH2[i_ring] 
				  + weightFactorHI*e_->bremssfromHI[i_ring] 
				  + e_->bremssfromHII[i_ring]);
	  break;
     	  
	default :
	  break;
	}
      } 
    } // for i_rings
    
    return;
  }
  
  void ComputeGammaMaps::initDirection(Direction * d,
				       const unsigned long& counter_ ){
    
    double b,l;
    
    if (in->doUseHealpix){
      Utils::pix2ang_ring(nside,counter_,&b,&l);
      d->b = b;
      d->l = l;
      d->b = M_PI/2.0-d->b;
      d->bdeg = d->b/DegToRad;
      d->ldeg = d->l/DegToRad;
    }
    else {
      d->i_lat = counter_%nb;
      d->bdeg = bGrid[d->i_lat];
      d->b = d->bdeg*DegToRad;
      //if (i_lat == 0) {
      d->i_long = counter_/nb;
      d->ldeg = lGrid[d->i_long];
      d->l = d->ldeg*DegToRad;
      //}
    }
    
    d->sinl = sin(d->l); 
    d->cosl = cos(d->l);
    d->sinb = sin(d->b);
    d->cosb = cos(d->b);
  };
  
  void ComputeGammaMaps::updateLos(Los * los_, 
				   Direction * d_,
				   const int& iEgamma_) {
    
    try {
    
      los_->iEgamma = iEgamma_;
      los_->d += los_->dStep;
      los_->z = los_->d*d_->sinb;

      los_->r = (xsun>0) 
	? pow(xsun,2)+pow(los_->d*d_->cosb,2)-2.0*xsun*los_->d*d_->cosb*d_->cosl 
	: xsun*xsun+pow(los_->d*d_->cosb,2)+2.0*xsun*los_->d*d_->cosb*d_->cosl; 
      
      if (los_->r < 1.0e-10) los_->r = 1.0e-10;
      if (los_->r < 0.) throw 1;
      
      los_->r = std::sqrt(los_->r);

      if ( los_->r > in->rToCoarseGrid || fabs(los_->z) > in->zToCoarseGrid ){ // TO BE CHANGED!
	const double dStepOld = los_->dStep;
	los_->dStep = los_->dStep*10.0/(1.0+fabs(d_->sinb));
	los_->dStepFactor = los_->dStep/dStepOld;
      }
      
      // interesting_region = 0;
      // else 
      // interesting_region = 1;
      
      // if (interesting_region == 1)
      // new_ds = ds;
      // else
      // new_ds = ds * 10./(1.+ fabs(sinb)) ;
      // new_factor = new_ds/ds;

      if (ny) {
	los_->xs = (xsun>0) 
	  ? xsun*(1.1)-los_->d*d_->cosb*d_->cosl 
	  : xsun*(1.1)+los_->d*d_->cosb*d_->cosl;
	//RR*cos(phis);
	
	los_->ys = los_->d*d_->cosb*d_->sinl;//RR*sin(phis);
	
	los_->ir = int(floor((los_->xs-xmin)/deltar));
	if (los_->ir > nr-2) los_->ir = nr-2;
	if (los_->ir < 0) los_->ir = 0;
	los_->t = (los_->xs-rGrid[los_->ir])/deltar;
	
	los_->iy = int(floor((los_->ys-ymin)/deltay));
	if (los_->iy > ny-2) los_->iy = ny-2;
	if (los_->iy < 0) los_->iy = 0;
	los_->lpart = (los_->ys-yGrid[los_->iy])/deltay;
      }
      
      los_->ir2d = int(floor(los_->r/deltar));
      if (los_->ir2d > nr-2) los_->ir2d = nr-2;
      //if (i < 0) i = 0;
      los_->t2d = (los_->r-rGrid[los_->ir2d])/deltar;
      
      //int iz = GetIZindexU(zz,u);
      los_->iz = int(floor((los_->z-zmin)/deltaz));
      if (los_->iz > nz-2) los_->iz = nz-2;
      if (los_->iz < 0) los_->iz = 0;
      los_->u = (los_->z-zGrid[los_->iz])/deltaz;
      
      los_->ind = (ny) 
	? index_emission(0,los_->ir,los_->iy,los_->iz,iEgamma_) 
	: index_emission(0,los_->ir2d,los_->iz,iEgamma_);
      los_->ind_isoHII = los_->ind; //index_emission_isoHII(ir,iz,iEgamma);
      
      los_->ind_gas = los_->ir2d*nz+los_->iz;
    }
    catch (int e){
      if (e == 1) std::cout<<"Fatal error in LOS!"<<std::endl;
    }
    
    return;
  }

} // namespace
