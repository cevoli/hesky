#include "readInputXML.h"

namespace hesky {
  
  int Input::loadFile( const std::string& inputFilename_ ){
    
    xmlFilename = inputFilename_;
  
    TiXmlDocument * doc = new TiXmlDocument(); // inputfilename_.c_str() );
  
    try {
    
      if ( doc->LoadFile(xmlFilename.c_str(), TIXML_DEFAULT_ENCODING) ){
      
	int found = xmlFilename.rfind('/');
	if (found == std::string::npos) found = -1;
      
	xmlFilenameInit = std::string(xmlFilename, found+1, xmlFilename.size()-4-(found+1));
      
	readDragonInputFile(doc);
	readSynchrotronParams(doc);
	readGammaParams(doc);
	readGasParams(doc);
	readYieldParams(doc);
	readGridParams(doc);
      }
      else
	throw PROBLEM_WITH_INPUT_FILE;
    }
    catch (int e){
      std::cerr<<"Fatal problem with the input file: "<<inputFilename_<<std::endl;
    }
  
    delete doc;
  
    return 0;
  }

  int Input::QueryIntAttribute(std::string obj, TiXmlElement* el) {
    TiXmlElement* el1 = el->FirstChildElement(obj.c_str());
    int result = -1;
  
    if (el1) el1->QueryIntAttribute("value", &result);
    return result;
  }

  double Input::QueryDoubleAttribute(std::string obj, TiXmlElement* el) {
    TiXmlElement* el1 = el->FirstChildElement(obj.c_str());
    double result = -1.0;
  
    if (el1) el1->QueryDoubleAttribute("value", &result);
    return result;
  }

  void Input::checkDragonRunPath() {
    if (dragonRunPath == ""){
      std::cerr<<"Warning! Path not specified, I assume dragonRuns/"<<std::endl;
      dragonRunPath = "dragonRuns";
    }
    return;
  }

  void Input::readDragonInputFile(TiXmlDocument * doc){
    
    TiXmlElement  * el  = NULL;
    TiXmlElement  * el1 = NULL;
    
    try {
      el = doc->FirstChildElement("Input");
      
      if (el){
	el1 = el->FirstChildElement("File");
	
	if (el1) {
	  el1->QueryStringAttribute("value", &dragonRunFilename);
	  el1->QueryStringAttribute("type", &filetype);
	  el1->QueryStringAttribute("path", &dragonRunPath);
	  checkDragonRunPath();
	}

	dragonRunFilenameFull = dragonRunPath + "/" + dragonRunFilename;
	
	if (filetype == "FITS")
	  dragonRunFilenameFull += ".fits.gz";
	else if (filetype == "ROOT")
	  dragonRunFilenameFull += ".root";
	else throw NO_INPUT_FILETYPE;      
      }
      
      else {
	filetype = "Constant";
	//throw NO_INPUT_FILE;
      }

    }
    catch (int e){
      std::cerr<<"Fatal error in readDragonInputFile!"<<std::endl;
    }
    
    return;
  }

  void Input::readSynchrotronParams(TiXmlDocument * doc){

    TiXmlElement  * el  = NULL;
    TiXmlElement  * el1 = NULL;
  
    std::string bf;

    try {
      el = doc->FirstChildElement("Synchrotron");
    
      if (el) {
	doComputeSynchrotronMaps = true;
	minFrequency   = QueryDoubleAttribute("MinFreq", el);
	maxFrequency   = QueryDoubleAttribute("MaxFreq", el);
	ratioFrequency = QueryDoubleAttribute("FreqFactor", el);
      
	el1 = el->FirstChildElement("MagneticField");
      
	if (!el1)	throw NO_BFIELD;
      
	el1->QueryStringAttribute("type", &bf);
	if (bf == "Azimuthal") magneticFieldModel = azimuthalBfield;
	else if (bf == "Spiral") magneticFieldModel = spiralBfield;
	else if (bf == "Prouda") magneticFieldModel = proudaBfield;
	else if (bf == "Wmap") magneticFieldModel = wmapBfield;
	else if (bf == "Pshirkov") magneticFieldModel = pshirkovBfield;
	else if (bf == "Farrar") magneticFieldModel = farrarBfield;
	else if (bf == "Uniform") magneticFieldModel = uniformBfield;
	else if (bf == "Tkachev") magneticFieldModel = tkachevBfield;
	else 
	  throw NO_BMODEL;
      
	bDiskAtSun = QueryDoubleAttribute("B0disk", el);
	bHaloAtSun = QueryDoubleAttribute("B0halo", el);
	bTurbAtSun = QueryDoubleAttribute("B0turb", el);
	betaMagField = QueryDoubleAttribute("betaFarrar", el);
      }
    }
    catch (int e){
      if (e == NO_BFIELD) 
	std::cerr<<"You must specify a magnetic field model!"<<std::endl;
      if (e == NO_BMODEL) 
	std::cerr<<"Your specified Magnetic Field model "<<bf<<" has not been implemented yet!"<<std::endl;
      std::cerr<<"Fatal error in readDragonInputFile!"<<std::endl;
    }
  
    return;
  }

  void Input::readGammaParams(TiXmlDocument * doc){
  
    TiXmlElement  * el  = NULL;
    TiXmlElement  * el1 = NULL;
  
    el = doc->FirstChildElement("ComputeGamma");
    
    if (el) {
      doComputeGammaMaps = true;
      
      minEnergy = QueryDoubleAttribute("Emin", el);
      maxEnergy = QueryDoubleAttribute("Emax", el);
      ratioEnergy = QueryDoubleAttribute("Efactor", el);
      rToCoarseGrid = QueryDoubleAttribute("rToCoarseGrid", el);
      zToCoarseGrid = QueryDoubleAttribute("zToCoarseGrid", el);
      doComputeAnisotropicIC = el->FirstChildElement("AnisotropicIC");
    }

    return;
  }
  
  void Input::readGasParams(TiXmlDocument * doc){

    TiXmlElement  * el  = NULL;
    TiXmlElement  * el1 = NULL;
  
    std::string gastype;
    std::string xcomode;
  
    try {
    
      el = doc->FirstChildElement("Gas");
    
      if ( !el && (doComputeGammaMaps) ) 
	throw NO_GAS;
    
      if (el) {
	el->QueryStringAttribute("CO", &gastype);
	
	if (gastype == "Analytical") coModel = analyticalCO;
	else if (gastype == "Galprop") coModel = galpropRingsForCO;
	else if (gastype == "Pohl") coModel = pohlGasForCO;
	else if (gastype == "SpiralArms") coModel = galpropWithSpiralsForCO;
	else
	  throw NO_GAS_TYPE;
      
	el->QueryStringAttribute("HI", &gastype);

      	if (gastype == "Analytical") hiModel = analyticalHI;
	else if (gastype == "Galprop") hiModel = galpropRingsForHI;
	else if (gastype == "NS") hiModel = nakanishiSofueForHI;
	else if (gastype == "SpiralArms") hiModel = galpropWithSpiralsForHI;
	else
	  throw NO_GAS_TYPE;
	
	el->QueryStringAttribute("XCO", &xcomode);
      
	if (xcomode == "galprop") xcoModel = galpropXco;
	else if (xcomode == "dragon") xcoModel = dragonXco;
	else
	  throw NO_XCO;
                  
	if ( coModel == galpropWithSpiralsForCO || hiModel == galpropWithSpiralsForHI ){
	  el->QueryDoubleAttribute("SpiralExponent", &spiralExponent);
	}
      }
    }
    catch (int e){
      if (e == NO_GAS) std::cerr<<"You must have GAS to produce gamma-rays or neutrinos!"<<std::endl;
      if (e == NO_GAS_TYPE) std::cerr<<"Your specified Gas model has not been implemented yet!"<<std::endl;
      if (e == NO_XCO) std::cerr<<"Your specified XCO mode "<<xcomode<<" has not been implemented yet!"<<std::endl;
    }

    return;
  }

  void Input::readYieldParams(TiXmlDocument * doc){
  
    TiXmlElement * el  = NULL;
    TiXmlElement * el1 = NULL;
  
    std::string gyield;
  
    try {
      el = doc->FirstChildElement("GammaYield");
      
      if ( !el && (doComputeGammaMaps) )
	throw NO_YIELD;
      
      if (el) {
	el->QueryStringAttribute("type", &gyield);
	if (gyield == "GalpropPPMESON") gammaYieldModel = galpropGammaYield;
	else if (gyield == "Kamae06") 
	  gammaYieldModel = KamaeGammaYield;
	else
	  throw NO_YIELD_TYPE;
      }
    }
    catch (int e){
      if (e == NO_YIELD) 
	std::cerr<<"You must specify a gamma-ray or neutrino production model!"<<std::endl;
      if (e == NO_YIELD_TYPE)
	std::cerr<<"Your specified Interaction model "<<gyield<<" has not been implemented yet!"<<std::endl;
    }

    return;
  }

  void Input::readGridParams(TiXmlDocument * doc){
  
    TiXmlElement * el  = NULL;
    TiXmlElement * el1 = NULL;
  
    el = doc->FirstChildElement("Healpix");

    if (el) {
      doUseHealpix = true;
      el->QueryIntAttribute("resolution", &healpixResolution);
    }
  
    el = doc->FirstChildElement("Grid");
  
    try {
      if (el) {
	maxLong = QueryDoubleAttribute("Lmax", el);
	minLong = QueryDoubleAttribute("Lmin", el);
	maxLat = QueryDoubleAttribute("Bmax", el);
	minLat = QueryDoubleAttribute("Bmin", el);
      	nLong = QueryIntAttribute("nL", el);
	nLat = QueryIntAttribute("nB", el);
      }
      else
	throw NO_GRID;
    }
    catch (int e){
      std::cerr<<"You must provide GRID properties!"<<std::endl;
    }

    return;
  }

  void Input::printInputParameters() {
  
    std::cout<<"Run settings below: "<<std::endl;
    std::cout<<"... run name = "<<xmlFilenameInit<<std::endl;
    std::cout<<"... input file name = "<<dragonRunFilename<<std::endl;
    std::cout<<"... input file type = "<<filetype<<std::endl;

    std::cout<<"... compute gamma-rays? "<<doComputeGammaMaps<<std::endl;
  
    if (doComputeGammaMaps){
      std::cout<<"... compute anisotropic IC? "<<doComputeAnisotropicIC<<std::endl;
      std::cout<<"... energy ratio for gammas = "<<ratioEnergy<<std::endl;
      std::cout<<"... max Energy for gammas = "<<maxEnergy<<std::endl;
      std::cout<<"... min Energy for gammas = "<<minEnergy<<std::endl;
      std::cout<<"... Gamma yield model = "<<gammaYieldModel<<std::endl;
      std::cout<<"... CO Gas type = "<<coModel<<std::endl;
      std::cout<<"... HI Gas type = "<<hiModel<<std::endl;
      std::cout<<"... XCO model = "<<xcoModel<<std::endl;
      std::cout<<"... rToCoarseGrid = "<<rToCoarseGrid<<std::endl;
      std::cout<<"... zToCoarseGrid = "<<zToCoarseGrid<<std::endl;
    }
  
    /*
      cout << "... compute neutrinos? " << iNeutrino << endl;
  
      if (iNeutrino) {
      cout << "... Emax for neutrinos = " << Emaxneu << endl;
      cout << "... Emin for neutrinos = " << Eminneu << endl;
      cout << "... delta E / E for neutrinos = " << Eneufact << endl;
      }
  
      cout << "... compute synchrotron radiation? " << iSync << endl;

      if (iSync) {
      cout << "... delta nu / nu (synchro) = " << Nufact << endl;
      cout << "... Numax (synchro) = " << MaxFreq << endl;
      cout << "... Numin (synchro) = " << MinFreq << endl;
      cout << "... Magnetic Field model = " << BM << endl;
      cout << "... B0 disk = " << B0disk << endl;
      cout << "... B0 halo = " << B0halo << endl;
      cout << "... B0 turb = " << B0turb << endl;
      }
    */
  
    std::cout<<"... using Healpix format? "<<doUseHealpix<<std::endl;
  
    if (doUseHealpix) 
      std::cout<<"... resolution of Healpix map = "<<healpixResolution<<std::endl;
    std::cout<<"... L grid dimension = "<<nLong<<std::endl;
    std::cout<<"... B grid dimension = "<<nLat<<std::endl;
    std::cout<<"... minimum L = "<<minLong<<std::endl;
    std::cout<<"... maximum L = "<<maxLong<<std::endl;
    std::cout<<"... minimum B = "<<minLat<<std::endl;
    std::cout<<"... maximum B = "<<maxLat<<std::endl;
 
    std::cout<<std::endl;
    return ;
  }

  Input::Input(const Input& in){
    xcoModel = in.xcoModel;
    gammaYieldModel = in.gammaYieldModel;
    magneticFieldModel = in.magneticFieldModel;
    coModel = in.coModel;
    hiModel = in.hiModel;
    minFrequency = in.minFrequency;
    maxFrequency = in.maxFrequency;
    ratioFrequency = in.ratioFrequency;
    bDiskAtSun = in.bDiskAtSun;
    bHaloAtSun = in.bHaloAtSun;
    bTurbAtSun = in.bTurbAtSun;
    betaMagField = in.betaMagField;
    minEnergy = in.minEnergy;
    maxEnergy = in.maxEnergy;
    ratioEnergy = in.ratioEnergy;
    spiralExponent = in.spiralExponent;
  }

} // namespace hesky  
