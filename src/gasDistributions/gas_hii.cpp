#include "gas_hii.h"

namespace hesky {
  
  HIIGasDistribution::HIIGasDistribution(Galaxy * gal_) : Gas(gal_) {
    
    std::cout<<"... calculating the smooth HII distribution"<<std::endl;
    
    averageGas = std::vector<double>(nr*nz, 0.0);
    
    const double ddz = deltaz*0.1;
    
    for ( int i=0;i<nr;i++ ){
      double r = rGrid[i];
      for ( int j=0;j<nz;j++ ) 
	averageGas[Gas::index2D(i,j)] = 
	  std::max(AnalyticModel::nHIIinGalaxyCoordAveraged(r, zGrid[j], deltaz, ddz), small);
    }
  }
  
} // namespace

