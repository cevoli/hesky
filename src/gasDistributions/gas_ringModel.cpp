#include "gas_ringModel.h"

namespace hesky {

  RingGasModel::RingGasModel(Galaxy* gal_, const int& whichGas_) {
    
    galaxyGrid = gal_;
    
    nl = galaxyGrid->GetNl();
    nb = galaxyGrid->GetNb();
    deltal = galaxyGrid->GetDeltal();
    deltab = galaxyGrid->GetDeltab();
    lGrid = galaxyGrid->GetL();
    bGrid = galaxyGrid->GetB();
    
    inputGasFilename = assignInputGasFilename(whichGas_);
    readRingGasFromFITS();
    mapRebinning();
  }
  
  std::string RingGasModel::assignInputGasFilename(const int& whichGas_) {
    
    if (whichGas_==H2_CODE) {
      if ( galaxyGrid->GetInput()->coModel == galpropRingsForCO ||
	   galaxyGrid->GetInput()->coModel == analyticalCO ) 
	return coGalpropFile;
      else throw NO_GAS;
    }
    
    else if (whichGas_==HI_CODE) {
      if ( galaxyGrid->GetInput()->hiModel == galpropRingsForHI || 
	   galaxyGrid->GetInput()->hiModel == analyticalHI ) 
	return hiGalpropFile;
      else throw NO_GAS;
    }
    
    else throw NO_GAS;

    std::string empty = "emptyGas";
    return empty;
  }
  
  void RingGasModel::readRingGasFromFITS() {
    
    fitsfile * ringsfile = NULL;
    
    int status = 0;    
    
    if ( fits_open_file(&ringsfile, inputGasFilename.c_str(), READONLY, &status) ) 
      fits_report_error(stderr, status);
    
    int anynul;
    int fpixel = 1;
    float nullval = 1.0e-15;
    
    int NAXIS1, NAXIS2, NAXIS3;
    
    if (fits_read_key(ringsfile, TINT, "NAXIS1", &NAXIS1, NULL, &status)) 
      fits_report_error(stderr, status);
    if (fits_read_key(ringsfile, TINT, "NAXIS2", &NAXIS2, NULL, &status)) 
      fits_report_error(stderr, status);
    if (fits_read_key(ringsfile, TINT, "NAXIS3", &NAXIS3, NULL, &status)) 
      fits_report_error(stderr, status);
    
    long nelements = NAXIS1*NAXIS2*NAXIS3;
    
    nRingsInl = NAXIS1, nRingsInb = NAXIS2, nRings = NAXIS3;
      
    std::cout<<"... reading gas file with nelements = "<<nelements<<" and nRings = "<<nRings<<std::endl;
    
    if (fits_read_key(ringsfile, TDOUBLE, "CRVAL1", &CRVAL1, NULL, &status)) 
      fits_report_error(stderr, status);
    if (fits_read_key(ringsfile, TDOUBLE, "CDELT1", &CDELT1, NULL, &status)) 
      fits_report_error(stderr, status);
    if (fits_read_key(ringsfile, TDOUBLE, "CRVAL2", &CRVAL2, NULL, &status)) 
      fits_report_error(stderr, status);
    if (fits_read_key(ringsfile, TDOUBLE, "CDELT2", &CDELT2, NULL, &status)) 
      fits_report_error(stderr, status);
    
    lmin_rings = CRVAL1;
    lmax_rings = CRVAL1 + (nRingsInl-1)*CDELT1;
    bmin_rings = CRVAL2;
    bmax_rings = CRVAL2 + (nRingsInb-1)*CDELT2;
    
    float * ringsArrayTemp = new float[nelements]();
    ringsMatrixTemp.resize (nelements, 0.0);

    if (fits_read_img(ringsfile, TFLOAT, fpixel, nelements, &nullval, ringsArrayTemp, &anynul, &status)) 
      fits_report_error(stderr, status);
    
    int HDU_TYPE;
    
    if( fits_movabs_hdu(ringsfile,2,&HDU_TYPE,&status) ) 
      fits_report_error(stderr, status);
    
    binsAlongR = std::vector<float>(2*nRings,0.0); // two columns: Rmin, Rmax
    
    long long int FIRSTROW = 1, FIRSTELEM = 1, N_ELEMENTS = nRings;

    for(int i_col=0;i_col<2;i_col++) // read each column
      if( fits_read_col(ringsfile,TFLOAT,i_col+1,FIRSTROW,FIRSTELEM,N_ELEMENTS,&nullval,&binsAlongR[i_col*nRings],&anynul,&status) ) 
	fits_report_error(stderr, status);
        
    if ( fits_close_file(ringsfile, &status) ) 
      fits_report_error(stderr, status);

    long counter = 0;
    
    for (int k = 0; k < nRings; k++) {
      for (int j = 0; j < nRingsInb; j++) {
	for (int i = 0; i < nRingsInl; i++) {
	  ringsMatrixTemp[index(i,j,k)] = std::max(ringsArrayTemp[counter], float(0.0));
	  counter++;
	}
      }
    }

    delete [] ringsArrayTemp;
    ringsArrayTemp = NULL;
    return;
  }
  
  void RingGasModel::mapRebinning() { // Rebinning of gas maps, as in Galprop
    
    if ( galaxyGrid->GetInput()->doUseHealpix ){
      nl = int(ceil((lmax_rings-lmin_rings)/deltal));
      nb = int(ceil((bmax_rings-bmin_rings)/deltab));
      deltal = (lmax_rings-lmin_rings)/double(nl-1);
      deltab = (bmax_rings-bmin_rings)/double(nb-1);
    }
    
    double ldel_new, lmin_new;
    int nl_new;
    
    ldel_new = CDELT1;
    lmin_new = lmin_rings;
    nl_new = nRingsInl;
    
    bool changel = false;
    
    if (ldel_new < deltal) {
      ldel_new = deltal;
      nl_new = nl;
      changel = true;
    }
    
    double bdel_new, bmin_new;
    int nb_new;
    bool changeb = false;
    
    bdel_new = CDELT2;
    bmin_new = bmin_rings;
    nb_new = nRingsInb;
    
    if (bdel_new < deltab) {
      bdel_new = deltab;
      nb_new = nb;
      changeb = true;
    }
    
    if (changel || changeb) std::cout<<"\t"<<"Start rebinning"<<std::endl;
    
    int nelements_gal = nl_new*nb_new*nRings;
    
    if (!changeb && !changel) ringsMatrix = ringsMatrixTemp;
    else {
      ringsMatrix = std::vector<float>(nelements_gal, 0.0);
      std::vector<int> n_used(nelements_gal, 0);
        
      for(int i_Ring = 0; i_Ring < nRings; i_Ring++) {
	for(int i_b = 0; i_b < nb_new; i_b++) {
	  for(int i_l = 0; i_l < nl_new; i_l++) {
                    
	    double l = lmin_new + i_l*ldel_new;
	    double b = bmin_new + i_b*bdel_new;
                    
	    int i_long_in= (int) ( (l-lmin_rings)/CDELT1 + 0.5 );
	    int i_lat_in = (int) ( (b-bmin_rings)/CDELT2 + 0.5 );
	    int ind = index_gal(i_l, i_b, i_Ring);
	    ringsMatrix[ind] += ringsMatrixTemp[index(i_long_in, i_lat_in, i_Ring)];
	    n_used[ind]++;
                    
	  } //  i_long
	} //  i_lat
      } //  i_Ring
      
      // normalize by number of cells used in rebinning
      for(int i_Ring = 0; i_Ring < nRings; i_Ring++) {
	for(int i_lat = 0; i_lat < nb_new; i_lat++) {
	  for(int i_long = 0; i_long < nl_new; i_long++) {
	    int ind = index_gal(i_long, i_lat, i_Ring);
	    if(n_used[ind] > 0) {
	      ringsMatrix[ind] /= n_used[ind];
	    }
	  }
	}
      }
    }
    nb = nb_new;
    nl = nl_new;
    deltal = ldel_new;
    deltab = bdel_new;
    
    lGrid.assign(nl,0.0);
    bGrid.assign(nb,0.0);
    
    for (int i = 0; i < nl; i++) lGrid[i] = lmin_new + double(i)*deltal;
    for (int i = 0; i < nb; i++) bGrid[i] = bmin_new + double(i)*deltab;
    
    ringsMatrixTemp.clear();   
    return;
  }
  
  int RingGasModel::WhichRing(const double& r) {
    int i_r = 0;
    for (i_r = 0; i_r < nRings; i_r++) 
      if (r <= binsAlongR[nRings+i_r]) break;
    return i_r;
  }
  
  int RingGasModel::GetIndGal(const double& l, const double& b, const int& ring, double& t, double& u) {
    int il = int(floor((l-lGrid.front())/deltal));
    int ib = int(floor((b-bGrid.front())/deltab));
    if (il < 0) il = 0;
    if (ib < 0) ib = 0;
    if (il > nl-2) il = nl-2;
    if (ib > nb-2) ib = nb-2;
    t = (l - lGrid[il])/deltal;
    u = (b - bGrid[ib])/deltab;
    return index_gal(il,ib,ring);
  }
  
} // namespace
