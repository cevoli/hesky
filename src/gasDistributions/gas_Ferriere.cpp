#include "gas_Ferriere.h"
#include "constants.h"
#include "gasdensity_3D.h"
#include "galaxy.h"
#include "fitsio.h"

#include <algorithm>

using namespace std;

FerriereGas::FerriereGas(int nx_, int ny_, int nz_, Galaxy* gal) : Gas(gal) {
    nx_gas = nx_;
    ny_gas = ny_;
    nz_gas = nz_;
    xmin = 0.0;
    xmax = 20.0;
    ymin = 0.0;
    ymax = 20.0;
    zmin = -4.0;
    zmax = 4.0;
    
    for (int i = 0; i < nx_gas; i++) x_array.push_back(xmin + (double)i/(double)(nx_gas-1)*(xmax-xmin));
    for (int i = 0; i < ny_gas; i++) y_array.push_back(ymin + (double)i/(double)(ny_gas-1)*(ymax-ymin));
    for (int i = 0; i < nz_gas; i++) z_array.push_back(zmin + (double)i/(double)(nz_gas-1)*(zmax-zmin));
    
    gasdensity = vector<double>(nx_gas*ny_gas*nz_gas, 0.0);
    HIIdensity = vector<double>(nx_gas*ny_gas*nz_gas, 0.0);
    
    for (int i = 0; i < nx_gas; i++) {
        for (int j = 0; j < ny_gas; j++) {
            for (int k = 0; k < nz_gas; k++) {
                int ind = index(i,j,k);
                gasdensity[ind] = gas_density_3D_ferriere(x_array[i],y_array[j],z_array[k],0);
                HIIdensity[ind] = gas_density_3D_ferriere(x_array[i],y_array[j],z_array[k],3);
            }
        }
    }
}

FerriereGas::FerriereGas(string input_filename, Galaxy* gal) : Gas(gal) {
    
    fitsfile* infile = NULL;
    int status = 0;
    if (fits_open_file(&infile, input_filename.c_str(), READONLY, &status)) fits_report_error(stderr, status);
    
    int bitpix;
    int naxis;
    long npoints[3];
    long fpixel = 1;
    long nelements = 0;
    int anynul;
    float nullval = 0;
    char gkey1[] = "CRVAL1";
    char gkey2[] = "CDELT1";
    char gkey3[] = "CRVAL2";
    char gkey4[] = "CDELT2";
    char gkey5[] = "CRVAL3";
    char gkey6[] = "CDELT3";
    
    if (fits_get_img_param(infile, 3, &bitpix, &naxis, npoints, &status)) fits_report_error(stderr, status);
    
    nx_gas = npoints[0];
    ny_gas = npoints[1];
    nz_gas = npoints[2];
    nelements = nx_gas*ny_gas*nz_gas;
    cout << "\t" << "Number of gas points " << nelements << endl;
    double dx, dy, dz;
    double crxmin;
    fits_read_key(infile, TDOUBLE, gkey1, &xmin, NULL, &status);  
    fits_read_key(infile, TDOUBLE, gkey2, &dx, NULL, &status);
    fits_read_key(infile, TDOUBLE, gkey3, &ymin, NULL, &status);
    fits_read_key(infile, TDOUBLE, gkey4, &dy, NULL, &status);
    fits_read_key(infile, TDOUBLE, gkey5, &zmin, NULL, &status);
    fits_read_key(infile, TDOUBLE, gkey6, &dz, NULL, &status);
    
    x_array = vector<double>(nx_gas, 0.0);
    y_array = vector<double>(ny_gas, 0.0);
    z_array = vector<double>(nz_gas, 0.0);
    
    for (int i = 0; i < nx_gas; i++) x_array[i] = (xmin + (double)i*dx)/1e3;
    for (int i = 0; i < ny_gas; i++) y_array[i] = (ymin + (double)i*dy)/1e3;
    for (int i = 0; i < nz_gas; i++) z_array[i] = (zmin + (double)i*dz)/1e3;
    
    xmin /= 1e3;
    ymin /= 1e3;
    ymin /= 1e3;
    
    xmax = x_array.back();
    ymax = y_array.back();
    zmax = z_array.back();
    
    cout << "\t" << "Keywords read" << endl;
    
    float* gas_array = new float[nelements]();
    gasdensity = vector<double>(nelements, 0.0);
    HIIdensity = vector<double>(nelements, 0.0);
    
    fits_read_img(infile, TFLOAT, fpixel, nelements, &nullval, gas_array, &anynul, &status);
    fits_close_file(infile, &status);
    
    int indexone = 0;
    for (int k = 0; k < npoints[2]; ++k) {
        for (int j = 0; j < npoints[1]; ++j) {
            for (int i = 0; i < npoints[0]; ++i) {
                int ind = index(i,j,k);
                HIIdensity[ind] = gas_density_3D_ferriere(x_array[i], y_array[j], z_array[k], 3);
                gasdensity[ind] = 2.0*gas_array[indexone] + HIIdensity[ind] + gas_density_3D_ferriere(x_array[i], y_array[j], z_array[k], 1);
                indexone++;
            }
        }
    }
    
    delete [] gas_array;
    gas_array = NULL;
    
    cout << "\t" << "Gas data were successfully stored in memory" << endl;
    
    return ;
}

FerriereGas::FerriereGas(std::string H2file, std::string HIfile, Galaxy* gal) : Gas(gal) {
    fitsfile* infile = NULL;
    int status = 0;
    if (fits_open_file(&infile, H2file.c_str(), READONLY, &status)) fits_report_error(stderr, status);

    int bitpix;
    int naxis;
    long npoints[3];
    long fpixel = 1;
    long nelements = 0;
    int anynul;
    float nullval = 0;
    
    char gkey1[] = "CRVAL1";
    char gkey2[] = "CDELT1";
    char gkey3[] = "CRVAL2";
    char gkey4[] = "CDELT2";
    char gkey5[] = "CRVAL3";
    char gkey6[] = "CDELT3";
    
    if (fits_get_img_param(infile, 3, &bitpix, &naxis, npoints, &status)) fits_report_error(stderr, status);
    
    nx_gas = npoints[0];
    ny_gas = npoints[1];
    nz_gas = npoints[2];
    nelements = nx_gas*ny_gas*nz_gas;
    cout << "\t" << "Number of gas points " << nelements << endl;
    double dx, dy, dz;
    
    fits_read_key(infile, TDOUBLE, gkey1, &xmin, NULL, &status);
    fits_read_key(infile, TDOUBLE, gkey2, &dx, NULL, &status);
    fits_read_key(infile, TDOUBLE, gkey3, &ymin, NULL, &status);
    fits_read_key(infile, TDOUBLE, gkey4, &dy, NULL, &status);
    fits_read_key(infile, TDOUBLE, gkey5, &zmin, NULL, &status);
    fits_read_key(infile, TDOUBLE, gkey6, &dz, NULL, &status);
    
    x_array = vector<double>(nx_gas, 0.0);
    y_array = vector<double>(ny_gas, 0.0);
    z_array = vector<double>(nz_gas, 0.0);
    
    for (int i = 0; i < nx_gas; i++) x_array[i] = (xmin + (double)i*dx);
    for (int i = 0; i < ny_gas; i++) y_array[i] = (ymin + (double)i*dy);
    for (int i = 0; i < nz_gas; i++) z_array[i] = (zmin + (double)i*dz);
    
    xmax = x_array.back();
    ymax = y_array.back();
    zmax = z_array.back();
        
    cout << "\t" << "Keywords read" << endl;
    
    float* gasH2_array = new float[nelements]();
    float* gasHI_array = new float[nelements]();
    gasdensity = vector<double>(nelements, 0.0);
    HIIdensity = vector<double>(nx_gas*ny_gas*nz_gas, 0.0);
    
    fits_read_img(infile, TFLOAT, fpixel, nelements, &nullval, gasH2_array, &anynul, &status);
    
    fits_close_file(infile, &status);
    

    
    
    status = 0;
    fitsfile* infileone = NULL;
    if (fits_open_file (&infileone, HIfile.c_str(), READONLY, &status)) fits_report_error(stderr, status);
    
    if (fits_get_img_param(infileone, 3, &bitpix, &naxis, npoints, &status)) fits_report_error(stderr, status);
    
    fits_read_img(infileone, TFLOAT, fpixel, nelements, &nullval, gasHI_array, &anynul, &status);
    fits_close_file(infileone, &status);
    
    int index1 = 0;
  //  cout << "Starting NS debugging" << endl;
    for (int k = 0; k < npoints[2]; ++k) {
        for (int j = 0; j < npoints[1]; ++j) {
            for (int i = 0; i < npoints[0]; ++i) {
                int ind = index(i,j,k);
                HIIdensity[ind] = gas_density_3D_ferriere(x_array[i], y_array[j], z_array[k], 3);
                if (gasH2_array[index1] < 0 || gasH2_array[index1] > 1e5) gasH2_array[index1] = 0;
                gasdensity[ind] = 2.0*gasH2_array[index1] + HIIdensity[ind] + gasHI_array[index1];
                index1++;
/*                
                if (HIIdensity[ind] < 0 || HIIdensity[ind] > 1e10) cout << x_array[i] << " " << y_array[j] << " " << z_array[k] << " " << HIIdensity[ind] << " " << gasdensity[ind] << " " << gasH2_array[index1] << " " << gasHI_array[index1] <<  " " << HIIdensity[ind] << endl;
  */          
            }
        }
    }
//    cout << "Ending NS debugging" << endl;

    delete [] gasH2_array;
    delete [] gasHI_array;
    gasH2_array = NULL;
    gasHI_array = NULL;
    cout << "\t" << "Gas data were successfully stored in memory" << endl;
    
    return ;
}

/*
 int FerriereGas::GetXIndex(double xx, double& t) {
 int i = int(floor(fabs(xx)/(xmax-xmin)*(nx-1)));
 if (x_array[i] > fabs(xx) && i>0) i--;
 t = (fabs(xx)-x_array[i])/(x_array[i+1]-x_array[i]);
 
 return i;
 }
 
 int FerriereGas::GetYIndex(double yy, double& u) {
 int j = int(floor(fabs(yy)/(ymax-ymin)*(ny-1)));
 if (y_array[j] > fabs(yy) && j>0) j--;
 u = (fabs(yy)-y_array[j])/(y_array[j+1]-y_array[j]);
 
 return j;
 }
 
 int FerriereGas::GetZIndex(double zz, double& l) {
 int k = int(floor(fabs(zz-zmin)/(zmax-zmin)*(nz-1)));
 if (z_array[k] > fabs(zz) && k>0) k--;
 l = (fabs(zz)-z_array[k])/(z_array[k+1]-z_array[k]);
 
 return l;
 }
 
 double FerriereGas::GetDensity(double x, double y, double z, const int WhichGas) {
 
 //return 1;
 int i = int(fabs(x)/(xmax-xmin)*(nx-1));
 int j = int(fabs(y)/(ymax-ymin)*(ny-1));
 int k = int((z-zmin)/(zmax-zmin)*(nz-1));
 
 if (x_array[i] > fabs(x) && i>0) i--;
 if (y_array[j] > fabs(y) && j>0) j--;
 if (z_array[k] > z && k>0) k--;
 
 // R interpolation
 
 double t = (fabs(x)-x_array[i])/(x_array[i+1]-x_array[i]);
 double u = (fabs(y)-y_array[j])/(y_array[j+1]-y_array[j]);
 double gas_zlow = 0;
 double gas_zhigh = 0;
 
 int ind = index(i,j,k);
 double onetoneu = (1.-t)*(1.-u);
 double onetu = (1.-t)*(u);
 double tu = (t)*(u);
 double toneu = (t)*(1.-u);
 
 
 switch (WhichGas) {
 case 0 : 
 gas_zlow = gasdensity[ind]*onetoneu + gasdensity[ind+ny*nz]*toneu + gasdensity[ind+ny*nz+nz]*tu + gasdensity[ind+nz]*onetu;
 gas_zhigh = gasdensity[ind+1]*onetoneu + gasdensity[ind+ny*nz+1]*toneu + gasdensity[ind+ny*nz+nz+1]*tu + gasdensity[ind+nz+1]*onetu;
 break ;
 case 3 : 
 gas_zlow = HIIdensity[ind]*onetoneu + HIIdensity[ind+ny*nz]*toneu + HIIdensity[ind+ny*nz+nz]*tu + HIIdensity[ind+nz]*onetu;
 gas_zhigh = HIIdensity[ind+1]*onetoneu + HIIdensity[ind+ny*nz+1]*toneu + HIIdensity[ind+ny*nz+nz+1]*tu + HIIdensity[ind+nz+1]*onetu;
 break ;
 case 4 :
 gas_zlow = nonHIIdensity[ind]*onetoneu + nonHIIdensity[ind+ny*nz]*toneu + nonHIIdensity[ind+ny*nz+nz]*tu + nonHIIdensity[ind+nz]*onetu;
 gas_zhigh = nonHIIdensity[ind+1]*onetoneu + nonHIIdensity[ind+ny*nz+1]*toneu + nonHIIdensity[ind+ny*nz+nz+1]*tu + nonHIIdensity[ind+nz+1]*onetu;
 break ;
 default :
 gas_zlow = -1;
 gas_zhigh = -1;
 break;
 }
 u = (z-z_array[k])/(z_array[k+1]-z_array[k]);
 
 return (gas_zlow*(1.-u)+gas_zhigh*u);
 }
 
 double FerriereGas::GetDensity(int x, int y, int z, const int WhichGas) {
 if (WhichGas == 0) {
 return gasdensity[index(x,y,z)];
 }
 else if (WhichGas == 3) {
 HIIdensity[index(x,y,z)];
 }
 else if (WhichGas == 4) {
 return nonHIIdensity[index(x,y,z)];
 }
 
 return 0.0;
 }
 
 void FerriereGas::Print(const char*, int) {
 
 return ;
 }
 */
