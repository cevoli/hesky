#include "gas_co.h"

namespace hesky {
  
  COGasDistribution::COGasDistribution(Galaxy * gal_) : Gas (gal_), RingGasModel (gal_,H2_CODE) {
    
    std::cout<<"... calculating the smooth H2 gas distribution"<<std::endl;
    
    xcoModel = gal_->GetInput()->xcoModel;
    
    if ( gal_->GetInput()->coModel == pohlGasForCO ) // case Pohl:
      readCoModelFromFITS(PohlAverage);
    
    else if ( gal_->GetInput()->coModel == galpropRingsForCO ||
	      gal_->GetInput()->coModel == analyticalCO ){   
      
      averageGas = std::vector<double>(nr*nz, 0.0);
	
      const double ddz = deltaz*0.1;
      
      for ( int i=0;i<nr;i++ ) {
	double r = rGrid[i];
	for ( int j=0;j<nz;j++ ) 
	  averageGas[Gas::index2D(i,j)] = 
	    std::max(AnalyticModel::nH2inGalaxyCoordAveraged(r, zGrid[j], deltaz, ddz), small);
      }
    }
    
    else throw NO_GAS_TYPE;
  }
  
  void COGasDistribution::readCoModelFromFITS(const std::string& filename_)
  {
    fitsfile * outfile = NULL;
    
    int status = 0;
    const int naxis = 2;
        
    int fpixel = 1;
    int bitpix = FLOAT_IMG;
    
    if (fits_open_file(&outfile, filename_.c_str(), READONLY, &status)) 
      fits_report_error(stderr, status);
    
    int anynul;
    int crnr, crnz;
    double rmin, zmin;
    
    if (fits_read_key(outfile, TINT, "NAXIS1", &nr, NULL, &status)) 
      fits_report_error(stderr, status);
    if (fits_read_key(outfile, TINT, "NAXIS2", &nz, NULL, &status)) 
      fits_report_error(stderr, status);
    if (fits_read_key(outfile, TDOUBLE, "CRVAL1", &rmin, NULL, &status)) 
      fits_report_error(stderr, status);
    if (fits_read_key(outfile, TDOUBLE, "CDELT1", &deltar, NULL, &status)) 
      fits_report_error(stderr, status);
    if (fits_read_key(outfile, TDOUBLE, "CRVAL2", &zmin, NULL, &status)) 
      fits_report_error(stderr, status);
    if (fits_read_key(outfile, TDOUBLE, "CDELT2", &deltaz, NULL, &status)) 
      fits_report_error(stderr, status);
    
    rGrid.clear();
    zGrid.clear();
    
    for (int i = 0; i < nz;i++) zGrid.push_back(zmin + double(i)/double(nz-1)*deltaz);
    for (int i = 0; i < nr;i++) rGrid.push_back(rmin + double(i)/double(nr-1)*deltar);

    averageGas = std::vector<double>(nr*nz, 0.0);

    long nelements = nr*nz;
    float nullval  = 1.e-15;
    float * values = new float[nelements]();
    int counter = 0;

    if (fits_read_img(outfile, TFLOAT, fpixel, nelements, &nullval, values, &anynul, &status)) 
      fits_report_error(stderr, status);
    
    for (int ib = 0; ib < nz; ++ib) {
      for (int il = 0; il < nr; ++il) {
	averageGas[Gas::index2D(il,ib)] = values[counter];
	counter++;
      }
    }
    
    if ( fits_close_file(outfile, &status) ) 
      fits_report_error(stderr, status);
    
    delete [] values;
    return;
  }

  double COGasDistribution::X_CO(const double& r) {
    
    switch (xcoModel) {

    case constantXco:
      return 1;
      break;
      
    case galpropXco:
      if (r < 3.5)
	return 0.4;
      else if (r < 5.5)
	return 0.6;
      else if (r < 7.5)
	return 0.8;
      else if (r < 9.5)
	return 1.5;
      else return 10.;
      break;

    case dragonXco:
      if (r < 2.0)
	return 0.5;
      else
	return 1.5;
      break;

    default:
      return 1.;
    }
  }

} // namespace
