#include "gas_analyticModel.h"

namespace AnalyticModel {

  double nH2inGalaxy(const double& rInKpc, 
		     const double& zInKpc ){ // [B88]/Table 3
    
    double nH2_ = 0.0;
    double fR,fZ0,fZh;                                          
    
    double R[18] = { 0.00, 2.25, 2.75, 3.25, 3.75, 4.25, 4.75, 5.25, 5.75,  // kpc, col.1
		     6.25, 6.75, 7.25, 7.75, 8.25, 8.75, 9.25, 9.75,10.25};
    double Y[18] = { 0.00, 1.5, 3.3, 5.8, 5.5, 8.4, 9.0, 9.6, 8.6,          // CO, K km s^-1 (col.4)
		     9.10, 7.9, 9.2, 7.7, 5.0, 3.6, 4.8, 1.7, 0.0};        
    double Z0[18] = { 0.039, 0.039, 0.036, 0.000, -.008, 0.001, -.010, -.001, -.004,    // kpc, col.7
		      -.019, -.022, -.014, -.009, -.004, 0.013, -.004, -.020, -.020};
    double Zh[18] = { 0.077, 0.077, 0.080, 0.061, 0.065, 0.071, 0.072, 0.082, 0.083,    // kpc, col.10
		      0.073, 0.063, 0.058, 0.072, 0.080, 0.066, 0.023, 0.147, 0.147};
  
    const double H2toCO = 1.e20; // [SM96]
  
    if(rInKpc > R[17]) return nH2_;
  
    int i;

    for ( i=0;i<17;i++ )  
      if(R[i] <= rInKpc && rInKpc <= R[i+1])  break;
    
    fR  =  Y[i] + ( Y[i+1]- Y[i])/(R[i+1]-R[i])*(rInKpc-R[i]); 
    fZ0 = Z0[i] + (Z0[i+1]-Z0[i])/(R[i+1]-R[i])*(rInKpc-R[i]);
    fZh = Zh[i] + (Zh[i+1]-Zh[i])/(R[i+1]-R[i])*(rInKpc-R[i]);

    nH2_ = fR * exp(-log(2.)*pow((zInKpc-fZ0)/fZh,2))*H2toCO/KPC2CM;
    
    return nH2_ < 0. ? 0.: nH2_;
  }
  
  double nHIinGalaxy(const double& rInKpc,
		     const double& zInKpc) { // Table 1 [GB76]
    
    double R[30] = { 0.0, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0,  // kpc, col.1
		     6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5, 10.0, 10.5, 11.0,
		     11.5, 12.0, 12.5, 13.0, 13.5, 14.0, 14.5, 15.0, 15.5, 16.0};
    
    double Y[30] = { .10, .13, .14, .16, .19, .25, .30, .33, .32, .31,       // nHI, cm^-3
		     .30, .37, .38, .36, .32, .29, .38, .40, .25, .23,       // (col.3)
		     .32, .36, .32, .25, .16, .10, .09, .08, .06, .00};
    
    double fR,fZ,fZ1=0,fZ2=0,R1,R2=R[29],Y1,Y2=Y[29];
    double nGB=0.33,nDL =0.57;    // cm^-3, disk density @ 4-8 kpc; [GB76], [DL90]
    double A1=0.395,z1=0.212/2.0, // cm^-3, kpc; Z-distribution parameters from [DL90]
      A2=0.107,z2=0.530/2.0,
      B =0.064,zh=0.403;
    
    int i;

    for ( i=0;i<29;i++ ) 
      if(R[i] <= rInKpc && rInKpc <= R[i+1])  break;
    
    R1 = (R[i]+R[i+1])/2;   
    Y1 = Y[i];
    
    if( rInKpc<R1 ){  
      if (i>0) { 
	R2 = (R[i-1]+R[i])/2; 
	Y2 = Y[i-1]; 
      }
      else { 
	R2 = R[0]; 
	Y2 = Y[0]; 
      }
    }
    else if (i<28) { 
      R2 = (R[i+1]+R[i+2])/2; 
      Y2 = Y[i+1]; 
    }
    
    fR = Y1+(Y2-Y1)/(R2-R1)*(rInKpc-R1); // interpolation in R!
    
    R2 = (R[28]+R[29])/2.0;
    
    if (rInKpc>R2) fR = Y[28]*exp(-(rInKpc-R2)/3.0); // extrapolation in R!
    
    // calculation of Z-dependence
    
    if (rInKpc<10.) // [DL90]
      fZ1 = A1*exp(-log(2.)*pow(zInKpc/z1,2))+A2*exp(-log(2.)*pow(zInKpc/z2,2))+B*exp(-fabs(zInKpc)/zh);
    
    if (rInKpc>8.) // [C86] 
      fZ2 = nDL*exp(-pow(zInKpc/(0.0523*exp(0.11*rInKpc)),2)); 
    
    if (rInKpc <= 8.) 
      fZ = fZ1;
    else {
      if (rInKpc >=10.) fZ = fZ2;
      else fZ = fZ1+(fZ2-fZ1)/2.0*(rInKpc-8.0); // interp. [DL90] & [C86]
    }
    return fZ*fR/nGB;
  }
  
  double nHIIinGalaxy(const double& rInKpc,
		      const double& zInKpc)
  {
    const double fne1 = 0.025, H1 = 1.00, A1 = 20.0;
    const double fne2 = 0.200, H2 = 0.15, A2 = 2.0;
    const double R2 = 4.0;
    const double ne1 = fne1 * exp(-fabs(zInKpc)/H1) * exp (-pow(rInKpc/A1,2));
    const double ne2 = fne2 * exp(-fabs(zInKpc)/H2) * exp (-pow((rInKpc-R2)/A2,2));
    
    return ne1+ne2;
  }
  
  double nH2inGalaxylbAveraged(const double& l,
			       const double& b,
			       const double& db,
			       const double& ddb,
			       const double& d,
			       const double& dd,
			       const double& ddd) 
  {
    const double cosl = cos(l*DegToRad);
    double nH2_av_lb_ = 0;

    int nuse=0;
    
    for(double bb=b-db/2.+ddb/2.; bb<b+db/2.; bb+=ddb) {
      if (fabs(bb) > 90.) continue;
      const double sinb = sin(bb*DegToRad);
      const double cosb = cos(bb*DegToRad);
        
      for(double d1=d-dd/2.+ddd/2.; d1<d+dd/2.; d1+=ddd) {
	const double z = d1*sinb;                      // altitude of the current point 
	const double R = sqrt(rsun*rsun+pow(d1*cosb,2)
			      -2.0*rsun*d1*cosb*cosl); // Galactocentric distance of the current point
	
	nH2_av_lb_ += nH2inGalaxy(R,z);
	nuse++;
      }
    }
    return nH2_av_lb_/double(nuse);
  }

  double nH2inGalaxyCoordAveraged(const double& R,
				  const double& z,
				  const double& dz,
				  const double& dzz)
  {  
    double nH2_av_ = 0.0;
    int nuse = 0;
  
    for ( double zz=z-dz/2.;zz<=z+dz/2.;zz+=dzz ){
      nH2_av_+=nH2inGalaxy(R,zz);
      nuse++;
    }
    return nH2_av_/nuse;
  }

  double nHIinGalaxyCoordAveraged(const double& R,
				  const double& z,
				  const double& dz,
				  const double& dzz)
  {
    double nHI_av_ = 0.0;
    int nuse = 0;
    
    for ( double zz=z-dz/2.; zz<=z+dz/2.; zz+=dzz ){
      nHI_av_+=nHIinGalaxy(R,zz);
      nuse++;
    }
    return nHI_av_/nuse;
  }

  double nHIIinGalaxyCoordAveraged(const double& R,
				   const double& z,
				   const double& dz,
				   const double& dzz)
  {  
    double nHII_av_ = 0.0;
    int nuse = 0;
    
    for ( double zz=z-dz/2.; zz<=z+dz/2.; zz+=dzz ){
      nHII_av_+=nHIIinGalaxy(R,zz);
      nuse++;
    }
    return nHII_av_/nuse;
  }
  
  double totalGasInGalaxy(const double& x,
			  const double& y,
			  const double& z,
			  const int& WhichGas ){
    
    double xpc, ypc, zpc, xc, yc, r, rpc, thetac, xb, yb, x2, y2, Xc, Lc, Hc2, HcI, H2_CMZ, HI_CMZ;
  
    //conversion from kpc to pc
    xpc = 1e3*x;
    ypc = 1e3*y;
    zpc = 1e3*z;
    
    r = sqrt(pow(x,2) + pow(y,2));
    rpc = r*1000.;
    
    double H2_density, HI_density, HII_density, Result = 0.;
    
    if (r < 2.0) {
        
      //Central Molecular Zone contribution 
      xc = -50.; yc = 50.;thetac = 70.0*DegToRad;
        
      //x2 and y2: variables that map CMZ disk
      xb = xpc - xc;
      yb = ypc - yc;
      x2 = (xb)*cos(thetac) + (yb)*sin(thetac);
      y2 = - (xb)*sin(thetac) + (yb)*cos(thetac);
      Xc = 125.;
      Lc = 137.;
      Hc2 = 18.;
      HcI = 54.;
    
      H2_CMZ = 150. * exp( - pow( ((sqrt( pow(x2,2) + pow((2.5*y2),2) ) - Xc) / (Lc)) ,4) ) * exp ( - pow((zpc/Hc2),2)  );
      HI_CMZ = 8.8 * exp( -  pow( ((sqrt( pow(x2,2) + pow((2.5*y2),2) ) - Xc) / (Lc)) ,4) ) * exp ( - pow((zpc/HcI),2)  );
    
      double y_3 = -10.; //pc
      double z_3 = -20.;
      double L3 = 145.;
      double H3 = 26.;
      double L2 = 3700.;
      double H2 = 140.;
      double L1 = 17000.;
      double H1 = 950.;
        
      double HII_CMZ;
      double pezzo3 = 8.*0.005*cos(3.141592*rpc/(2*L1))*sech(zpc/H1)*sech(zpc/H1);
        
      HII_CMZ = 8.0 * (exp(-(xpc*xpc+(ypc-y_3)*(ypc-y_3))/(L3*L3)) * exp(-((zpc-z_3)*(zpc-z_3))/(H3*H3))) + 8.*0.009 * exp(-((rpc-L2)*(rpc-L2))/(L2*L2/4.))*sech(zpc/H2)*sech(zpc/H2) + pezzo3;
        
      //GB disk contribution *****************************************************************************
        
      double alpha1, beta1, thetad1, Xd, Ld, Hd, HdI, x3, y3, z3, H2_GBdisk, HI_GBdisk;
        
      alpha1 = 13.5*DegToRad;
      beta1 = 20.0*DegToRad;
      thetad1 = 48.5*DegToRad;
    
      Xd = 1200.; //pc
      Ld = 438.;
      Hd = 42.;
      HdI = 120.;
        
      x3 = xpc*cos(beta1)*cos(thetad1) - ypc*(sin(alpha1)*sin(beta1)*cos(thetad1) - cos(alpha1)*sin(thetad1)) - zpc*(cos(alpha1)*sin(beta1)*cos(thetad1) + sin(alpha1)*sin(thetad1));
      y3 =  -xpc*cos(beta1)*cos(thetad1) + ypc*(sin(alpha1)*sin(beta1)*sin(thetad1) + cos(alpha1)*cos(thetad1)) + zpc*(cos(alpha1)*sin(beta1)*sin(thetad1) + sin(alpha1)*cos(thetad1));
      z3 = xpc*sin(beta1) + ypc*sin(alpha1)*cos(beta1) + zpc*cos(alpha1)*cos(beta1);
        
      H2_GBdisk = 4.8 * exp(- pow( ((sqrt( pow(x3,2) + pow((3.1*y3),2) ) - Xd ) / (Ld)) ,4) ) * exp(- pow((z3/Hd),2));
      HI_GBdisk = 0.34 * exp(- pow( ((sqrt( pow(x3,2) + pow((3.1*y3),2) ) - Xd ) / (Ld)) ,4) ) * exp(-pow((z3/HdI),2));
            
      // Totals for Ferriere model
        
      H2_density =  (1.0/0.5) * (H2_CMZ + H2_GBdisk); // Correction factor: XCO 0.5 --> 1.0
      HI_density = HI_CMZ + HI_GBdisk;
      HII_density = HII_CMZ;
    }
  
    else { // if (r > 2.0) uses Galprop model
    
      //disk contribution (r > 2 kpc) for molecular gas: Bronfman et al. ********************************
    
      H2_density = nH2inGalaxy(r,z);
      HI_density = nHIinGalaxy(r,z);
      HII_density = nHIIinGalaxy(r,z);
    }
  
    // XCO correction factors
      
    if (r < 2.) H2_density *= 0.5;
    else H2_density *= 1.2;
    
    // Result ******************************************************************************************
  
    if (WhichGas == 2) Result = H2_density;
    if (WhichGas == 1) Result = HI_density;
    if (WhichGas == 0) Result = 2.0*H2_density + HI_density + HII_density;
    if (WhichGas == 4) Result = 2.0*H2_density + HI_density;
    if (WhichGas == 3) Result = HII_density;
  
    return Result;
  }

} // namespace
