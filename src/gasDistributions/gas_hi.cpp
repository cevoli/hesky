#include "gas_hi.h"

namespace hesky {

  HIGasDistribution::HIGasDistribution(Galaxy* gal_) : Gas(gal_), RingGasModel(gal_,HI_CODE) {
    
    std::cout<<"... calculating the smooth HI gas distribution"<<std::endl;
    
    if (gal_->GetInput()->hiModel == nakanishiSofueForHI) // case NS:
      readHiModelFromFITS(NSAverage);
    
    else if (gal_->GetInput()->hiModel == galpropRingsForHI || // case GalpropRingsHI:
	     gal_->GetInput()->hiModel == analyticalHI) { 
      
      averageGas = std::vector<double>(nr*nz, 0.0);
      
      const double ddz = deltaz*0.1;
      
      for (int i = 0; i < nr; i++) {
	double r = rGrid[i];
	for (int j=0; j<nz; j++) 
	  averageGas[Gas::index2D(i,j)] 
	    = std::max(AnalyticModel::nHIinGalaxyCoordAveraged(r, zGrid[j], deltaz, ddz), small);
      }
    }
    
    else throw NO_GAS_TYPE;
  }
  
  void HIGasDistribution::readHiModelFromFITS(const std::string& filename_) {
    
    fitsfile * outfile = NULL;
    
    const int naxis = 2;
    int status = 0;
    int fpixel = 1;
    int bitpix = FLOAT_IMG;
    int anynul;
    double rmin,zmin;
    
    if (fits_open_file(&outfile, filename_.c_str(), READONLY, &status)) 
      fits_report_error(stderr, status);
    
    if (fits_read_key(outfile, TINT, "NAXIS1", &nr, NULL, &status)) 
      fits_report_error(stderr, status);
    if (fits_read_key(outfile, TINT, "NAXIS2", &nz, NULL, &status)) 
      fits_report_error(stderr, status);
    if (fits_read_key(outfile, TDOUBLE, "CRVAL1", &rmin, NULL, &status)) 
      fits_report_error(stderr, status);
    if (fits_read_key(outfile, TDOUBLE, "CDELT1", &deltar, NULL, &status)) 
	fits_report_error(stderr, status);
    if (fits_read_key(outfile, TDOUBLE, "CRVAL2", &zmin, NULL, &status)) 
	fits_report_error(stderr, status);
    if (fits_read_key(outfile, TDOUBLE, "CDELT2", &deltaz, NULL, &status)) 
      fits_report_error(stderr, status);

    rGrid.clear(); zGrid.clear();
    
    for (int i = 1; i < nz;i++) zGrid.push_back(zmin + double(i)/double(nr-1)*deltaz);
    for (int i = 1; i < nr;i++) rGrid.push_back(rmin + double(i)/double(nr-1)*deltar);
    
    averageGas = std::vector<double>(nr*nz, 0.0);
    
    long nelements = nr*nz;
    float nullval = 1.e-15;
    float * values = new float[nelements]();
    int counter = 0;
    if (fits_read_img(outfile, TFLOAT, fpixel, nelements, &nullval, values, &anynul, &status)) 
      fits_report_error(stderr, status);
    
    for (int ib = 0; ib < nz; ++ib) {
      for (int il = 0; il < nr; ++il) {
	averageGas[Gas::index2D(il,ib)] = values[counter];
	counter++;
      }
    }
    
    if ( fits_close_file(outfile, &status) ) 
      fits_report_error(stderr, status);
    
    delete [] values;
    return;
  }
  
} // namespace
