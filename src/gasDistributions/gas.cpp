#include "gas.h"

namespace hesky {

  Gas::Gas(Galaxy* gal) :
    rGrid(gal->GetR()),
    zGrid(gal->GetZ()),
    deltar(gal->GetDeltar()),
    deltaz(gal->GetDeltaz()),
    nr(gal->GetNr()),
    nz(gal->GetNz())
  { }
  
  double Gas::Get2DDensity(const double& r, 
			   const double& z){
    
    if (r > rGrid.back() || fabs(z) > zGrid.back()) return 0.0;
    
    int i = int((r-rGrid[0])/deltar);
    int j = int((z-zGrid[0])/deltaz);
    
    while (i > nr-1) i--;
    while (j > nz-1) j--;
    
    double t = (r-rGrid[i])/deltar;
    double u = (z-zGrid[j])/deltaz;
    
    int ind = index2D(i,j);
    
    return averageGas[ind]*(1.-t)*(1-u) 
      + averageGas[ind+1]*(1.-t)*(u) 
      + averageGas[ind+nz]*(t)*(1-u) 
      + averageGas[ind+nz+1]*(t)*(u);
  }
  
} // namespace
