#include "particles.h"

//CR data are read in (m^2 s GeV sr)^-1. We have to convert into  (cm^2 s GeV sr)^-1.
#define CONVERTDENSITYFACTOR 0.0001

namespace hesky {

  Particle::Particle() {
    isPresent = false;
    nr = -1;
    ny = -1;
    nz = -1;
    nE = -1;
    atomicNr = -3;
    massNr = -1;
    Deltar = 0.0;
    Deltay = 0.0;
    Deltaz = 0.0;
    rmax = 0;
    rmin = 0;
    ymax = 0;
    ymin = 0;
    zmax = 0;
    zmin = 0;
    Ekmin = -1;
    Ekfact = 0;
  }

  Particle::Particle(const Particle& p) {
    isPresent = p.isPresent;
    nr = p.nr;
    ny = p.ny;
    nz = p.nz;
    nE = p.nE;
    atomicNr = p.atomicNr;
    massNr = p.massNr;
    Deltar = p.Deltar;
    Deltay = p.Deltay;
    Deltaz = p.Deltaz;
    rmax = p.rmax;
    rmin = p.rmin;
    ymax = p.ymax;
    ymin = p.ymin;
    zmax = p.zmax;
    zmin = p.zmin;
    Ekmin = p.Ekmin;
    Ekfact = p.Ekfact;
    EnergyGrid.assign(p.EnergyGrid.begin(), p.EnergyGrid.end());
    rGrid.assign(p.rGrid.begin(), p.rGrid.end());
    yGrid.assign(p.yGrid.begin(), p.yGrid.end());
    zGrid.assign(p.zGrid.begin(), p.zGrid.end());
    density.assign(p.density.begin(),p.density.end());
  }
  
  Particle::Particle(const int& A_, const int& Z_) {

    isPresent = true;
    massNr = A_;
    atomicNr = Z_;
    
    const double GalaxySize = 20.;    // [kpc] 
    const double GalaxyGrid = 1.;     // [kpc]
    const double HaloSize = 5.;       // [kpc]
    const double MinimumEnergy = 0.1; // [GeV]
    const double MaximumEnergy = 300; // [GeV]
    
    for ( double r=0;r<=GalaxySize;r+=GalaxyGrid )
      rGrid.push_back(r);
    
    for ( double z=-HaloSize;z<=HaloSize;z+=GalaxyGrid/2. )
      zGrid.push_back(z);
    
    for ( double E=MinimumEnergy;E<=MaximumEnergy;E*=1.2 )
      EnergyGrid.push_back(E);
    
    nr = rGrid.size();
    ny = 0;
    nz = zGrid.size();
    nE = EnergyGrid.size();
    
    Deltar = rGrid[1]-rGrid[0];
    Deltaz = zGrid[1]-zGrid[0];
    Deltay = 0.0;
    
    rmax = rGrid.back();
    rmin = rGrid.front();
    zmax = zGrid.back();
    zmin = zGrid.front();
    ymin = 0;
    ymax = 0;
    
    Ekmin = EnergyGrid.front();
    Ekfact = EnergyGrid[1]/EnergyGrid[0];
      
    long nelements = nE*nr*nz;

    density = std::vector<double>(nelements,1.0);

    /*for (int j = 0; j < nz; j++) {
      for (int i = 0; i < nr; i++) {
	for (int k = 0; k < nE; k++) {
	  if ( rGrid[i]*rGrid[i] + zGrid[j]*zGrid[j] > 2.0*2.0 )
	    density[index(k,i,j)] = 0.0;
	} // for in k
      } // for in i
    } // for in j
    */

    return;
  }

  Particle::Particle(const std::string& inputFilename_, const int& A_, const int& Z_) {
  
    isPresent = false;
    massNr = A_;
    atomicNr = Z_;
    
    try {
      if (!checkFileExists(inputFilename_))
	throw NO_INPUT_FILE;
      
      readHeaderFromFITS(inputFilename_);
      readDensityFromFITS(inputFilename_);
    }
  
    catch (int e){
      std::cerr<<"Input file do not exist!"<<std::endl;
      exit(0);
    }
  
    return;
  }

  void Particle::readHeaderFromFITS(const std::string& inputFilename_) {

    FILE *fp;
    fp=fopen("cfitsio.log","w+");

    fitsfile * infile = NULL;
  
    int status = 0;
    int hdutype = 0;

    if ( fits_open_file(&infile, inputFilename_.c_str(), READONLY, &status) ) 
      fits_report_error(fp, status);
  
    if (fits_movabs_hdu(infile, 1, &hdutype, &status)) 
      fits_report_error(fp, status);
  
    if (fits_read_key(infile, TDOUBLE, "rmax",     &rmax,   NULL, &status)) fits_report_error(fp, status);
    if (fits_read_key(infile, TDOUBLE, "zmin",     &zmin,   NULL, &status)) fits_report_error(fp, status);
    if (fits_read_key(infile, TDOUBLE, "zmax",     &zmax,   NULL, &status)) fits_report_error(fp, status);
    if (fits_read_key(infile, TDOUBLE, "Ekmin",    &Ekmin,  NULL, &status)) fits_report_error(fp, status);
    if (fits_read_key(infile, TDOUBLE, "Ekin_fac", &Ekfact, NULL, &status)) fits_report_error(fp, status);
    if (fits_read_key(infile, TINT,    "dimE",     &nE,     NULL, &status)) fits_report_error(fp, status);
    if (fits_read_key(infile, TINT,    "dimr",     &nr,     NULL, &status)) fits_report_error(fp, status);
    if (fits_read_key(infile, TINT,    "dimz",     &nz,     NULL, &status)) fits_report_error(fp, status);
    if (fits_read_key(infile, TINT,    "dimy",     &ny,     NULL, &status)) fits_report_error(fp, status);  
  
    if (status) { std::cout<<"Setting up 2D grid"<<std::endl; ny=0; status=0; }
    else { std::cout<<"Setting up 3D grid"<<std::endl; status=0; }
  
    Deltar = rmax/(double)(nr-1);
    Deltay = (!ny) ? 0.0 : rmax/double(ny-1);
    Deltaz = (zmax-zmin)/(double)(nz-1);
  
    if (!ny) { 
      std::cout<<"... "<<"GCR will be read from a regular FITS file; E points: "<<nE;
      std::cout<<"; r points: "<<nr<<"; z points: "<<nz<<std::endl;
    }
    else {
      std::cout<<"... "<<"GCR will be read from a regular FITS file; E points: "<<nE;
      std::cout<<"; x points: "<<nr<<"; y points: "<<ny<<"; z points: "<<nz<<std::endl;
    }
    
    for (int i = 0; i < nr; ++i) rGrid.push_back((double)i*Deltar);
    for (int i = 0; i < ny; ++i) yGrid.push_back((double)i*Deltar);
    for (int i = 0; i < nz; ++i) zGrid.push_back(zmin + (double)i*Deltaz);
    for (int i = 0; i < nE; ++i) EnergyGrid.push_back(exp(log(Ekmin) + (double)i*log(Ekfact))); 
  
    if (fits_close_file(infile, &status)) fits_report_error(fp, status);
  
    fclose(fp);
    return;
  }

  void Particle::readDensityFromFITS(const std::string& inputFilename_) {
  
    fitsfile * infile = NULL;
    float * nullval  = NULL;
  
    int counterInd = 0;
    int anynul = -1;
    int status = 0;
    int hdu_index = 2;  
    int hdu_actual = 0;
    int hdu_num = 0;  
    int hdu_type = 0;
    int fpixel = 1;
    
    int Z=0, A=0;
    
    long nelements = (ny==0) ? nE*nr*nz : nE*nr*ny*nz;

    density = std::vector<double>(nelements,0.0);

    float * readingCrDensityVec = new float[nelements]();
    
    try {
      
      if ( fits_open_file(&infile, inputFilename_.c_str(), READONLY, &status) ) 
	fits_report_error(stderr, status);
    
      if ( fits_get_num_hdus(infile, &hdu_num, &status) )
	fits_report_error(stderr, status);

      while (hdu_actual < hdu_num && A <= massNr) {
	
	if (fits_movabs_hdu(infile, hdu_index, &hdu_type, &status)) 
	  fits_report_error(stderr, status); // Move to the next HDU (the first HDU = 1)
	
	if (hdu_type != IMAGE_HDU) std::cerr<<"Not an image!"<<std::endl;
      
	fits_get_hdu_num(infile, &hdu_actual);
      
	if ( fits_read_key(infile, TINT, "Z_", &Z, NULL, &status) )
	  fits_report_error(stderr, status);
	if ( fits_read_key(infile, TINT, "A",  &A, NULL, &status) ) 
	  fits_report_error(stderr, status);

	if (Z == atomicNr && A == massNr) {
	
	  isPresent = true;
	
	  std::cout<<"... "<<"reading nucleus with Z = "<<Z<<" A = "<<A<<" HDU = "<<hdu_actual<<std::endl;
	
	  if (fits_read_img(infile, TFLOAT, fpixel, nelements, nullval, readingCrDensityVec, &anynul, &status)) 
	    fits_report_error(stderr, status);
	
	  counterInd = 0;
	
	  if ( ny!=0 ) throw 1;
	
	  for (int j = 0; j < nz; j++) {
	    for (int i = 0; i < nr; i++) {
	      //for (int iy = 0; iy < ny; iy++) {
	      for (int k = 0; k < nE; k++) {
		//if (ny==-1) 
		density[index(k,i,j)] += double(readingCrDensityVec[counterInd]) * CONVERTDENSITYFACTOR;
		//else density[index(k,i,iy,j)] += double(cr_density[ind]) * .0001;
		counterInd++;
	      } // for in k
	    } // for in i
	  } // for in j
	} // if 
	
	hdu_index++;
      } // while
    }
  
    catch (int e) {
      if (e==1) std::cerr<<"3D mode not implemented yet!"<<std::endl;
    }
  
    delete [] readingCrDensityVec;
    readingCrDensityVec = NULL;
  
    if (fits_close_file(infile, &status)) fits_report_error(stderr, status);
    return;
  }

  const double Particle::CRdensity(int Eind, double r, double z) {
    double rr = fabs(r);
    int j = int(rr/Deltar);
    int k = int((z-zmin)/Deltaz);
    if (j<0) j=0;
    if (k<0) k=0;
    while (j >= nr-1) j--;
    while (k >= nz-1) k--;
  
    double t = (rr-rGrid[j])/(rGrid[j+1]-rGrid[j]);
    double u = (z-zGrid[k])/(zGrid[k+1]-zGrid[k]);
  
    return (density[index(Eind,j,k)]*(1.-t)*(1.-u) 
	    + density[index(Eind,j+1,k)]*(t)*(1.-u) 
	    + density[index(Eind,j+1,k+1)]*(t)*(u) 
	    + density[index(Eind,j,k+1)]*(1.-t)*(u));
  }
  
  const double Particle::CRdensity(int Eind, int j, int k, int l, double t, double u, double w) {
    const int basic1 = index(Eind,j,k,l);
    const int basic2 = index(Eind,j+1,k,l);
  
    double lowval = density[basic1]*(1.-u)*(1.-w) 
      + density[basic1+nE]*(1.-u)*(w) 
      + density[basic1+nz*nE]*(u)*(1.-w) 
      + density[basic1+nz*nE+nE]*(u)*(w);

    double highval = density[basic2]*(1.-u)*(1.-w) 
      + density[basic2+nE]*(1.-u)*(w) 
      + density[basic2+nz*nE]*(u)*(1.-w) 
      + density[basic2+nz*nE+nE]*(u)*(w);
  
    return (lowval*(1.-t) + highval*(t));
  }

  void Particle::CalcTUW(double xx, double yy, double z, double& t, double& u, double& w, int& j, int& k, int& l) {
    j = int((xx-rmin)/Deltar);
    k = int((yy-ymin)/Deltay);
    l = int((z-zmin)/Deltaz);
    if (j<0) j=0;
    if (k<0) k=0;
    if (l<0) l=0;
    while (j >= nr-1) j--;
    while (k >= ny-1) k--;
    while (l >= nz-1) l--;
  
    t = (xx-rGrid[j])/(rGrid[j+1]-rGrid[j]);
    u = (yy-yGrid[j])/(yGrid[j+1]-yGrid[j]);
    w = (z-zGrid[l])/(zGrid[l+1]-zGrid[l]);

    return ;
  }

  void Particle::CalcTU(double rr, double z, double& t, double& u, int& j, int& k) {
    j = int(rr/Deltar);
    k = int((z-zmin)/Deltaz);
    if (j<0) j=0;
    if (k<0) k=0;
    while (j >= nr-1) j--;
    while (k >= nz-1) k--;
  
    t = (rr-rGrid[j])/(rGrid[j+1]-rGrid[j]);
    u = (z-zGrid[k])/(zGrid[k+1]-zGrid[k]);
  
    return ;
  }

}



