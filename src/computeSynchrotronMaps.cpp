#include "computeSynchrotronMaps.h"

//#undef _OPENMP

#ifdef _OPENMP
#include <omp.h>
#endif

namespace hesky {

  ComputeSynchrotronMaps::ComputeSynchrotronMaps(Galaxy * galaxyGrid_){
    
    galaxyGrid = galaxyGrid_;
    in = galaxyGrid->GetInput();
    minz = galaxyGrid->GetZ().front();
    minFrequency = in->minFrequency;
    maxFrequency = in->maxFrequency;
    ratioFrequency = in->ratioFrequency;
    nFrequency = int(std::log(maxFrequency/minFrequency)/std::log(ratioFrequency) + 1.9);
    
    nb = galaxyGrid->GetNb();
    nl = galaxyGrid->GetNl();
    nz = galaxyGrid->GetNz();
    ny = galaxyGrid->GetNy();
    npix = galaxyGrid->GetNpix();

    if (in->doUseHealpix) {
      fluxParallelToHealpix = std::vector<double>(nFrequency*npix,0.0);
      fluxPerpendicularToHealpix = std::vector<double>(nFrequency*npix,0.0);
      fluxRandomToHealpix = std::vector<double>(nFrequency*npix,0.0);
    }
    else {
      flux_par = std::vector<double>(nFrequency*nl*nb,0.0); 
      flux_perp = std::vector<double>(nFrequency*nl*nb,0.0); 
      flux_rand = std::vector<double>(nFrequency*nl*nb,0.0); 
    }
    
    for ( int i=0;i<nFrequency;i++ ) 
      frequencyGrid.push_back(exp(log(minFrequency) + double(i)*log(ratioFrequency)));
  }
  
  ComputeSynchrotronMaps::~ComputeSynchrotronMaps() {
    
    frequencyGrid.clear();
    
    flux_par.clear();
    flux_perp.clear();
    flux_rand.clear();
    
    fluxParallelToHealpix.clear();
    fluxPerpendicularToHealpix.clear();
    fluxRandomToHealpix.clear();
  }
  
  void ComputeSynchrotronMaps::ComputeMap(std::vector<Particle*> part, 
					  MagneticField * B,  
					  const unsigned long& Initial_pixel, 
					  const unsigned long& Final_pixel ){
    
    std::cout<<"Computing synchrotron map..."<<std::endl;
    
    std::vector<double> rGrid = galaxyGrid->GetR();
    std::vector<double> yGrid = galaxyGrid->GetY();
    std::vector<double> zGrid = galaxyGrid->GetZ();
    std::vector<double> lGrid = galaxyGrid->GetL();
    std::vector<double> bGrid = galaxyGrid->GetB();
    
    const double rMax = (ny) ? sqrt(rGrid.back()*rGrid.back() + yGrid.back()*yGrid.back()) : rGrid.back();
    const double zMax = zGrid.back();
    //const double RSun = 8.3;
    const double factor = log(part[0]->GetEkfact())/cLight;//C();
    const double factnuc = 3.0/(4.0*M_PI)*emc;
    const unsigned long maxiter = in->doUseHealpix ? npix : nl*nb;
    const unsigned long nside = galaxyGrid->GetNside();    
    const int nEpr = part[0]->GetNE();
    
    if (in->doUseHealpix) 
      std::cout<<"... evaluating the synchrotron map in Healpix format"<<std::endl;
    
    unsigned long counter = 0;
    
    std::vector<double> PartE(part[0]->GetE());
    
    int iElectron = -1;
    int iPositron = -1;
    
    if (part[0]->WhoAmI() == hesky::ELECTRON) {
      iElectron = 0;
      if (part.size() > 1 && part[1]->WhoAmI() == hesky::POSITRON)
	iPositron = 1;
    }
    else if (part[0]->WhoAmI() == hesky::POSITRON)
      iPositron = 0;
    
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic) num_threads(NUMTHREADS)
#endif
    for (counter = Initial_pixel; counter <= Final_pixel; counter++) {
      
      if (counter%100 == 0 )
	std::cout<<counter<<"\t"<<Final_pixel<<std::endl;

      double l = 0;
      double b = 0;
      double ldeg = 0;
      double bdeg = 0;
      
      int i_long = -1;
      int i_lat = -1;
      
      double phis = -1;
      double xs = 0.0;
      double ys = 0.0;
      
      if (in->doUseHealpix) {
	Utils::pix2ang_ring(nside,counter,&b,&l);
	b = M_PI/2.0-b;
	bdeg = b/DegToRad;
	ldeg = l/DegToRad;
      }
      else {
	i_lat = counter%nb;
	bdeg = bGrid[i_lat];
	b = bdeg*DegToRad;
	if (i_lat == 0) {
	  i_long = counter/nb;
	  ldeg = lGrid[i_long];
	  l = ldeg*DegToRad;
	}
      }
      
      //#pragma omp critical
      //std::cout<<"coordinates now = "<<ldeg<<"\t"<<bdeg<<std::endl;
      
      const double sinl = sin(l);
      const double cosl = cos(l);
      const double sinb = sin(b);
      const double cosb = cos(b);

      for ( int iFreq=0;iFreq<nFrequency;iFreq++ ){

	double d = 0.0;
	double zz = 0.0;
	double RR = 0.0;
	double nubar = frequencyGrid[iFreq];
	int ind_flux = (in->doUseHealpix) 
	  ? index_healpix(counter, iFreq) 
	  : index_flux(i_long, i_lat, iFreq);
	
	while (RR < rMax && fabs(zz) < zMax) {	
	  
	  d += losStepInKpcInSynchro; 
	  zz=d*sinb;
	  const double dcosb = d*cosb;
	  RR = (xsun>0) // Galactocentric distance of point
	    ? xsun*xsun+pow(dcosb,2)-2.0*xsun*dcosb*cosl 
	    : xsun*xsun+pow(dcosb,2)+2.0*xsun*dcosb*cosl; 
	  if ( RR<1.0e-100 ) RR = 1.0e-100;
	  RR = sqrt(RR);
                
	  xs = (xsun>0) 
	    ? xsun*(1.1) - d*cosb*cosl 
	    : xsun*(1.1) + d*cosb*cosl;
	  ys = d*cosb*sinl;
	  
	  const double BperpInG = B->getBperp(xs, zz, ys);
	  const double BrandInG = B->getBrand(xs, zz, ys);
	  const double ConstRegularInErg = sqrt3e3mc2over2 * BperpInG;
	  const double ConstRandomInErg = 4.0 * sqrt3e3mc2over2 * BrandInG;
                
	  double emission_par = 0.0;
	  double emission_perp = 0.0;
	  double emission_rand = 0.0;
                
	  int j = -1;
	  int k = -1;
	  int l = -1;
	  double t = 0;
	  double u = 0;
	  double w = 0;
                
	  if (ny) part[0]->CalcTUW(xs, ys, zz, t, u, w, j, k, l);
	  else part[0]->CalcTU(RR, zz, t, u, j, k);
                
	  for ( int ip=0;ip<nEpr;ip++ ){
	    double lept = 0.0;
	    if (iElectron+1) lept  = (ny) 
			       ? part[iElectron]->CRdensity(ip, j, k, l, t, u, w) 
			       : part[iElectron]->CRdensity(ip, j, k, t, u);
	    if (iPositron+1) lept += (ny) 
			       ? part[iPositron]->CRdensity(ip, j, k, l, t, u, w) 
			       : part[iPositron]->CRdensity(ip, j, k, t, u);
	    lept *= PartE[ip];
	    
	    const double factnucgamma2 = factnuc*pow(1.0+PartE[ip]/Mele,2.0);
	    double nucRegular = factnucgamma2*BperpInG;
	    double nucRandom = 2.0*factnucgamma2*BrandInG;
	    
	    if (nucRandom < 1e-6 && BrandInG != 0) 
	      nucRandom = 1e-6; // Check physically!!
	    if (nucRegular < 1e-6 && BperpInG != 0) 
	      nucRegular = 1e-6; // Check physically!!
	    if (nucRegular > 0) {
	      const double xRegular = log10(nubar/nucRegular);
	      const double F = NumericalFunctions::SynchroF(xRegular);
	      const double G = NumericalFunctions::SynchroG(xRegular);
	      emission_par += lept * (F-G);
	      emission_perp += lept * (F+G); 
	    }
	    if (nucRandom > 0) {
	      double xRandom = nubar/nucRandom;
	      double logxrand = log10(xRandom);
	      double K13 = NumericalFunctions::SynchroK13(logxrand);
	      double K43 = NumericalFunctions::SynchroK43(logxrand);
	      emission_rand += lept * xRandom * xRandom * (K43*K13 - 3.0/5.0 * xRandom * (K43*K43 - K13*K13));
	    }
	  } // for ip               
	  
	  if (in->doUseHealpix) {
	    //int ind_heal = index_healpix(counter, iEgamma);
	    fluxParallelToHealpix[ind_flux] += emission_par * losStepInCmInSynchro * ConstRegularInErg * factor;
	    fluxPerpendicularToHealpix[ind_flux] += emission_perp * losStepInCmInSynchro * ConstRegularInErg * factor;
	    fluxRandomToHealpix[ind_flux] += emission_rand * losStepInCmInSynchro * ConstRandomInErg * factor;
	  }
	  else {
	    flux_par[ind_flux] +=  emission_par * losStepInCmInSynchro * ConstRegularInErg * factor;	
	    flux_perp[ind_flux] +=  emission_perp * losStepInCmInSynchro * ConstRegularInErg * factor;	
	    flux_rand[ind_flux] +=  emission_rand * losStepInCmInSynchro * ConstRandomInErg * factor;	
	  }
	  
	} // while
      }
    }
  
    std::cout<<"... done!"<<std::endl;
  
    return ;
  }

} // namespace



