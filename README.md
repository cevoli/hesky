# README #

### About HESKY ###

* HESKY is a code to calculate the nonthermal diffuse emissions from a given distribution of galactic cosmic rays. The relevant processes included are: pion production (both for photons and neutrinos), Bremsstrahlung, inverse Compton and synchrotron (both in intensity and polarization).
* The current version is 1.0.
* This project is part of the [DRAGON](http://dragonproject.org/) code.

### Requirements ###

* C++, C, Fortran compilers
* CFITSIO (tested with version > 3.3) 

### Installation ###

HESKY uses CMAKE to configure the Makefile. 

* mkdir build
* cd build
* cmake .. -DCMAKE_INSTALL_PREFIX=\`pwd\` -DCFITSIO_ROOT_DIR=*path to your cfitsio directory*
* make
* mkdir output

Download [DATA](http://www.desy.de/~cevoli/data_hesky_v1.0.tar.gz) file and untar it in the build directory.

### Example files ###

* examples/constantGcr.xml : GCR are set to constant.

### Support ###

In case of questions about the code please send do not hesitate to send an email to [Carmelo Evoli](mailto:carmelo.evoli@desy.de) or [Daniele Gaggero](daniele.gaggero@sissa.it) or to use the issue tracker [here](https://bitbucket.org/cevoli/hesky/issues).