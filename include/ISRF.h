#ifndef __ISRF_H
#define __ISRF_H

#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "constants.h"
#include "errorCodes.h"
#include "fitsio.h"

namespace hesky {
  
  class ISRF {
    
  protected:

    int dimnu;
    int nr;
    int ny;
    int nz;
    int ncomp;
    double rmin;
    double ymin;
    double zmin;
    double numin;
    double Dr;
    double Dy;
    double Dz;
    double Dnu;
    double logDnu;
    
    std::vector<double> nu;
    std::vector<double> r_array;
    std::vector<double> y_array;
    std::vector<double> z_array;
    std::vector<double> field;
    std::vector<double> field_comp;
    std::vector<double> en_dens;

    inline int index_field(int I, int K, int L) { return (I*nr + K)*nz+ L; }
    inline int index_endens(int I, int K, int L) { return (I*nz + K)*ncomp+ L; }
    inline int index_field(int I, int J, int K, int L) { return ((I*nr + J)*ny+K)*nz+ L; }
    inline int index_endens(int I, int J, int K, int L) { return ((I*ny+J)*nz + K)*ncomp+ L; }
    inline int isrf_index(int ir, int iz, int inu, int icomp, int Nr, int Nz, int Nnu, int Ncomp) {
      return icomp*Nnu *Nz*Nr+inu*Nz*Nr+iz*Nr+ir;
    }
    
  public:

    ISRF();
    ISRF(std::string,std::vector<double>,std::vector<double>,std::vector<double>);
    ISRF(const ISRF& rad);
    virtual ~ISRF() {
      nu.clear();
      r_array.clear();
      z_array.clear();
      field.clear();
      field_comp.clear();
      en_dens.clear();
    }
    
    inline int GetDimnu()                const { return dimnu;      }
    inline int GetNr()                   const { return nr;         }
    inline int GetNz()                   const { return nz;         }
    inline int GetNComp()                const { return ncomp;      }
    
    inline double GetRmin()              const { return rmin;       }
    inline double GetZmin()              const { return zmin;       }
    inline double GetNumin()             const { return numin;      }
    
    inline double GetDr()                const { return Dr;         }
    inline double GetDz()                const { return Dz;         }
    inline double GetDnu()               const { return Dnu;        }
    
    inline std::vector<double>& GetNu()        { return nu;         }
    inline std::vector<double>& GetR_()        { return r_array;    }
    inline std::vector<double>& GetZ_()        { return z_array;    }
    inline std::vector<double>& GetField()     { return field;      }
    inline std::vector<double>& GetFieldComp() { return field_comp; }

    const double GetDensity(int, int, int);
    const double GetDensity_over_Nu(int, int, int);
    const double GetDensity(double, double, int);
    const double GetDensity(double, double, double);
    const double GetDensity_over_Nu(double, double, int);
    const double GetDensity_over_Nu(double, double, double);
    
    const double GetDensity(int, int, int, int);
    const double GetDensity_over_Nu(int, int, int, int);
    const double GetDensity(double, double, int, int);
    const double GetDensity(double, double, double, int);
    const double GetDensity_over_Nu(double, double, int, int);
    const double GetDensity_over_Nu(double, double, double, int);
    
    inline const double GetDensity_over_Nu(int ind, double t, double u) { 
      return (field_comp[ind]*(1.-t)*(1.-u) 
	      + field_comp[ind+ny*nz*ncomp]*(t)*(1.-u) 
	      + field_comp[ind+ny*nz*ncomp+ncomp]*(t)*(u) 
	      + field_comp[ind+ncomp]*(1.-t)*(u)); }
    
    inline const double GetDensity_over_Nu(int ind, double t, double u, double l) { 
      return GetDensity_over_Nu(ind, t, u)*(1.-l) 
	+ GetDensity_over_Nu(ind+nz*ncomp, t, u)*(l); }
    
    inline int index_fieldcomp (int I, int K, int L, int J) { 
      return ((I*nr + K)*nz + L)*ncomp+J; }
    
    inline int index_fieldcomp (int I, int M, int K, int L, int J) { 
      return ((I*nr + K)*nz + L)*ncomp+J; }
    
    void Print();

    float isrf_energy_density(float, float, int);
};

} // namespace
#endif
