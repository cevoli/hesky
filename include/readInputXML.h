#ifndef _INPUT_H
#define _INPUT_H

#include <iostream>
#include <string>

#include "constants.h"
#include "errorCodes.h"
#include "tinyxml.h"

//class TiXmlElement;

namespace hesky {

  class Input {
    
  public:
    
    Input() { 
      doUseHealpix=false; 
      doComputeGammaMaps=false; 
      doComputeNeutrinoMaps=false;
      doComputeSynchrotronMaps=false; 
      doComputeAnisotropicIC=false; 

      healpixResolution = -1;
      nLong = 0;
      nLat = 0;
      
      maxLong = -1;
      minLong = -1;
      maxLat = -1;
      minLat = -1;
      minFrequency = -1;
      maxFrequency = -1;
      ratioFrequency = -1;
      bDiskAtSun = -1;
      bHaloAtSun = -1;
      bTurbAtSun = -1;
      betaMagField = -1;
      minEnergy = -1;
      maxEnergy = -1;
      ratioEnergy = -1;
      rToCoarseGrid = +100;
      zToCoarseGrid = +100;
      spiralExponent = -1;
    } 
     
    Input(const Input& in);
    ~Input() { }
    void printInputParameters();
    int loadFile(const std::string&);
    
    bool doUseHealpix;
    bool doComputeSynchrotronMaps;
    bool doComputeGammaMaps;
    bool doComputeNeutrinoMaps;
    bool doComputeAnisotropicIC;
    bool doOutputFITS;
    bool doOutputASCII;
    bool doOutputEmissivity;
    
    int healpixResolution;
    int nLong;
    int nLat;
    
    double maxLong;
    double minLong;
    double maxLat;
    double minLat;
    double minFrequency;
    double maxFrequency;
    double ratioFrequency;
    double bDiskAtSun;
    double bHaloAtSun;
    double bTurbAtSun;
    double betaMagField;
    double minEnergy;
    double maxEnergy;
    double ratioEnergy;
    double rToCoarseGrid;
    double zToCoarseGrid;
    double spiralExponent;
          
    std::string xmlFilename;
    std::string xmlFilenameInit;
    std::string dragonRunFilename;
    std::string dragonRunPath;
    std::string dragonRunFilenameFull;
    std::string filetype;
    std::string outputFITStype;

    xcoModels xcoModel;
    gammaYieldModels gammaYieldModel;
    magneticFieldModels magneticFieldModel;
    gasCoModels coModel;
    gasHiModels hiModel;
    
  private:
    
    int QueryIntAttribute(std::string, TiXmlElement*);
    double QueryDoubleAttribute(std::string, TiXmlElement*);
    void checkDragonRunPath();

    void readDragonInputFile(TiXmlDocument*);
    void readOutputParams(TiXmlDocument*);
    void readSynchrotronParams(TiXmlDocument*);
    void readGammaParams(TiXmlDocument*);  
    void readGasParams(TiXmlDocument*);
    void readYieldParams(TiXmlDocument*);
    void readGridParams(TiXmlDocument*);
  };

} // namespace hesky

#endif
