#ifndef _GALAXY_H
#define _GALAXY_H

#include <cmath>
#include <iostream> 
#include <vector>

#include "constants.h"
#include "errorCodes.h"
#include "fitsio.h"
#include "readInputXML.h"

//class Input;

namespace hesky {

  class Galaxy {
    
  public:
    
    Galaxy() { }
  
    Galaxy(Input * in_,
	   const std::vector<double>& r_,
	   const std::vector<double>& y_,
	   const std::vector<double>& z_){

      in = in_;
      nl = in_->nLong;
      nb = in_->nLat;
      lmin = in_->minLong;
      lmax = in_->maxLong;
      bmin = in_->minLat;
      bmax = in_->maxLat;
      deltal = (in_->maxLong-in_->minLong)/double(in_->nLong-1);
      deltab = (in_->maxLat-in_->minLat)/double(in_->nLat-1);
      healpixResolution = in_->healpixResolution;
      nside = in_->doUseHealpix ? pow(2.0, in_->healpixResolution) : 0;
      npix = 12*nside*nside;
      rGrid = r_;
      yGrid = y_;
      zGrid = z_;
      nr = rGrid.size();
      ny = yGrid.size();
      nz = zGrid.size();
      deltar = rGrid[1]-rGrid[0];
      deltaz = zGrid[1]-zGrid[0];
      if (yGrid.size() > 1) 
	deltay = yGrid[1]-yGrid[0];
      else deltay = 0;
      if (npix) angularResolution = 4.0*M_PI/sqrt(npix);
      else {
	lGrid = std::vector<double>(nl,0.0);
	bGrid = std::vector<double>(nb,0.0);
	for (int i = 0; i < nl; i++) lGrid[i] = lmin + double(i)*deltal;
	for (int i = 0; i < nb; i++) bGrid[i] = bmin + double(i)*deltab;
      }

      return;
    }

  Galaxy(const Galaxy& p) :
    in(p.in),
      nl(p.nl),
      nb(p.nb),
      nr(p.nr),
      ny(p.ny),
      nz(p.nz),
      nside(p.nside),
      npix(p.npix),
      angularResolution(p.angularResolution),    
      healpixResolution(p.healpixResolution),
      lmin(p.lmin),
      lmax(p.lmax),
      bmin(p.bmin),
      bmax(p.bmax),
      deltal(p.deltal),
      deltab(p.deltab),
      deltar(p.deltar),
      deltay(p.deltay),
      deltaz(p.deltaz),
      lGrid(p.lGrid),
      bGrid(p.bGrid),
      rGrid(p.rGrid),
      yGrid(p.yGrid),
      zGrid(p.zGrid)
	{ }
    
    virtual ~Galaxy() { 
      lGrid.clear();
      bGrid.clear();
      rGrid.clear();
      yGrid.clear();
      zGrid.clear();
    }
    
    inline int GetNl() const { return nl; }
    inline int GetNb() const { return nb; }
    inline int GetNr() const { return nr; }
    inline int GetNx() const { return nr; }
    inline int GetNy() const { return ny; }
    inline int GetNz() const { return nz; }
    inline double GetLmin() const { return lmin; }
    inline double GetLmax() const { return lmax; }
    inline double GetBmin() const { return bmin; }
    inline double GetBmax() const { return bmax; }
    inline double GetDeltal() const { return deltal; }
    inline double GetDeltab() const { return deltab; }
    inline double GetDeltar() const { return deltar; }
    inline double GetDeltax() const { return deltar; }
    inline double GetDeltay() const { return deltay; }
    inline double GetDeltaz() const { return deltaz; }
    inline unsigned long GetNpix() const { return npix; }
    inline unsigned long GetNside() const { return nside; }
    inline int GetHealpixResolution() const { return healpixResolution; }
    inline double GetAngularResolution() const { return angularResolution; }
    
    inline std::vector<double>& GetL() { return lGrid; }
    inline std::vector<double>& GetB() { return bGrid; }
    inline std::vector<double>& GetR() { return rGrid; }
    inline std::vector<double>& GetX() { return rGrid; }
    inline std::vector<double>& GetY() { return yGrid; }
    inline std::vector<double>& GetZ() { return zGrid; }
    
    inline Input* GetInput() { return in; }
    /*double GetSpiralExponent();*/
    /*void Print(fitsfile*, double, double, double, int);*/
    
  protected:
    
    Input * in;
    
    int nl;
    int nb;
    int nr;
    int ny;
    int nz;
    int healpixResolution;

    unsigned long nside;
    unsigned long npix;
    
    double angularResolution;
    double lmin;
    double lmax;
    double bmin;
    double bmax;
    double deltal;
    double deltab;
    double deltar;
    double deltay;
    double deltaz;
    
    std::vector<double> lGrid;
    std::vector<double> bGrid;
    std::vector<double> rGrid;
    std::vector<double> yGrid;
    std::vector<double> zGrid;
  };
  
}
#endif
