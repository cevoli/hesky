/*
 * DragonExceptions.h
 *
 *  Created on: 24/apr/2015
 *      Author: maccione
 */

#ifndef INCLUDE_DRAGONEXCEPTIONS_H_
#define INCLUDE_DRAGONEXCEPTIONS_H_

#include <exception>
#include <string>

namespace hesky {
	class ParticleNameNotImplementedException: public std::runtime_error {
		private:
			std::string reason;

		public:
			ParticleNameNotImplementedException(const std::string& str) : runtime_error(str), reason(str) { }
			virtual ~ParticleNameNotImplementedException() throw() {}
			virtual const char* what() {
				return reason.c_str();
			}
	};
}
#endif /* INCLUDE_DRAGONEXCEPTIONS_H_ */
