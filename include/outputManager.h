#ifndef __OUTPUT_MANAGER_H
#define __OUTPUT_MANAGER_H

#include <fstream>
#include <iostream>
#include <string>

#include "fitsio.h"
#include "hesky.h"
#include "readInputXML.h"

namespace hesky {

  class OutputManager {

  public:
    
    OutputManager() {}
    OutputManager(HESKY*);
    ~OutputManager();
    
    void saveMapsOnFITS();
    void saveEmissivityOnFITS();
    void saveMapsOnASCII();
 
  private:
    
    //int status;
    int nE;
    int nr;
    int nz;
    int nl;
    int nb;
    int nComp;
    int nFrequency;

    long npix;
    long nrows;
    long nside;
    
    double deltal;
    double deltab;
    double Emin;
    double Emax;
    double Efactor; 
    double angres;
    double minFrequency;
    double maxFrequency;
    double ratioFrequency;

    std::vector<double> bGrid;
    std::vector<double> lGrid;
    std::vector<double> rGrid;
    std::vector<double> zGrid;
    
    fitsfile * output_gamma_maps;
    fitsfile * output_synchrotron_maps;
    fitsfile * output_emissivity;
    
    HESKY * he;
    Input * in;
    
    std::string outputGammaFilenameFITS;
    std::string outputGammaFilenameASCII;
    std::string outputGammaFilenameEmissivityFITS;
    std::string outputGammaFilenameEmissivityASCII;
    std::string outputSynchroFilenameFITS;
    std::string outputSynchroFilenameASCII;
    
    std::string initFilename() {
      std::string i = "!output/";
      i += in->xmlFilenameInit;
      if (in->dragonRunFilename != "" ){
	i += "_";
	i += in->dragonRunFilename;  
      }
      if (in->doUseHealpix)
	i += "_healpix";
      else
	i += "_grid";
    
      return i;
    }
    
    std::string getProcessToString(const int&,const int&);
    void makeGammaFilenames();
    void makeSynchrotronFilenames();
    void saveMapsOnFITSwithGrid();
    void saveSynchroMapsOnFITSwithGrid();
    void saveMapsOnFITSwithHealpix();
    void saveSynchroMapsOnFITSwithHealpix();  
    void createSynchroOnFITSfirstHeader(); 
    void saveSynchroMapsOnFITSwithHealpixImageMode();
    void saveSynchroMapsOnFITSwithHealpixBinaryMode();
  };
  
} // namespace

#endif

