#ifndef _MAGNFIELD_H
#define _MAGNFIELD_H

#include <iostream>
#include <map>
#include <vector>

#include "constants.h"
#include "galaxy.h"

namespace hesky {
  
  class MagneticField {
    
  protected :
    
    int dim;
    
    std::vector<double> xGrid;
    std::vector<double> yGrid;
    std::vector<double> zGrid;
    std::vector<double> BRandom;
    
    std::map<int, std::vector<double> > BRegular;
    
    Galaxy * gal;
    
  public :
    
    MagneticField() { }
    MagneticField(const int& N, Galaxy * gal_);
    
    virtual ~MagneticField() { 
      xGrid.clear();
      yGrid.clear();
      BRegular.clear();
      BRandom.clear();
    }
    
    double getBtotal(const double& x,const double& z,const double& y);
    double getBperp(const double& x,const double& z,const double& y);
    double getBrand(const double& x,const double& z,const double& y);	
    inline int globalindex(const int& i,const int& j,const int& k) { return (i*dim+j)*dim+k; }
  };
  
  class UniformField : public MagneticField {
  public :
    UniformField(const int& N,const double& BValue,Galaxy* gal);
  };

} // namespace
#endif
