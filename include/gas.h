#ifndef _GAS_H
#define _GAS_H

#include <vector>
#include <iostream>
#include "galaxy.h"

namespace hesky {

  class Gas {
  
  public:
  
    Gas() { }
    Gas(Galaxy * gal);
  
    virtual ~Gas() { 
      averageGas.clear();
      rGrid.clear();
      zGrid.clear();
    }
    
    inline std::vector<double>& GetGas() { return averageGas; }
    inline int index2D(const int& ir, const int& iz) const { return ir*nz + iz; }
    inline double GetAverageDensity(const int& i, const int& j) const { return averageGas[index2D(i,j)]; }
    
    double Get2DDensity(const double&, const double &);


	/*  virtual double X_CO(double) { }
	virtual std::vector<double>& GetX() { return x_array; }
	virtual std::vector<double>& GetY() { return y_array; }
	virtual std::vector<double>& GetZ() { return z_array; }
	virtual std::vector<double>& GetGasTotal() { return gasdensity; }
	virtual std::vector<double>& GetGasHII()   { return HIIdensity; }
	virtual std::vector<double>& GetGasHI()    { return HIdensity; }
	virtual std::vector<double>& GetGasH2()    { return H2density; }
	virtual int index(int I, int J, int K) { return ((I)*ny_gas+(J))*nz_gas+(K); }
    */
    
    
  protected:

    int nr;
    int nz;
    
    double deltar;
    double deltaz;
    
    std::vector<double> averageGas;
    std::vector<double> rGrid;
    std::vector<double> zGrid;
    
    /*
      std::vector<double> gasdensity;
      std::vector<double> spiraldensity;
      std::vector<double> H2density;
      std::vector<double> HIdensity;
      std::vector<double> HIIdensity;
      std::vector<double> x_array;
      std::vector<double> y_array;
      std::vector<double> z_array;    
    */

    /*
      double xmin;
      double xmax;
      double ymin;
      double ymax;
      double zmin;
      double zmax;
      int nx_gas;
      int ny_gas;
      int nz_gas;
    */
  };

} // namespace
#endif
