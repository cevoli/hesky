#ifndef _COMPUTE_SYNCHRO_MAPS_H
#define _COMPUTE_SYNCHRO_MAPS_H

#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>

#include "galaxy.h"
#include "magneticField.h"
#include "particles.h"
#include "SynchrotronFunctions.h"
#include "utils.h"

//#include "input.h"
//#include "fitsio.h"

//class MagneticField;
//class Galaxy;
//class Particle;
//class SynchroFFunction;
//class SynchroGFunction;
//class SynchroK13Function;
//class SynchroK43Function;

//class Input;

namespace hesky {

  class ComputeSynchrotronMaps {
    
  public:
    
    ComputeSynchrotronMaps() {}
    ComputeSynchrotronMaps(Galaxy*);
    
    virtual ~ComputeSynchrotronMaps(); 
    
    void ComputeMap(std::vector<Particle*>,MagneticField*,const unsigned long&,const unsigned long&);
    
    inline int getNpix(){ 
      if (in->doUseHealpix)
	return npix;
      else
	return nl*nb;
    }
    
    int getNFrequency(){ return nFrequency; }
    double getMinFrequency(){ return minFrequency; }
    double getMaxFrequency(){ return maxFrequency; }
    double getRatioFrequency(){ return ratioFrequency; }
    
    inline double getParallelToHealpix(const int& i,const int& iFreq){
      return fluxParallelToHealpix[index_healpix(i,iFreq)]/(4.0*M_PI); }
    inline double getPerpendicularToHealpix(const int& i,const int& iFreq){
      return fluxPerpendicularToHealpix[index_healpix(i,iFreq)]/(4.0*M_PI); }
    inline double getRandomToHealpix(const int& i,const int& iFreq){
      return fluxRandomToHealpix[index_healpix(i,iFreq)]/(4.0*M_PI); }
    
  protected:
    
    Galaxy * galaxyGrid;
    Input * in;

    int nE;
    int nFrequency;
    int nb;
    int nl;
    int nz;
    int ny;

    unsigned long npix;
   
    double minz; // zmin
    double minFrequency; //numin;
    double maxFrequency; //numax;
    double ratioFrequency; // nufactor;
    
    std::vector<double> frequencyGrid;
    
    std::vector<double> flux_par;
    std::vector<double> flux_perp;
    std::vector<double> flux_rand;
    
    std::vector<double> fluxParallelToHealpix;
    std::vector<double> fluxPerpendicularToHealpix;
    std::vector<double> fluxRandomToHealpix;

    inline int index_flux(const int& I,const int& J,const int& K) { 
      return (I*nb+J)*nFrequency+K; }
    inline int index_healpix(const int& I,const int& J) { 
      return I*nFrequency+J; }
  };

} // namespace hesky

#endif
