#ifndef _PARTICLES_H
#define _PARTICLES_H

#include <iostream>
#include <sstream>
#include <vector>
#include <string>

#include "constants.h"
#include "errorCodes.h"
#include "DragonExceptions.h"
#include "fitsio.h"

namespace hesky {

typedef enum {
    PROTON,
    HELIUM,
    ELECTRON,
    POSITRON,
} ParticleName;

  class Particle {
    
  public:
    
    Particle();
    Particle(const std::string&, const int&, const int&);   
    Particle(const int&, const int&);
    Particle(const Particle& p);

    
    virtual ~Particle() {
      EnergyGrid.clear();
      rGrid.clear();
      yGrid.clear();
      zGrid.clear();
      density.clear();
    }
    
    inline const int GetNr() const { return nr; }
    inline const int GetNx() const { return nr; }
    inline const int GetNy() const { return ny; }
    inline const int GetNz() const { return nz; }
    inline const int GetNE() const { return nE; }
    inline const double GetDr() const { return Deltar; }
    inline const double GetDx() const { return Deltar; }
    inline const double GetDy() const { return Deltay; }
    inline const double GetDz() const { return Deltaz; }
    inline const double GetRmax() const { return rmax; }
    inline const double GetXmax() const { return rmax; }
    inline const double GetYmax() const { return ymax; }
    inline const double GetZmax() const { return zmax; }
    inline const double GetEkmin() const { return Ekmin; }
    inline const double GetEkfact() const { return Ekfact; }
    inline std::vector<double>& GetE() { return EnergyGrid;}
    inline std::vector<double>& GetR() { return rGrid;}
    inline std::vector<double>& GetX() { return rGrid;}
    inline std::vector<double>& GetY() { return yGrid;}
    inline std::vector<double>& GetZ() { return zGrid;}
    inline const int index (int J, int K, int L) { return ((J)*nr+(K))*nz+(L); }
    inline const int index (int I, int J, int K, int L) { return ((J*ny+K)*nz+L)*nE+I; }
    inline int GetZnumber() const { return atomicNr; }
    inline int GetAnumber() const { return massNr; }
    inline const bool checkIsPresent() const { return isPresent; }
  
    inline const double CRdensity(int i) const { return density[i]; }
    inline const double CRdensity(int Eind, int r, int z) { return density[index(Eind, r, z)]; }
    inline const double CRdensity(int Eind, int r, int y, int z) { return density[index(Eind, r, y, z)]; }
    inline const double CRdensity(int Eind, int j, int k, double t, double u) {
      const int basic = index(Eind,j,k);
      return (density[basic]*(1.-t)*(1.-u) 
	      + density[basic+nz]*(t)*(1.-u) 
	      + density[basic+nz+1]*(t)*(u) 
	      + density[basic+1]*(1.-t)*(u));
    }
  
    const double CRdensity(int Eind, int j, int k, int l, double t, double u, double w);
    const double CRdensity(int, double, double);

    ParticleName WhoAmI() {
      if (massNr==1 && atomicNr==1) return PROTON;
      if (massNr==4 && atomicNr==2) return HELIUM;
      if (massNr==0 && atomicNr==1) return POSITRON;
      if (massNr==0 && atomicNr==-1) return ELECTRON;
      std::ostringstream strstream;
      strstream << "Mass Number = " << massNr << " Atomic Number = " << atomicNr;
      throw hesky::ParticleNameNotImplementedException(strstream.str());
    }
    void CalcTU(double rr, double z, double& t, double& u, int& j, int& k);
    void CalcTUW(double xx, double yy, double z, double& t, double& u, double& w, int& j, int& k, int& l);

  protected:
  
    bool isPresent;  
    int nr;
    int ny;
    int nz;
    int nE;
    int atomicNr;
    int massNr;
    double Deltar;
    double Deltay;
    double Deltaz;
    double rmax;
    double rmin;
    double ymax;
    double ymin;
    double zmax;
    double zmin;
    double Ekmin;
    double Ekfact;
  
    std::vector<double> EnergyGrid;
    std::vector<double> rGrid;
    std::vector<double> yGrid;
    std::vector<double> zGrid;
    std::vector<double> density;
    
    void readHeaderFromFITS(const std::string&);
    void readDensityFromFITS(const std::string&);
  };

}
#endif
