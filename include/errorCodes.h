#ifndef _ERRORCODE_H
#define _ERRORCODE_H

#define NO_XML_FILE -1
#define NO_RADIATING_PARTICLES -2
#define NO_SYNCH_PARTICLES -3
#define NO_INPUT_FILE -4
#define NO_GAS -5
#define NO_GAS_TYPE -6
#define NO_XCO -7
#define NO_YIELD -8
#define NO_YIELD_TYPE -9
#define NO_BFIELD -10
#define NO_BMODEL -11
#define NO_GRID -12
#define PROBLEM_WITH_INPUT_FILE -13
#define ISRF_ENERGY_OUT_OF_RANGE -14
#define FILE_TYPE_NOT_SUPPORTED -15
#define NO_INPUT_FILETYPE -16
#define NO_ROOT -17
#define NOT_AN_IMAGE -18
#define NO_MAGNETIC_FIELD -19

#endif
