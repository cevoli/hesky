#ifndef _UTILS_H
#define _UTILS_H

#include <cmath>
#include <vector>

#include "constants.h"
#include "particles.h"
//#include "ISRF.h"
#include "readInputXML.h"
#include "cparamlib.h"

namespace Utils {
  
int findParticleIndex(hesky::ParticleName particleName, std::vector<hesky::Particle*>& particleDict);

  void InitElEm(std::vector<double>&, 
		std::vector<double>&, 
		int, int, 
		std::vector<double>&, 
		std::vector<double>&, 
		double, 
		std::vector<double>&);
  
  void InitPionEm(std::vector<double>&, 
		  int, int, 
		  std::vector<double>&, 
		  std::vector<double>&, 
		  double, int, 
		  hesky::Input*);

  /*void InitPionEm(std::vector<double>&,
		  std::vector<double>&,
		  int, int, 
		  std::vector<double>&, 
		  std::vector<double>&, 
		  double, int, 
		  hesky::Input*);*/
  
  double pp_meson_cc(double,double,int,int,int);
  double bremss_spec_cc(double, double, int, int);
  double aic_cc(int,int,double,double,double,double,double,double,double,double,double,int);
  double ICspectrum(double,double,double);
  void pix2ang_ring(long,long,double*,double*);
  
}

namespace KamaeYields {
  double GetSigma(double, double, int);
};

#endif


