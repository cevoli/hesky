#ifndef _CONSTANTS_H
#define _CONSTANTS_H

#include <string>
#include <cmath>
#include <fstream>

#define NUMTHREADS 16

inline double sgn(double a) { return (a>=0)-(a<0); }

inline bool checkFileExists(const std::string& filename) { std::ifstream ifile(filename.c_str()); return ifile; }

typedef enum { galpropGammaYield, 
	       KamaeGammaYield } gammaYieldModels;

typedef enum { constantXco,
	       galpropXco, 
	       dragonXco } xcoModels;

typedef enum { azimuthalBfield, 
	       spiralBfield, 
	       proudaBfield, 
	       wmapBfield, 
	       tkachevBfield, 
	       pshirkovBfield, 
	       farrarBfield, 
	       uniformBfield } magneticFieldModels;

typedef enum { analyticalCO,
	       galpropRingsForCO, 
	       galpropWithSpiralsForCO, 
	       pohlGasForCO } gasCoModels;

typedef enum { analyticalHI,
	       galpropRingsForHI, 
	       galpropWithSpiralsForHI, 
	       nakanishiSofueForHI } gasHiModels;

const double Pi = 3.14159265358979323846;
const double cLight = 2.99792458e10; // [cm/s]
const double DegToRad = Pi/180.0; // dtr
const double RadToDeg = 1.0/DegToRad; // rdt

const double KPC2CM = 3.085677580e21;

const double losStepInKpc = 0.1; // Step size (kpc)
const double losStepInCm = losStepInKpc * KPC2CM; // Step size (cm)

const double losStepInKpcInSynchro = 0.1; // Step size (kpc)
const double losStepInCmInSynchro = losStepInKpcInSynchro * KPC2CM; // Step size (cm)

const double maxzAboutGas = 1.0; // kpc

// CHECK HERE

const std::string hiGalpropFile = "DATA/rbands_hi12_qdeg_zmax1.fits.gz";
const std::string coGalpropFile = "DATA/rbands_co8_qdeg.fits.gz";
const std::string isrfGalpropFile = "DATA/MilkyWay_DR0.5_DZ0.1_DPHI10_RMAX20_ZMAX5_galprop_format.fits.gz";
const std::string PohlDatacube = "DATA/pohl.fits";
const std::string NSMolecularDatacube = "DATA/jap_H2cube.fits";
const std::string NSAtomicDatacube = "DATA/jap_HIcube.fits";
const std::string PohlH2ColumnDensity = "DATA/rings_pohl.fits.gz";
const std::string PohlAverage = "DATA/2D_pohl.fits.gz";
const std::string NSHIColumnDensity = "DATA/rings_japHI.fits.gz";
const std::string NSAverage = "DATA/2D_japHI.fits.gz";


const double eV_to_erg = 1.60217733e-12;
const double erg_to_eV = 1./eV_to_erg;
const double h_planck  = 6.6260755e-27 ; // erg sec
const double Rele= 2.8179409238e-13;      // cm, =e^2/mc^2 class. electron radius
const double Mele = 0.000511;  // GeV, electron mass

const double small = 1e-30;

const double year      = 365.25*24.*3600. ;
const double Myr       = 1e6*year ;
const double kpc       = 3.08568e21 ;
const double km        = 1e5 ;

// *************************************************************************************

const double rsun  = 8.5;                  // Sun radial position
const double xsun  = -8.5;
const double ysun  = 0;
const double zsun  = 0.;                   // Sun height

//const double d0      = 0.1;                  // Step size (kpc) for synchro
//const double ddmax   = 0.1;                  // Maxtep size (kpc) for synchro
//const double dd0     = 0.1;                  // Step size (kpc) for gamma aniso
//const double dddmax  = 0.5;                  // Maxtep size (kpc) for gamma aniso

//const double interesting_region_rmax = 10.;
//const double interesting_region_zmax =  1.;
//const double ds_factor = 10.; //amplification factor for the step size out of the central region defined by interesting_region_rmax/zmax

//const double synchro_interesting_region_rmax = 10.;
//const double synchro_interesting_region_zmax =  1.;
//const double synchro_dd_factor = 10.; //amplification factor for the step size out of the central region defined by interesting_region_rmax/zmax

const double sigmapp = 3.28e-26;             // Cross section (cm^2)
const double alpha   = 2.7;                  // Spectral index
const double yieldpi = 0.0451378;            // Yield
const double ang     = 1./(4.*Pi);         // Solid angle
const double feta    = 1.1;                  // Eta contribution
const double fA      = 1.44;                 // CR Nuclei + interstellar He
const double cnu     = .32;                  // Rapporto neutrini

const double mp   = 0.938;
const double mpi0 = 0.135;
const double He_abundance = 0.11;
const double m_e =  9.10939e-28 ;  // electron mass, g
const double e_e =  4.80321e-10 ;  // electron charge, esu
// Derived constants.
const double emc     =      e_e   / ( m_e     * cLight   );     //  e  /mc
const double e3mc2   =  pow(e_e,3) /( m_e * pow(cLight,2));     //  e^3/mc^2
const double sqrt3e3mc2over2 = 0.5*sqrt(3.0)*e3mc2;

// *************************************************************************************

#endif
