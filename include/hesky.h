#ifndef _HESKY_H
#define _HESKY_H

#include "computeGammaMaps.h"
#include "computeSynchrotronMaps.h"
#include "galaxy.h"
#include "gas.h"
#include "gas_co.h"
#include "gas_hi.h"
#include "gas_hii.h"
#include "ISRF.h"
#include "magneticField.h"
#include "particles.h"
#include "readInputXML.h"
#include "utils.h"

/*#include <vector>
#include "fitsio.h"
#include "gamma.h"
#include "neutrino.h"
#include "gas.h"
#include "gas_SpiralArms.h"
#include "input.h"
#include <iostream>
#include <string>
#include "gammasky.h"
#include "synchro.h"
#include "azimfield.h"
#include "spiralfield.h"
#include "proudafield.h"
#include "tkachevfield.h"
#include "pshirkovfield.h"
#include "farrar.h"
#include "wmapfield.h"


#include "gas_SpiralArms.h"
#include "gas_Ferriere.h"
#include "utils.h"
#include "constants.h"
#include "errorcode.h"

#ifndef _OPENMP

#include <pthread.h>

struct  parameter_list_type {
  unsigned long low_pixel;
  unsigned long high_pixel;
  GAMMASKY* gamma_object;
} ;

#endif

using namespace std;
*/

/*
class Particle;
class PromptGammaRays;
//class Input;
//class Galaxy;
class MagneticField;
//class Photon;
//class Gas;
class COGas;
class HIGas;
class HIIGas;
class Gas;
class SpiralArmGas;
class Synchrotron;
class ISRF;
*/

namespace hesky {
  
  //ISRF * radiationField = NULL;
  
  class HESKY {
    
  public :
    
    HESKY() { }
    HESKY(Input*);    
    
    void setWorldNull();
    void readingCR();
    void computeGalaxyComponents();

    void Run();
    
    ~HESKY();
    
    inline int getNpix(){ 
      if (in->doUseHealpix)
	return galaxyGrid->GetNpix();
      else
	return galaxyGrid->GetNl()*galaxyGrid->GetNb();
    }

    inline Galaxy * getGalaxy() { return galaxyGrid; }
    inline ComputeGammaMaps * getGammaMaps() { return gammaMaps; }
    inline ComputeSynchrotronMaps * getSynchrotronMaps() { return synchrotronMaps; }
    inline Input * getInput() { return in; }

    /*
    //inline std::vector<Gas*> Get_gas_density(){ return gas_density;}
    inline COGas* GetCOGas() { return CORing; }
    inline HIGas* GetHIGas() { return HIRing; }
    inline HIIGas* GetHIIGas() { return HIIav; }
    inline SpiralArmGas* GetTotalGas() { return totalgas_spiral; }
    inline std::vector<Particle*> Get_particle_vector(){ return elposvect;}
    inline MagneticField* Get_magnetic_field(){ return magnfield;} 
    inline PromptGammaRays* Get_PromptGammaRays(){ return pgr;}
    inline Photon* GetPhoton(){return ph;}
    inline Neutrino* GetNeutrino(){return neu;}
    inline Synchrotron* GetSynchrotron(){return synchandle;}
    */
    
  protected:
    
    int status;
    int nMapComponents;
    
    std::vector<Particle*> particleDict;
    std::vector<Particle*> leptonsDict;

    Input * in;
    Galaxy * galaxyGrid;
    HIGasDistribution * HI;
    COGasDistribution * H2;
    HIIGasDistribution * HII;
    MagneticField * magneticField;
    
    ComputeGammaMaps * gammaMaps;
    ComputeSynchrotronMaps * synchrotronMaps;
    
    /*// For Gamma-rays
      Particle* carb;

      PromptGammaRays* pgr;
      //std::vector<Gas*> gas_density;
      COGas* CORing;

      HIIGas* HIIav;
      SpiralArmGas* totalgas_spiral;
      Photon* ph;
      Neutrino* neu;
      fitsfile* outfile_g;
      fitsfile* outfile_n;
      fitsfile* outfile_g_emiss;
      
      // For Synchrotron


      Synchrotron* synchandle;
      fitsfile* outfile_s;*/
  };
} // namespace hesky

#endif
