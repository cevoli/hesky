#ifndef _GAS_HII_H
#define _GAS_HII_H

#include "gas.h"
#include "gas_analyticModel.h"

namespace hesky {
  
  class HIIGasDistribution : public Gas {
    
  public:
    
    HIIGasDistribution() { }
    HIIGasDistribution(Galaxy*);
    virtual ~HIIGasDistribution() { }
    
  };
  
} // namespace

#endif
