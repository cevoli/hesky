#ifndef _GAS_CO_H
#define _GAS_CO_H

#include "gas_ringModel.h"
#include "gas_analyticModel.h"
#include "gas.h"

namespace hesky {
  
  class COGasDistribution : public Gas, public RingGasModel {
    
  public:
    
    COGasDistribution() { }
    COGasDistribution(Galaxy*);
    virtual ~COGasDistribution() { }
    virtual double X_CO(const double&);
    
  protected:
    
    xcoModels xcoModel;
    void readCoModelFromFITS(const std::string&);
  
  };

} // namespace
#endif
