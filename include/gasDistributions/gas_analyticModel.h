#ifndef _GAS_ANALYTIC_MODEL_H
#define _GAS_ANALYTIC_MODEL_H

#include <cmath>
#include "constants.h"

inline double sech(const double A) {return 2.0*exp(A)/(exp(2.*A)+1.);}

namespace AnalyticModel {
  
  double nH2inGalaxy(const double&,const double&); // nH2_Gal
  double nHIinGalaxy(const double&,const double&); // nHI_Gal
  double nHIIinGalaxy(const double&,const double&); // nHII_Gal
  
  double nH2inGalaxylbAveraged(const double&,const double&,
			       const double&,const double&,
			       const double&,const double&,
			       const double&); // nH2_av_lb
  double nHIIinGalaxyCoordAveraged(const double&,const double&,
				   const double&,const double&); // nHII_av 
  double nH2inGalaxyCoordAveraged(const double&,const double&, 
				  const double&,const double&); // nH2_av
  double nHIinGalaxyCoordAveraged(const double&,const double&, 
				  const double&,const double&); // nHI_av
  double totalGasInGalaxy(const double&,const double&,
			  const double&,const int&); // gas_density_3D_ferriere
  
}

#endif
