#ifndef _GAS_RINGMODEL_H
#define _GAS_RINGMODEL_H

#include <cmath>
#include <fstream>
#include <string>
#include <vector>

#include "galaxy.h"

#define HI_CODE  1
#define H2_CODE  2
#define HII_CODE 3
 
namespace hesky {
  
  class RingGasModel {
    
  public:

    RingGasModel() { }
    RingGasModel(Galaxy*, const int&);
    
    virtual ~RingGasModel() {
      if (galaxyGrid) delete galaxyGrid;
      lGrid.clear();
      bGrid.clear();
      ringsMatrix.clear();
      binsAlongR.clear();
    }
    
    int GetNrings() const { return nRings; }
    std::vector<float>& GetRbins() { return binsAlongR; }
    inline double GetDensity(const int& l,const int& b,const int& r) { 
      return ringsMatrix[index_gal(l,b,r)]; }
    inline double GetDensity(const int& ind,const double& t,const double& u) { 
      return ringsMatrix[ind]*(1.0-t)*(1.0-u) 
	+ ringsMatrix[ind+nb*nRings]*(t)*(1.0-u) 
	+ ringsMatrix[ind+nRings]*(1.0-t)*(u) 
	+ ringsMatrix[ind+nRings*(nb+1)]*(t)*(u); }    
    
    int WhichRing(const double&);
    int GetIndGal(const double&,const double&,const int&,double&,double&);
    
  protected: 
    
    Galaxy * galaxyGrid;

    int nb, nl;
    int nRings, nRingsInb,  nRingsInl;
    double deltal, deltab;
    double CRVAL1, CDELT1;
    double CRVAL2, CDELT2;
    double lmin_rings, lmax_rings;
    double bmin_rings, bmax_rings;
    std::vector<double> lGrid, bGrid;
    std::vector<float> ringsMatrix, ringsMatrixTemp;
    std::vector<float> binsAlongR;
    std::string inputGasFilename;
    inline int index(int J, int K, int L) { return ((J)*nRingsInb+(K))*nRings+(L); }
    inline int index_gal(int J, int K, int L) { return ((J)*nb+(K))*nRings+(L); }
    
    std::string assignInputGasFilename(const int&);
    void readRingGasFromFITS();
    void mapRebinning();
  };
}
#endif
