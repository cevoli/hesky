#ifndef _GAS_HI_H
#define _GAS_HI_H

#include "gas_ringModel.h"
#include "gas_analyticModel.h"
#include "gas.h"

namespace hesky {
  
  class HIGasDistribution : public Gas, public RingGasModel {
    
  public:
    HIGasDistribution() { }
    HIGasDistribution(Galaxy*);
    virtual ~HIGasDistribution() { }
  
  protected:
    
    void readHiModelFromFITS(const std::string &);
  };
}

#endif
