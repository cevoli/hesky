#ifndef _COMPUTE_GAMMA_MAPS_H
#define _COMPUTE_GAMMA_MAPS_H

#include <algorithm>
#include <map>

#include "galaxy.h"
#include "gas_co.h"
#include "gas_hi.h"
#include "gas_hii.h"
#include "ISRF.h"
#include "particles.h"
#include "readInputXML.h"
#include "utils.h"

enum mapName { PionFromH2,
	       PionFromHI,
	       PionFromHII,
	       BremssFromH2,
	       BremssFromHI,
	       BremssFromHII,
	       InverseCompton };

typedef std::map<mapName, std::vector<double> > mapType;

namespace hesky {
  
  class Direction {

  public :

    int i_lat;
    int i_long;
    
    double sinl;
    double cosl;
    double sinb;
    double cosb;
    double l;
    double ldeg;
    double b;
    double bdeg;
    
    Direction(){}
    ~Direction(){}
    
  };

  class Los {

  public:
    
    int ind;
    int ind_isoHII;
    int ind_gas;
    int ir2d;
    int ir;
    int iz;
    int iy;
    int iEgamma;
    double d;
    double z;
    double r;  // RR -> Galactocentric distance of point
    double xs;
    double ys;
    double t;
    double u;
    double t2d;
    double dStep;
    double dStepFactor;
    double lpart;
    
    Los(){ 
      ind = ind_isoHII = ind_gas = ir2d = ir = iz = iy = -1;
      d = z = r = xs = ys = t = u = t2d = lpart = 0.; 
      dStep = losStepInKpc;
      dStepFactor = 1;
    }; 
    ~Los() {}
    
  };
    
  class Emissivity {
    
  public :
    
    std::vector<double> pi0fromH2;
    std::vector<double> pi0fromHI;
    std::vector<double> pi0fromHII;
    std::vector<double> bremssfromH2;
    std::vector<double> bremssfromHI;
    std::vector<double> bremssfromHII;
    
    Emissivity() { nring = 0; }
    Emissivity(const int& nring_){
      nring = nring_;
      pi0fromH2.assign(nring,0.0);
      pi0fromHI.assign(nring,0.0);
      pi0fromHII.assign(nring,0.0);
      bremssfromH2.assign(nring,0.0);
      bremssfromHI.assign(nring,0.0);
      bremssfromHII.assign(nring,0.0);
    }
    ~Emissivity(){
      pi0fromH2.clear();
      pi0fromHI.clear();
      pi0fromHII.clear();
      bremssfromH2.clear();
      bremssfromHI.clear();
      bremssfromHII.clear();
    }
    
    void Init(){
      try { 
	pi0fromH2.assign(nring,0.0); if (pi0fromH2.size() != nring) throw 1;
	pi0fromHI.assign(nring,0.0); if (pi0fromHI.size() != nring) throw 1;
	pi0fromHII.assign(nring,0.0); if (pi0fromHII.size() != nring) throw 1;
	bremssfromH2.assign(nring,0.0); if (bremssfromH2.size() != nring) throw 1;
	bremssfromHI.assign(nring,0.0); if (bremssfromHI.size() != nring) throw 1;
	bremssfromHII.assign(nring,0.0); if (bremssfromHII.size() != nring) throw 1;
      }
      catch (int e){
	std::cerr<<"Fatal error in Emissivity!"<<std::endl;
      }
      return;
    }

  private:
    
    int nring;
  
  };
  
  class ComputeGammaMaps {

  public:
  
    ComputeGammaMaps() { }
    ComputeGammaMaps(Galaxy*,COGasDistribution*,HIGasDistribution*,HIIGasDistribution*,const int&);
  
    ~ComputeGammaMaps() {
      if (galaxyGrid) delete galaxyGrid;
      if (in) delete in;
      if (H2) delete H2;
      if (HI) delete HI;
      if (HII) delete HII;
      energyGrid.clear();
      fluxToHealpix.clear();
      anisotropicIcToHealpix.clear(); 
      isotropicIcToHealpix.clear();   
      fluxToGrid.clear(); 
      anisotropicIcToGrid.clear(); 
      isotropicIcToGrid.clear();   
      emissionMaps.clear();
      emissionMapForIsotropic.clear(); 
      emissionMapForHII.clear();
    }
  
    inline double GetEmission(const int& ind,const double& t,const double& u) { 
      return emissionMaps[ind]*(1.-t)*(1.-u) 
	+ emissionMaps[ind+nz*nMapEnergy]*(t)*(1.-u) 
	+ emissionMaps[ind+nz*nMapEnergy+nMapEnergy]*(t)*(u) 
	+ emissionMaps[ind+nMapEnergy]*(1.-t)*(u); }
  
    inline double GetEmissionHII(const int& ind,const double& t,const double& u) { 
      return (emissionMapForHII[ind]*(1.-t)*(1.-u) 
	      + emissionMapForHII[ind+nz*nMapEnergy]*(t)*(1.-u) 
	      + emissionMapForHII[ind+nz*nMapEnergy+nMapEnergy]*(t)*(u) 
	      + emissionMapForHII[ind+nMapEnergy]*(1.-t)*(u)); }

    inline double GetEmissionIso(const int& ind,const double& t,const double& u) { 
      return emissionMapForIsotropic[ind]*(1.-t)*(1.-u) 
	+ emissionMapForIsotropic[ind+nz*nMapEnergy]*(t)*(1.-u) 
	+ emissionMapForIsotropic[ind+nz*nMapEnergy+nMapEnergy]*(t)*(u) 
	+ emissionMapForIsotropic[ind+nMapEnergy]*(1.-t)*(u); }
  
    inline double GetEmission(const int& ind,const double& t,const double& u,const double& l) { 
      return GetEmission3D(ind, t, u)*(1.-l) + GetEmission3D(ind+nz*nMapEnergy, t, u)*(l); }

    inline double GetEmissionHII(const int& ind,const double& t,const double& u,const double& l) { 
      return GetEmissionHII3D(ind, t, u)*(1.-l) + GetEmissionHII3D(ind+nz*nMapEnergy, t, u)*(l); }

    inline double GetEmissionIso(const int& ind,const double& t,const double& u,const double& l) { 
      return GetEmissionIso3D(ind, t, u)*(1.-l) + GetEmissionIso3D(ind+nz*nMapEnergy, t, u)*(l); }
  
    inline int GetNE(){ return nMapEnergy; }
    inline int GetNr(){ return nr; }
    inline int GetNz(){ return nz; }
    inline int GetNMapComponents(){ return nMapComponents; }
    inline double GetEmin(){ return minEnergy; }
    inline double GetEmax(){ return maxEnergy; }
    inline double GetEratio(){ return ratioEnergy; }
    
    inline double getE2FluxToHealpix(const int& iComp, const int& i, const int& iEgamma){
      return fluxToHealpix[index_fluxhealpix(iComp,i,iEgamma)]*pow(energyGrid[iEgamma],2); }
    //return mapToHealpix[InverseCompton].at(index_fluxhealpix(0,i,iEgamma))*pow(energyGrid[iEgamma],2); }
    
    //inline double getE2FluxToHealpix(const mapName& iMap, const int& i, const int& iEgamma){
    //  return fluxToHealpix[index_fluxhealpix(iComp,i,iEgamma)]*pow(energyGrid[iEgamma],2); }
    //return mapToHealpix[iMap].at(index_fluxhealpix(0,i,iEgamma))*pow(energyGrid[iEgamma],2); }
    
    inline double getEmissivityToOutput(const int& i, const int& j, const int& iEgamma){
      return emissionMaps[index_emission(0, i, j, iEgamma)]; }
    
    /* void ComputeMap(Particle*, Gas*, double);
       void ComputeMap(Particle*, Particle*, ISRF*);
       void ComputeMapCube(COGas*, HIGas*, Gas*, PromptGammaRays* prompt);
       
       void PrintMap(fitsfile*, PromptGammaRays* prompt);
       void PrintEmissionMap(fitsfile*); */
    
    void computeEmissionMap(std::vector<Particle*>,ISRF*);    
    void computeMapRings(unsigned long,unsigned long);
    
  protected:
    
    Galaxy * galaxyGrid;
    Input * in;
    
    COGasDistribution * H2;
    HIGasDistribution * HI;
    HIIGasDistribution * HII;

    int nl;
    int nb;
    int nr;
    int ny;
    int nz;
    int nMapEnergy;
    int nside;
    int nMapComponents;

    unsigned long nrnznE;
    unsigned long nrnynznE;
    unsigned long nlnbnE;
    unsigned long npixnE;
    unsigned long npix;
    unsigned long maxiter;

    double maxEnergy;
    double minEnergy;
    double ratioEnergy; // Efactor;
    double deltar;
    double deltaz;
    double deltay;
    double rmin;
    double rmax;
    double xmin;
    double xmax;
    double ymin;
    double ymax;
    double zmin;
    double zmax;

    std::vector<double> energyGrid; // E_grid
    std::vector<double> fluxToHealpix; //flux_healpix;
    std::vector<double> anisotropicIcToHealpix; // aniso_healpix;
    std::vector<double> isotropicIcToHealpix;   // iso_healpix;
    std::vector<double> fluxToGrid; //flux_healpix;
    std::vector<double> anisotropicIcToGrid; // aniso_healpix;
    std::vector<double> isotropicIcToGrid;   // iso_healpix;
    std::vector<double> emissionMaps; //emission;
    std::vector<double> emissionMapForIsotropic; //_iso;
    std::vector<double> emissionMapForHII; //_HII;

    std::map< mapName,std::vector<double> > mapToHealpix;
  
    std::vector<double> rGrid; // r_grid
    std::vector<double> yGrid; 
    std::vector<double> zGrid;
    std::vector<double> bGrid;
    std::vector<double> lGrid;
  
    inline int index_flux(int I, int J, int K, int L) { 
      return (((I)*nl+(J))*nb+(K))*nMapEnergy+(L); }
    inline int index_pgr_aniso_iso(int I, int J, int K) { 
      return ((I)*nb+(J))*nMapEnergy+(K); }
    inline int index_emission(int I, int J, int K, int L) { 
      return (((I)*nr+(J))*nz+(K))*nMapEnergy+(L); }
    inline int index_emission(int M, int I, int J, int K, int L) { 
      return ((((M)*nr+(I))*ny+(J))*nz+(K))*nMapEnergy+(L); }
    inline int index_emission_isoHII(int I, int J, int K) { 
      return (((I)*nz+(J))*nMapEnergy+(K)); }
    inline int index_emission_isoHII(int L, int I, int J, int K) { 
      return ((((L)*ny+(I))*nz+(J))*nMapEnergy+(K)); }
    inline int index_fluxhealpix(int I, int J, int K) { 
      return ((I)*npix+(J))*nMapEnergy+(K); }
    inline int index_healpix(int I, int J) { 
      return (I)*nMapEnergy+(J); }
    inline int index_brems(int i, int j, int k) { 
      return (i*nMapEnergy+j)*2+k; }

    inline double GetEmission3D(int ind, double t, double u) { 
      return emissionMaps[ind]*(1.-t)*(1.-u) 
	+ emissionMaps[ind+ny*nz*nMapEnergy]*(t)*(1.-u) 
	+ emissionMaps[ind+ny*nz*nMapEnergy+nMapEnergy]*(t)*(u) 
	+ emissionMaps[ind+nMapEnergy]*(1.-t)*(u); }
    inline double GetEmissionHII3D(int ind, double t, double u) { 
      return (emissionMapForHII[ind]*(1.-t)*(1.-u) 
	      + emissionMapForHII[ind+ny*nz*nMapEnergy]*(t)*(1.-u) 
	      + emissionMapForHII[ind+ny*nz*nMapEnergy+nMapEnergy]*(t)*(u) 
	      + emissionMapForHII[ind+nMapEnergy]*(1.-t)*(u)); }
    inline double GetEmissionIso3D(int ind, double t, double u) { 
      return emissionMapForIsotropic[ind]*(1.-t)*(1.-u) 
	+ emissionMapForIsotropic[ind+ny*nz*nMapEnergy]*(t)*(1.-u) 
	+ emissionMapForIsotropic[ind+ny*nz*nMapEnergy+nMapEnergy]*(t)*(u) 
	+ emissionMapForIsotropic[ind+nMapEnergy]*(1.-t)*(u); }

    void initDirection(Direction*,const unsigned long&);
    void updateLos(Los*,Direction*,const int&);
    void loopOverRings(const int&,const unsigned long&,Direction*,Emissivity*,
		       std::vector<double>&,std::vector<double>&);
    void computeEmissivity(const double&,Los*,Emissivity*,
			   std::vector<double>&,std::vector<double>&);
  };
   
} // namespace
#endif
