#ifndef __MY_CFITSIO_H
#define __MY_CFITSIO_H

#include <iostream>
#include <string>
#include <cstring>

#include "fitsio.h"

namespace myCfitsio {
  
  void fits_create_file(fitsfile**,const std::string&);
  void fits_close_file(fitsfile*);
  void fits_create_tbl(fitsfile*,const long&,const int&,const std::string&);
  void fits_write_col(fitsfile*,const int&,const long&,float*);
  void fits_create_img(fitsfile*,const int&,const int&,long*);
  void fits_write_img(fitsfile*,const int&,float*);
  void fits_write_date(fitsfile*);
  void fits_write_key(fitsfile*,const std::string&,const std::string&);
  void fits_write_key(fitsfile*,const std::string&,const std::string&,const std::string&);
  void fits_write_key(fitsfile*,const std::string&,int&);
  void fits_write_key(fitsfile*,const std::string&,long&,const std::string&);
  void fits_write_key(fitsfile*,const std::string&,double&);
  void fits_write_key(fitsfile*,const std::string&,double&,const std::string&);
  
} // namespace

#endif
