#ifndef __SYNCHROTRON_FUNCTIONS_H
#define __SYNCHROTRON_FUNCTIONS_H

namespace NumericalFunctions {
  
  double SynchroF(const double& x);
  double SynchroG(const double& x);
  double SynchroK13(const double& x);
  double SynchroK43(const double& x); 
  
}

#endif
